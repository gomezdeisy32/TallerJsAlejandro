﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace DR_Doctor_Lemus
{
    public partial class Ingresar : Form
    {
        public Ingresar()
        {
            InitializeComponent();
        }

  

        
        private void BtSalir_Click(object sender, EventArgs e)
        {
            TxbUsuario.Text = "";
            Close();
            TxbContraseña.Text = "";
        }

        private void BtIngresar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            try
            {
                SqlCommand cmd = new SqlCommand
                ("select ID_USUARIO, CONTRASEÑA from USUARIO where ID_USUARIO ='" + TxbUsuario.Text + "' and CONTRASEÑA = '" + TxbContraseña.Text + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "ingresar");
                DataRow dro;
                dro = ds.Tables["ingresar"].Rows[0];
                if ((TxbUsuario.Text == dro["ID_USUARIO"].ToString()) || (TxbContraseña.Text == dro["CONTRASEÑA"].ToString()))
                {
                    Principal I = new Principal();
                    I.Show();
                    this.Hide();

                }

            }

            catch
            {
                MessageBox.Show("clave o usuario erronea");
                TxbContraseña.Clear();
                TxbUsuario.Clear();
                TxbUsuario.Focus();
            }
            finally
            {
               
              
            }
        }

       

       

        private void TxbUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.sololetras(e);
        }

        private void TxbUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (TxbUsuario.Text.Length == 0)
                {
                    MessageBox.Show("el campo no puede estar vacio");
                    TxbUsuario.Focus();
                }
                else
                {
                    TxbContraseña.Focus();
                }
            }
        }

        private void TxbContraseña_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                if (TxbContraseña.Text.Length == 0)
                {
                    MessageBox.Show("el campo no puede estar vacio");
                    TxbContraseña.Focus();
                }
                else
                {
                    BtIngresar.Focus();
                }
            }
        }

        private void Ingresar_Load(object sender, EventArgs e)
        {

        }

        private void LbUsuario_Click(object sender, EventArgs e)
        {

        }

        private void TxbContraseña_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        

        

    }
}
