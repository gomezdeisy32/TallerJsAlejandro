﻿namespace DR_Doctor_Lemus
{
    partial class cliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cliente));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LbCod_Cliente = new System.Windows.Forms.Label();
            this.LbNombreCliente = new System.Windows.Forms.Label();
            this.LbApellidoCliente = new System.Windows.Forms.Label();
            this.LbNumeroDocCliente = new System.Windows.Forms.Label();
            this.TxbCodCliente = new System.Windows.Forms.TextBox();
            this.TxbNombreCliente = new System.Windows.Forms.TextBox();
            this.TxbApellidoCliente = new System.Windows.Forms.TextBox();
            this.TxbNumeroDocCliente = new System.Windows.Forms.TextBox();
            this.BtNuevo = new System.Windows.Forms.Button();
            this.BtConsultar = new System.Windows.Forms.Button();
            this.BtActualizar = new System.Windows.Forms.Button();
            this.LbCod_FacturaC = new System.Windows.Forms.Label();
            this.TxbCodFacturaCliente = new System.Windows.Forms.TextBox();
            this.BtSalir = new System.Windows.Forms.Button();
            this.LbBuscar = new System.Windows.Forms.Label();
            this.LbActualizar = new System.Windows.Forms.Label();
            this.LbNuevo = new System.Windows.Forms.Label();
            this.LbSalir = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Bt_Modificar = new System.Windows.Forms.Button();
            this.BtCancelar = new System.Windows.Forms.Button();
            this.LbModificar = new System.Windows.Forms.Label();
            this.LbCancelar = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(119, -2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(346, 594);
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // LbCod_Cliente
            // 
            this.LbCod_Cliente.AutoSize = true;
            this.LbCod_Cliente.BackColor = System.Drawing.Color.Transparent;
            this.LbCod_Cliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCod_Cliente.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCod_Cliente.Location = new System.Drawing.Point(146, 37);
            this.LbCod_Cliente.Name = "LbCod_Cliente";
            this.LbCod_Cliente.Size = new System.Drawing.Size(126, 20);
            this.LbCod_Cliente.TabIndex = 12;
            this.LbCod_Cliente.Text = "Codigo Cliente";
            // 
            // LbNombreCliente
            // 
            this.LbNombreCliente.AutoSize = true;
            this.LbNombreCliente.BackColor = System.Drawing.Color.Transparent;
            this.LbNombreCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbNombreCliente.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbNombreCliente.Location = new System.Drawing.Point(189, 101);
            this.LbNombreCliente.Name = "LbNombreCliente";
            this.LbNombreCliente.Size = new System.Drawing.Size(71, 20);
            this.LbNombreCliente.TabIndex = 13;
            this.LbNombreCliente.Text = "Nombre";
            // 
            // LbApellidoCliente
            // 
            this.LbApellidoCliente.AutoSize = true;
            this.LbApellidoCliente.BackColor = System.Drawing.Color.Transparent;
            this.LbApellidoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbApellidoCliente.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbApellidoCliente.Location = new System.Drawing.Point(189, 176);
            this.LbApellidoCliente.Name = "LbApellidoCliente";
            this.LbApellidoCliente.Size = new System.Drawing.Size(73, 20);
            this.LbApellidoCliente.TabIndex = 14;
            this.LbApellidoCliente.Text = "Apellido";
            // 
            // LbNumeroDocCliente
            // 
            this.LbNumeroDocCliente.AutoSize = true;
            this.LbNumeroDocCliente.BackColor = System.Drawing.Color.Transparent;
            this.LbNumeroDocCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbNumeroDocCliente.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.LbNumeroDocCliente.Location = new System.Drawing.Point(126, 258);
            this.LbNumeroDocCliente.Name = "LbNumeroDocCliente";
            this.LbNumeroDocCliente.Size = new System.Drawing.Size(168, 20);
            this.LbNumeroDocCliente.TabIndex = 15;
            this.LbNumeroDocCliente.Text = "Numero Documento";
            // 
            // TxbCodCliente
            // 
            this.TxbCodCliente.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.TxbCodCliente.Location = new System.Drawing.Point(296, 33);
            this.TxbCodCliente.Name = "TxbCodCliente";
            this.TxbCodCliente.Size = new System.Drawing.Size(140, 20);
            this.TxbCodCliente.TabIndex = 18;
            this.TxbCodCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCodCliente_KeyPress);
            // 
            // TxbNombreCliente
            // 
            this.TxbNombreCliente.Enabled = false;
            this.TxbNombreCliente.Location = new System.Drawing.Point(296, 100);
            this.TxbNombreCliente.Name = "TxbNombreCliente";
            this.TxbNombreCliente.Size = new System.Drawing.Size(140, 20);
            this.TxbNombreCliente.TabIndex = 19;
            this.TxbNombreCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbNombreCliente_KeyPress);
            // 
            // TxbApellidoCliente
            // 
            this.TxbApellidoCliente.Enabled = false;
            this.TxbApellidoCliente.Location = new System.Drawing.Point(296, 175);
            this.TxbApellidoCliente.Name = "TxbApellidoCliente";
            this.TxbApellidoCliente.Size = new System.Drawing.Size(140, 20);
            this.TxbApellidoCliente.TabIndex = 20;
            this.TxbApellidoCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbApellidoCliente_KeyPress);
            // 
            // TxbNumeroDocCliente
            // 
            this.TxbNumeroDocCliente.Enabled = false;
            this.TxbNumeroDocCliente.Location = new System.Drawing.Point(296, 260);
            this.TxbNumeroDocCliente.Name = "TxbNumeroDocCliente";
            this.TxbNumeroDocCliente.Size = new System.Drawing.Size(140, 20);
            this.TxbNumeroDocCliente.TabIndex = 21;
            this.TxbNumeroDocCliente.TextChanged += new System.EventHandler(this.TxbNumeroDocCliente_TextChanged);
            this.TxbNumeroDocCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbNumeroDocCliente_KeyPress);
            // 
            // BtNuevo
            // 
            this.BtNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtNuevo.BackgroundImage")));
            this.BtNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtNuevo.Enabled = false;
            this.BtNuevo.Location = new System.Drawing.Point(24, 179);
            this.BtNuevo.Name = "BtNuevo";
            this.BtNuevo.Size = new System.Drawing.Size(54, 50);
            this.BtNuevo.TabIndex = 24;
            this.BtNuevo.UseVisualStyleBackColor = true;
            this.BtNuevo.Visible = false;
            this.BtNuevo.Click += new System.EventHandler(this.BtNuevo_Click);
            // 
            // BtConsultar
            // 
            this.BtConsultar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtConsultar.BackgroundImage")));
            this.BtConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtConsultar.Location = new System.Drawing.Point(24, 48);
            this.BtConsultar.Name = "BtConsultar";
            this.BtConsultar.Size = new System.Drawing.Size(54, 50);
            this.BtConsultar.TabIndex = 25;
            this.BtConsultar.UseVisualStyleBackColor = true;
            this.BtConsultar.Click += new System.EventHandler(this.BtConsultar_Click);
            // 
            // BtActualizar
            // 
            this.BtActualizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtActualizar.BackgroundImage")));
            this.BtActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtActualizar.Enabled = false;
            this.BtActualizar.Location = new System.Drawing.Point(24, 294);
            this.BtActualizar.Name = "BtActualizar";
            this.BtActualizar.Size = new System.Drawing.Size(54, 50);
            this.BtActualizar.TabIndex = 27;
            this.BtActualizar.UseVisualStyleBackColor = true;
            this.BtActualizar.Visible = false;
            this.BtActualizar.Click += new System.EventHandler(this.BtActualizar_Click);
            // 
            // LbCod_FacturaC
            // 
            this.LbCod_FacturaC.AutoSize = true;
            this.LbCod_FacturaC.BackColor = System.Drawing.Color.Transparent;
            this.LbCod_FacturaC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCod_FacturaC.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCod_FacturaC.Location = new System.Drawing.Point(162, 335);
            this.LbCod_FacturaC.Name = "LbCod_FacturaC";
            this.LbCod_FacturaC.Size = new System.Drawing.Size(132, 20);
            this.LbCod_FacturaC.TabIndex = 28;
            this.LbCod_FacturaC.Text = "Codigo Factura";
            // 
            // TxbCodFacturaCliente
            // 
            this.TxbCodFacturaCliente.Enabled = false;
            this.TxbCodFacturaCliente.Location = new System.Drawing.Point(296, 335);
            this.TxbCodFacturaCliente.Name = "TxbCodFacturaCliente";
            this.TxbCodFacturaCliente.Size = new System.Drawing.Size(140, 20);
            this.TxbCodFacturaCliente.TabIndex = 29;
            this.TxbCodFacturaCliente.TextChanged += new System.EventHandler(this.TxbCodFacturaCliente_TextChanged);
            this.TxbCodFacturaCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCodFacturaCliente_KeyPress);
            // 
            // BtSalir
            // 
            this.BtSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtSalir.BackgroundImage")));
            this.BtSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtSalir.Location = new System.Drawing.Point(498, 293);
            this.BtSalir.Name = "BtSalir";
            this.BtSalir.Size = new System.Drawing.Size(54, 50);
            this.BtSalir.TabIndex = 42;
            this.BtSalir.UseVisualStyleBackColor = true;
            this.BtSalir.Click += new System.EventHandler(this.BtSalir_Click);
            // 
            // LbBuscar
            // 
            this.LbBuscar.AutoSize = true;
            this.LbBuscar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbBuscar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LbBuscar.Location = new System.Drawing.Point(21, 111);
            this.LbBuscar.Name = "LbBuscar";
            this.LbBuscar.Size = new System.Drawing.Size(40, 13);
            this.LbBuscar.TabIndex = 47;
            this.LbBuscar.Text = "Buscar";
            // 
            // LbActualizar
            // 
            this.LbActualizar.AutoSize = true;
            this.LbActualizar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbActualizar.Enabled = false;
            this.LbActualizar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.LbActualizar.Location = new System.Drawing.Point(25, 360);
            this.LbActualizar.Name = "LbActualizar";
            this.LbActualizar.Size = new System.Drawing.Size(53, 13);
            this.LbActualizar.TabIndex = 48;
            this.LbActualizar.Text = "Actualizar";
            this.LbActualizar.Visible = false;
            // 
            // LbNuevo
            // 
            this.LbNuevo.AutoSize = true;
            this.LbNuevo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbNuevo.Enabled = false;
            this.LbNuevo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbNuevo.Location = new System.Drawing.Point(39, 232);
            this.LbNuevo.Name = "LbNuevo";
            this.LbNuevo.Size = new System.Drawing.Size(39, 13);
            this.LbNuevo.TabIndex = 49;
            this.LbNuevo.Text = "Nuevo";
            this.LbNuevo.Visible = false;
            // 
            // LbSalir
            // 
            this.LbSalir.AutoSize = true;
            this.LbSalir.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbSalir.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbSalir.Location = new System.Drawing.Point(507, 360);
            this.LbSalir.Name = "LbSalir";
            this.LbSalir.Size = new System.Drawing.Size(27, 13);
            this.LbSalir.TabIndex = 50;
            this.LbSalir.Text = "Salir";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(149, 421);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(299, 121);
            this.dataGridView1.TabIndex = 51;
            // 
            // Bt_Modificar
            // 
            this.Bt_Modificar.BackColor = System.Drawing.Color.Transparent;
            this.Bt_Modificar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Bt_Modificar.BackgroundImage")));
            this.Bt_Modificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bt_Modificar.Enabled = false;
            this.Bt_Modificar.Location = new System.Drawing.Point(491, 48);
            this.Bt_Modificar.Name = "Bt_Modificar";
            this.Bt_Modificar.Size = new System.Drawing.Size(54, 50);
            this.Bt_Modificar.TabIndex = 52;
            this.Bt_Modificar.UseVisualStyleBackColor = false;
            this.Bt_Modificar.Visible = false;
            this.Bt_Modificar.Click += new System.EventHandler(this.Bt_Modificar_Click);
            // 
            // BtCancelar
            // 
            this.BtCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtCancelar.BackgroundImage")));
            this.BtCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtCancelar.Location = new System.Drawing.Point(491, 159);
            this.BtCancelar.Name = "BtCancelar";
            this.BtCancelar.Size = new System.Drawing.Size(54, 50);
            this.BtCancelar.TabIndex = 54;
            this.BtCancelar.UseVisualStyleBackColor = false;
            this.BtCancelar.Click += new System.EventHandler(this.BtCancelar_Click);
            // 
            // LbModificar
            // 
            this.LbModificar.AutoSize = true;
            this.LbModificar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbModificar.Enabled = false;
            this.LbModificar.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.LbModificar.Location = new System.Drawing.Point(495, 101);
            this.LbModificar.Name = "LbModificar";
            this.LbModificar.Size = new System.Drawing.Size(50, 13);
            this.LbModificar.TabIndex = 55;
            this.LbModificar.Text = "Modificar";
            this.LbModificar.Visible = false;
            // 
            // LbCancelar
            // 
            this.LbCancelar.AutoSize = true;
            this.LbCancelar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbCancelar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LbCancelar.Location = new System.Drawing.Point(497, 237);
            this.LbCancelar.Name = "LbCancelar";
            this.LbCancelar.Size = new System.Drawing.Size(49, 13);
            this.LbCancelar.TabIndex = 56;
            this.LbCancelar.Text = "Cancelar";
            // 
            // cliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(585, 578);
            this.Controls.Add(this.LbCancelar);
            this.Controls.Add(this.LbModificar);
            this.Controls.Add(this.BtCancelar);
            this.Controls.Add(this.Bt_Modificar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.LbSalir);
            this.Controls.Add(this.LbNuevo);
            this.Controls.Add(this.LbActualizar);
            this.Controls.Add(this.LbBuscar);
            this.Controls.Add(this.BtSalir);
            this.Controls.Add(this.TxbCodFacturaCliente);
            this.Controls.Add(this.LbCod_FacturaC);
            this.Controls.Add(this.BtActualizar);
            this.Controls.Add(this.BtConsultar);
            this.Controls.Add(this.BtNuevo);
            this.Controls.Add(this.TxbNumeroDocCliente);
            this.Controls.Add(this.TxbApellidoCliente);
            this.Controls.Add(this.TxbNombreCliente);
            this.Controls.Add(this.TxbCodCliente);
            this.Controls.Add(this.LbNumeroDocCliente);
            this.Controls.Add(this.LbApellidoCliente);
            this.Controls.Add(this.LbNombreCliente);
            this.Controls.Add(this.LbCod_Cliente);
            this.Controls.Add(this.pictureBox1);
            this.Name = "cliente";
            this.Text = "}";
            this.Load += new System.EventHandler(this.cliente_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label LbCod_Cliente;
        private System.Windows.Forms.Label LbNombreCliente;
        private System.Windows.Forms.Label LbApellidoCliente;
        private System.Windows.Forms.Label LbNumeroDocCliente;
        private System.Windows.Forms.TextBox TxbCodCliente;
        private System.Windows.Forms.TextBox TxbNombreCliente;
        private System.Windows.Forms.TextBox TxbApellidoCliente;
        private System.Windows.Forms.TextBox TxbNumeroDocCliente;
        private System.Windows.Forms.Button BtNuevo;
        private System.Windows.Forms.Button BtConsultar;
        private System.Windows.Forms.Button BtActualizar;
        private System.Windows.Forms.Label LbCod_FacturaC;
        private System.Windows.Forms.TextBox TxbCodFacturaCliente;
        private System.Windows.Forms.Button BtSalir;
        private System.Windows.Forms.Label LbBuscar;
        private System.Windows.Forms.Label LbActualizar;
        private System.Windows.Forms.Label LbNuevo;
        private System.Windows.Forms.Label LbSalir;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button Bt_Modificar;
        private System.Windows.Forms.Button BtCancelar;
        private System.Windows.Forms.Label LbModificar;
        private System.Windows.Forms.Label LbCancelar;

    }
}