﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DR_Doctor_Lemus;
using System.Data.SqlClient;


namespace DR_Doctor_Lemus
{
    public partial class Devolucion : Form
    {
        public Devolucion()
        {
            InitializeComponent();
        }

        private void BtSalir_Click(object sender, EventArgs e)
        {
            Principal Devolucion = new Principal();
            Devolucion.Show();
            this.Hide();
        }

        private void BtNuevo_Click(object sender, EventArgs e)
        {
            if (TxbCodDevoluion.Text == "")
            {
                MessageBox.Show("el campo apellido esta  vacion");
                TxbCodDevoluion.Focus();


            }
            else if (TxbMotivDevol.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbMotivDevol.Focus();
            }
            else if (TxbCodEmpleadoDev.Text == "")
            {
                MessageBox.Show("el campo codigo entrega esta vacio");
                TxbCodEmpleadoDev.Focus();
            }
            else if (TxbCodigoFacDev.Text == "")
            {
                MessageBox.Show("el campo nombre esta vacio");
                TxbCodigoFacDev.Focus();
            }
            else if (TxbCantidad.Text == "")
            {
                MessageBox.Show("el campo numero documento esta vacio");
                TxbCantidad.Focus();
            }

            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");


                SqlCommand cmd = new SqlCommand("insert into DEVOLUCION(CODDIGODEVOLUCION, MOTIVODEVOLUCION,CODIGOEMPLEADO,CODIGOFACTURA,CANTIDAD)Values('" + TxbCodDevoluion.Text + "','" + TxbMotivDevol.Text + "','" + Convert.ToInt16(TxbCodEmpleadoDev.Text) + "', '" + Convert.ToInt16(TxbCodigoFacDev.Text) + "', '" + Convert.ToInt16(TxbCantidad.Text) +  "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se   agrego con exito ");
            }  
        }

        private void BtConsultar_Click(object sender, EventArgs e)
        {
            if (TxbCodDevoluion.Text == "")
            {

                MessageBox.Show("el campo no puede estar vacio");
                TxbCodDevoluion.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                try
                {
                    SqlCommand cmd = new SqlCommand
                    ("select * from DEVOLUCION where CODIGODEVOLUCION ='" + TxbCodDevoluion.Text + "' ", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "DEVOLUCION");
                    DataRow dro;
                    dro = ds.Tables["DEVOLUCION"].Rows[0];
                    TxbCantidad.Text = dro["CANTIDAD"].ToString();
                    TxbCodEmpleadoDev.Text = dro["CODIGOEMPLEADO"].ToString();
                    TxbCodigoFacDev.Text = dro["CODIGOFACTURA"].ToString();
                    TxbMotivDevol.Text = dro["MOTIVODEVOLUCION"].ToString();

                    BtActualizar.Enabled = true;
                    BtActualizar.Visible = true;



                }
                catch
                {
                    //MessageBox.Show(" usuario no encontrado")
                    const string mensaje = "devolucion no encontrada desea crearla si/no";
                    const string titulo = "consulta";
                    var resultado = MessageBox.Show(mensaje, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (resultado == DialogResult.No)
                    {
                        TxbCodDevoluion.Clear();
                        TxbCodDevoluion.Focus();
                    }
                    else
                    {


                        TxbCodDevoluion.Enabled= false;
                        TxbCantidad.Clear();
                        TxbCodEmpleadoDev.Clear();
                        TxbCodigoFacDev.Clear();
                        TxbMotivDevol.Clear();

                        TxbMotivDevol.Enabled = true;
                        TxbCodigoFacDev.Enabled = true;
                        TxbCodEmpleadoDev.Enabled = true;
                        TxbCantidad.Enabled = true;

                        BtNuevo.Visible = true;
                        BtNuevo.Enabled = true;
                        BtConsultar.Enabled = false;


                    }

                }

                finally
                {
                    con.Close();
                }

            }
        }

        private void BtActualizar_Click(object sender, EventArgs e)
        {
            habilitar();
            BtActualizar.Enabled = false;
            TxbCodDevoluion.Enabled = false;
            BtModificar.Enabled = true;
            BtModificar.Visible = true;
        }

        private void BtCancelar_Click(object sender, EventArgs e)
        {
            desabilitar();
            limpiar();
            TxbCodDevoluion.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtModificar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");

            SqlCommand cmd = new SqlCommand("update DEVOLUCION set MOTIVODEVOLUCION ='" + TxbMotivDevol.Text + "', CODIGOEMPLEADO = '" + Convert.ToInt16(TxbCodEmpleadoDev.Text) + "', CODIGOFACTURA = '" +Convert.ToInt16(TxbCodigoFacDev.Text) + "',  CANTIDAD = '" + Convert.ToInt16(TxbCantidad.Text) + "' where CODIGODEVOLUCION = '" + TxbCodDevoluion.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            MessageBox.Show("se modifico  correctamente");
            desabilitar();
            limpiar();
            TxbCodDevoluion.Enabled= true;
            BtConsultar.Enabled = true;
        }
        private void limpiar()
        {
            TxbCodDevoluion.Clear();
            TxbCantidad.Clear();
            TxbCodEmpleadoDev.Clear();
            TxbCodigoFacDev.Clear();
            TxbMotivDevol.Clear();

            BtActualizar.Visible = false;
            BtNuevo.Visible = false;
            BtModificar.Visible = false;


        }
        private void desabilitar()
        {

            TxbMotivDevol.Enabled = false;
            TxbCantidad.Enabled = false;
            TxbCodEmpleadoDev.Enabled = false;
            TxbCodigoFacDev.Enabled = false;
            BtActualizar.Enabled = false;
            BtNuevo.Enabled = false;
            BtModificar.Enabled = false;

        }
        private void habilitar()
        {
            TxbMotivDevol.Enabled = true;
            TxbCodigoFacDev.Enabled = true;
            TxbCodEmpleadoDev.Enabled = true;
            TxbCantidad.Enabled = true;
            BtActualizar.Enabled = true;
            BtNuevo.Enabled = true;
            BtModificar.Enabled = true;

        }

        private void Devolucion_Load(object sender, EventArgs e)
        {
            Conexion objeto = new Conexion();
            objeto.ActualizarGrid(this.dataGridView1, "select * from DEVOLUCION");
        }

        private void TxbMotivDevol_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.sololetras(e);
        }

        private void TxbCodEmpleadoDev_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbCodigoFacDev_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        
        

        

       

        
        }

        

        
        

        
    }



