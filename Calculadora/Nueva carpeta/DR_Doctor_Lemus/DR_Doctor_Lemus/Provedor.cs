﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DR_Doctor_Lemus;
using System.Data.SqlClient;

namespace DR_Doctor_Lemus
{
    public partial class Provedor : Form
    {
        public Provedor()
        {
            InitializeComponent();
        }

        

        private void BtSalir_Click(object sender, EventArgs e)
        {
           // Interfaz proveedor = new Interfaz();
            //proveedor.Show();
            //this.Hide();
        }

        
           

        private void BtConsultar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            try
            {
                SqlCommand cmd = new SqlCommand
                ("select * from PROVEEDOR where CODIGO_PROVEEDOR ='" + TxbCod_Proveedor.Text + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "provedor");
                DataRow dro;
                dro = ds.Tables["provedor"].Rows[0];
                TxbCod_FacturaC.Text = dro["CODIGOFACTURA_C"].ToString();
                Txb_Telefono.Text = dro["TELEFONO"].ToString();
                Txb_Nombre.Text = dro["NOMBREPROVEEDOR"].ToString();
                Txb_CodigoImportacion.Text = dro["CODIGO_IMPORTACION"].ToString();
                
            }
            catch
            {
                MessageBox.Show(" importacion no encontrada");
            }
            finally
            {
                con.Close();
            }

        }

        private void BtActualizar_Click(object sender, EventArgs e)
        {
            //conexion a base de datos
            SqlConnection conn = new SqlConnection("Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            //abro conexion
            conn.Open();
            //buscamos el nombre y el registro a actualizar para verificar
            string consulta = "select NOMBRE,CANTIDAD  from PROVEEDOR where telefono = '" + Txb_Telefono + "'";
            SqlCommand COMAND = new SqlCommand(consulta, conn);
            // El resultado lo guardaremos en una tabla

            DataTable tabla = new DataTable();
            // Usaremos un DataAdapter para leer los datos
            SqlDataAdapter AdaptadorTabla = new SqlDataAdapter(consulta, conn);
            //DataSet ds = new DataSet();
            // Llenamos la tabla con los datos leídos
            AdaptadorTabla.Fill(tabla);
            //guardo informacion en variables
            string nombre = tabla.Rows[0]["NOMBRE"].ToString();
            string cantidad = tabla.Rows[0]["CANTIDAD"].ToString();
            // Creamos el cuadro de dialogo y guardamos la respuesta que da el usuario
            DialogResult respuesta = MessageBox.Show("Desea actualizar el registro de " + nombre + " " + cantidad + ".", "actualizar", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            //Si acepto actualizar se ejecuta
            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {
                string StrComando = "DELETE FROM proveedor WHERE telefono =  '" + Txb_Telefono + "'";
                SqlCommand COMANDO = new SqlCommand(StrComando, conn);
                COMANDO.ExecuteNonQuery();
                MessageBox.Show("El registro seactualizo de manera  EXITOSA");


            }
            else
            {
                MessageBox.Show("el registro no se actualizo");

            }
            
            
            Txb_CodigoImportacion.Text = "";
            Txb_Nombre.Text = "";
            Txb_Telefono.Text = "";
            TxbCod_FacturaC.Text = "";
            TxbCod_Proveedor.Text = "";
           
        }

        private void TxbCod_Proveedor_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }
        }

        private void Txb_Nombre_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo letras");
            }
        }

        

        private void Txb_Telefono_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }
        }

        

        private void TxbCod_FacturaC_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }
        }

        private void TxbListaProductos_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }
        }

        private void Txb_Cantidad_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }
        }

        private void limpiar()
        {
           
            Txb_CodigoImportacion.Clear();
            Txb_Nombre.Clear();
            Txb_Telefono.Clear();
            TxbCod_FacturaC.Clear();
            TxbCod_Proveedor.Clear();
            BtActualizar.Visible = false;
            BtNuevo.Visible = false;
            BtModificar.Visible = false;


        }
        private void desabilitar()
        {

            
            Txb_CodigoImportacion.Enabled = false;
            Txb_Nombre.Enabled = false;
            Txb_Telefono.Enabled = false;
            TxbCod_FacturaC.Enabled = false;
            BtActualizar.Enabled = false;
            BtNuevo.Enabled = false;
            BtModificar.Enabled = false;

        }
        private void habilitar()
        {
            
            Txb_CodigoImportacion.Enabled = true;
            Txb_Nombre.Enabled = true;
            Txb_Telefono.Enabled = true;
            TxbCod_FacturaC.Enabled = true;
            BtActualizar.Enabled = true;
            BtNuevo.Enabled = true;
            BtModificar.Enabled = true;

        }

        private void BtConsultar_Click_1(object sender, EventArgs e)
        {
            if (TxbCod_Proveedor.Text == "")
            {

                MessageBox.Show("el campo no puede estar vacio");
                TxbCod_Proveedor.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                try
                {
                    SqlCommand cmd = new SqlCommand
                    ("select * from PROVEEDOR where CODIGO_PROVEEDOR ='" + TxbCod_Proveedor.Text + "' ", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "PROVEEDOR");
                    DataRow dro;
                    dro = ds.Tables["PROVEEDOR"].Rows[0];
                    Txb_Nombre.Text = dro["NOMBREPROVEEDOR"].ToString();
                    Txb_Telefono.Text = dro["TELEFONO"].ToString();
                    Txb_CodigoImportacion.Text = dro["CODIGO_IMPORTACION"].ToString();
                    TxbCod_FacturaC.Text = dro["CODIGOFACTURA_C"].ToString();
                    BtActualizar.Enabled = true;
                    BtActualizar.Visible = true;

                }
                catch
                {
                    //MessageBox.Show(" usuario no encontrado")
                    const string mensaje = "proveedor no encontrado desea crearlo si/no";
                    const string titulo = "consulta";
                    var resultado = MessageBox.Show(mensaje, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (resultado == DialogResult.No)
                    {
                        TxbCod_Proveedor.Clear();
                        TxbCod_Proveedor.Focus();
                    }
                    else
                    {


                        TxbCod_Proveedor.Enabled = false;
                        
                        Txb_CodigoImportacion.Clear();
                        Txb_Nombre.Clear();
                        Txb_Telefono.Clear();
                        TxbCod_FacturaC.Clear();
                        TxbCod_FacturaC.Enabled = true;
                        Txb_Telefono.Enabled = true;
                        Txb_Nombre.Enabled = true;
                        Txb_CodigoImportacion.Enabled = true;
                           

                        BtNuevo.Visible = true;
                        BtNuevo.Enabled = true;
                        BtConsultar.Enabled = false;

                    }

                }

                finally
                {
                    con.Close();
                }

            }
        }

        private void BtNuevo_Click_1(object sender, EventArgs e)
        {
            if (TxbCod_Proveedor.Text == "")
            {
                MessageBox.Show("el campo apellido esta  vacion");
                TxbCod_Proveedor.Focus();


            }
            else if (Txb_Nombre.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                Txb_Nombre.Focus();
            }
           
            else if (Txb_Telefono.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                Txb_Telefono.Focus();
            }
            else if (Txb_CodigoImportacion.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                Txb_CodigoImportacion.Focus();
            }
            else if (TxbCod_FacturaC.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbCod_FacturaC.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");


                SqlCommand cmd = new SqlCommand("insert into PROVEEDOR(CODIGO_PROVEEDOR,NOMBREPROVEEDOR,TELEFONO,CODIGO_IMPORTACION,CODIGOFACTURA_C)Values('" + TxbCod_Proveedor.Text + "','" + Txb_Nombre.Text + "', '" + Convert.ToInt16(Txb_Telefono.Text) + "', '" + Txb_CodigoImportacion.Text + "', '" + Convert.ToInt16(TxbCod_FacturaC.Text) + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se   agrego con exito ");
            }
        }

        private void BtActualizar_Click_1(object sender, EventArgs e)
        {
            habilitar();
            BtActualizar.Enabled = false;
            TxbCod_Proveedor.Enabled = false;
            BtModificar.Enabled = true;
            BtModificar.Visible = true;
        }

        private void BtModificar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");

            SqlCommand cmd = new SqlCommand("update PROVEEDOR set NOMBREPROVEEDOR ='" + Txb_Nombre.Text + "', TELEFONO = '" + Txb_Telefono.Text + "', CODIGOFACTURA_C = '" + TxbCod_FacturaC.Text + "',  CODIGO_IMPORTACION = '" + Txb_CodigoImportacion.Text + "'  where CODIGO_PROVEEDOR = '" + TxbCod_Proveedor.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            MessageBox.Show("se modifico  correctamente");
            desabilitar();
            limpiar();
            TxbCod_Proveedor.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtCancelar_Click(object sender, EventArgs e)
        {
            desabilitar();
            limpiar();
            Txb_CodigoImportacion.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtSalir_Click_1(object sender, EventArgs e)
        {
            Principal provedor = new Principal();
            provedor.Show();
            this.Hide();
        }

        private void Provedor_Load(object sender, EventArgs e)
        {
            Conexion objeto = new Conexion();
            objeto.ActualizarGrid(this.dataGridView1, "select * from PROVEEDOR");
        }

        private void Txb_Nombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.sololetras(e);
        }

        private void Txb_Telefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbCod_FacturaC_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
    }
}
