﻿namespace DR_Doctor_Lemus
{
    partial class facturacompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(facturacompra));
            this.LbCodigoFCompra = new System.Windows.Forms.Label();
            this.LbFechaFactura = new System.Windows.Forms.Label();
            this.LbCodigoProvedor = new System.Windows.Forms.Label();
            this.TxbCodProvedor = new System.Windows.Forms.TextBox();
            this.TxbCod_FacturaC = new System.Windows.Forms.TextBox();
            this.TxbFechaFactura = new System.Windows.Forms.TextBox();
            this.LbCodigoProducto = new System.Windows.Forms.Label();
            this.TxbCodigoProducto = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.LbCancelar = new System.Windows.Forms.Label();
            this.BtCancelar = new System.Windows.Forms.Button();
            this.LbNuevo = new System.Windows.Forms.Label();
            this.LbBuscar = new System.Windows.Forms.Label();
            this.BtConsultar = new System.Windows.Forms.Button();
            this.BtNuevo = new System.Windows.Forms.Button();
            this.BtModificar = new System.Windows.Forms.Button();
            this.LbSalir = new System.Windows.Forms.Label();
            this.LbActualizar = new System.Windows.Forms.Label();
            this.BtSalir = new System.Windows.Forms.Button();
            this.BtActualizar = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // LbCodigoFCompra
            // 
            this.LbCodigoFCompra.AutoSize = true;
            this.LbCodigoFCompra.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigoFCompra.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigoFCompra.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodigoFCompra.Location = new System.Drawing.Point(118, 71);
            this.LbCodigoFCompra.Name = "LbCodigoFCompra";
            this.LbCodigoFCompra.Size = new System.Drawing.Size(199, 20);
            this.LbCodigoFCompra.TabIndex = 0;
            this.LbCodigoFCompra.Text = "Codigo Factura Compra";
            // 
            // LbFechaFactura
            // 
            this.LbFechaFactura.AutoSize = true;
            this.LbFechaFactura.BackColor = System.Drawing.Color.Transparent;
            this.LbFechaFactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbFechaFactura.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbFechaFactura.Location = new System.Drawing.Point(157, 123);
            this.LbFechaFactura.Name = "LbFechaFactura";
            this.LbFechaFactura.Size = new System.Drawing.Size(126, 20);
            this.LbFechaFactura.TabIndex = 1;
            this.LbFechaFactura.Text = "Fecha Factura";
            // 
            // LbCodigoProvedor
            // 
            this.LbCodigoProvedor.AutoSize = true;
            this.LbCodigoProvedor.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigoProvedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigoProvedor.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodigoProvedor.Location = new System.Drawing.Point(157, 254);
            this.LbCodigoProvedor.Name = "LbCodigoProvedor";
            this.LbCodigoProvedor.Size = new System.Drawing.Size(141, 20);
            this.LbCodigoProvedor.TabIndex = 5;
            this.LbCodigoProvedor.Text = "Codigo Provedor";
            this.toolTip1.SetToolTip(this.LbCodigoProvedor, "recuerde que su codigo debe inicial con una letra");
            // 
            // TxbCodProvedor
            // 
            this.TxbCodProvedor.Enabled = false;
            this.TxbCodProvedor.Location = new System.Drawing.Point(323, 250);
            this.TxbCodProvedor.Name = "TxbCodProvedor";
            this.TxbCodProvedor.Size = new System.Drawing.Size(131, 20);
            this.TxbCodProvedor.TabIndex = 6;
            // 
            // TxbCod_FacturaC
            // 
            this.TxbCod_FacturaC.Location = new System.Drawing.Point(323, 71);
            this.TxbCod_FacturaC.Name = "TxbCod_FacturaC";
            this.TxbCod_FacturaC.Size = new System.Drawing.Size(131, 20);
            this.TxbCod_FacturaC.TabIndex = 7;
            this.TxbCod_FacturaC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCod_FacturaC_KeyPress);
            // 
            // TxbFechaFactura
            // 
            this.TxbFechaFactura.Enabled = false;
            this.TxbFechaFactura.Location = new System.Drawing.Point(323, 134);
            this.TxbFechaFactura.Name = "TxbFechaFactura";
            this.TxbFechaFactura.Size = new System.Drawing.Size(131, 20);
            this.TxbFechaFactura.TabIndex = 8;
            this.TxbFechaFactura.TextChanged += new System.EventHandler(this.TxbFechaFactura_TextChanged);
            // 
            // LbCodigoProducto
            // 
            this.LbCodigoProducto.AutoSize = true;
            this.LbCodigoProducto.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigoProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigoProducto.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodigoProducto.Location = new System.Drawing.Point(157, 196);
            this.LbCodigoProducto.Name = "LbCodigoProducto";
            this.LbCodigoProducto.Size = new System.Drawing.Size(137, 20);
            this.LbCodigoProducto.TabIndex = 45;
            this.LbCodigoProducto.Text = "CodigoProducto";
            // 
            // TxbCodigoProducto
            // 
            this.TxbCodigoProducto.Enabled = false;
            this.TxbCodigoProducto.Location = new System.Drawing.Point(323, 196);
            this.TxbCodigoProducto.Name = "TxbCodigoProducto";
            this.TxbCodigoProducto.Size = new System.Drawing.Size(131, 20);
            this.TxbCodigoProducto.TabIndex = 46;
            this.TxbCodigoProducto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCodigoProducto_KeyPress);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(121, -4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(343, 604);
            this.pictureBox2.TabIndex = 114;
            this.pictureBox2.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(136, 349);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(308, 137);
            this.dataGridView1.TabIndex = 115;
            // 
            // LbCancelar
            // 
            this.LbCancelar.AutoSize = true;
            this.LbCancelar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbCancelar.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCancelar.Location = new System.Drawing.Point(38, 377);
            this.LbCancelar.Name = "LbCancelar";
            this.LbCancelar.Size = new System.Drawing.Size(49, 13);
            this.LbCancelar.TabIndex = 121;
            this.LbCancelar.Text = "Cancelar";
            // 
            // BtCancelar
            // 
            this.BtCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtCancelar.BackgroundImage")));
            this.BtCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtCancelar.Location = new System.Drawing.Point(33, 310);
            this.BtCancelar.Name = "BtCancelar";
            this.BtCancelar.Size = new System.Drawing.Size(55, 53);
            this.BtCancelar.TabIndex = 120;
            this.BtCancelar.UseVisualStyleBackColor = false;
            this.BtCancelar.Click += new System.EventHandler(this.BtCancelar_Click);
            // 
            // LbNuevo
            // 
            this.LbNuevo.AutoSize = true;
            this.LbNuevo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbNuevo.Enabled = false;
            this.LbNuevo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbNuevo.Location = new System.Drawing.Point(36, 283);
            this.LbNuevo.Name = "LbNuevo";
            this.LbNuevo.Size = new System.Drawing.Size(39, 13);
            this.LbNuevo.TabIndex = 119;
            this.LbNuevo.Text = "Nuevo";
            this.LbNuevo.Visible = false;
            // 
            // LbBuscar
            // 
            this.LbBuscar.AutoSize = true;
            this.LbBuscar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbBuscar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LbBuscar.Location = new System.Drawing.Point(35, 169);
            this.LbBuscar.Name = "LbBuscar";
            this.LbBuscar.Size = new System.Drawing.Size(40, 13);
            this.LbBuscar.TabIndex = 118;
            this.LbBuscar.Text = "Buscar";
            // 
            // BtConsultar
            // 
            this.BtConsultar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtConsultar.BackgroundImage")));
            this.BtConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtConsultar.Location = new System.Drawing.Point(32, 104);
            this.BtConsultar.Name = "BtConsultar";
            this.BtConsultar.Size = new System.Drawing.Size(54, 50);
            this.BtConsultar.TabIndex = 117;
            this.BtConsultar.UseVisualStyleBackColor = true;
            this.BtConsultar.Click += new System.EventHandler(this.BtConsultar_Click_1);
            // 
            // BtNuevo
            // 
            this.BtNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtNuevo.BackgroundImage")));
            this.BtNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtNuevo.Enabled = false;
            this.BtNuevo.Location = new System.Drawing.Point(33, 221);
            this.BtNuevo.Name = "BtNuevo";
            this.BtNuevo.Size = new System.Drawing.Size(54, 49);
            this.BtNuevo.TabIndex = 116;
            this.BtNuevo.UseVisualStyleBackColor = true;
            this.BtNuevo.Visible = false;
            this.BtNuevo.Click += new System.EventHandler(this.BtNuevo_Click_1);
            // 
            // BtModificar
            // 
            this.BtModificar.BackColor = System.Drawing.Color.Transparent;
            this.BtModificar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtModificar.BackgroundImage")));
            this.BtModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtModificar.Enabled = false;
            this.BtModificar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtModificar.Location = new System.Drawing.Point(502, 208);
            this.BtModificar.Name = "BtModificar";
            this.BtModificar.Size = new System.Drawing.Size(53, 56);
            this.BtModificar.TabIndex = 126;
            this.BtModificar.UseVisualStyleBackColor = false;
            this.BtModificar.Visible = false;
            this.BtModificar.Click += new System.EventHandler(this.BtModificar_Click);
            // 
            // LbSalir
            // 
            this.LbSalir.AutoSize = true;
            this.LbSalir.BackColor = System.Drawing.Color.DimGray;
            this.LbSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.LbSalir.Location = new System.Drawing.Point(515, 395);
            this.LbSalir.Name = "LbSalir";
            this.LbSalir.Size = new System.Drawing.Size(27, 13);
            this.LbSalir.TabIndex = 125;
            this.LbSalir.Text = "Salir";
            // 
            // LbActualizar
            // 
            this.LbActualizar.AutoSize = true;
            this.LbActualizar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbActualizar.Enabled = false;
            this.LbActualizar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbActualizar.Location = new System.Drawing.Point(503, 157);
            this.LbActualizar.Name = "LbActualizar";
            this.LbActualizar.Size = new System.Drawing.Size(53, 13);
            this.LbActualizar.TabIndex = 124;
            this.LbActualizar.Text = "Actualizar";
            this.LbActualizar.Visible = false;
            // 
            // BtSalir
            // 
            this.BtSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtSalir.BackgroundImage")));
            this.BtSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtSalir.Location = new System.Drawing.Point(502, 330);
            this.BtSalir.Name = "BtSalir";
            this.BtSalir.Size = new System.Drawing.Size(53, 48);
            this.BtSalir.TabIndex = 123;
            this.BtSalir.UseVisualStyleBackColor = true;
            this.BtSalir.Click += new System.EventHandler(this.BtSalir_Click_1);
            // 
            // BtActualizar
            // 
            this.BtActualizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtActualizar.BackgroundImage")));
            this.BtActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtActualizar.Enabled = false;
            this.BtActualizar.Location = new System.Drawing.Point(502, 108);
            this.BtActualizar.Name = "BtActualizar";
            this.BtActualizar.Size = new System.Drawing.Size(54, 46);
            this.BtActualizar.TabIndex = 122;
            this.BtActualizar.UseVisualStyleBackColor = true;
            this.BtActualizar.Visible = false;
            this.BtActualizar.Click += new System.EventHandler(this.BtActualizar_Click_1);
            // 
            // facturacompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(585, 578);
            this.Controls.Add(this.BtModificar);
            this.Controls.Add(this.LbSalir);
            this.Controls.Add(this.LbActualizar);
            this.Controls.Add(this.BtSalir);
            this.Controls.Add(this.BtActualizar);
            this.Controls.Add(this.LbCancelar);
            this.Controls.Add(this.BtCancelar);
            this.Controls.Add(this.LbNuevo);
            this.Controls.Add(this.LbBuscar);
            this.Controls.Add(this.BtConsultar);
            this.Controls.Add(this.BtNuevo);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.TxbCodigoProducto);
            this.Controls.Add(this.LbCodigoProducto);
            this.Controls.Add(this.TxbFechaFactura);
            this.Controls.Add(this.TxbCod_FacturaC);
            this.Controls.Add(this.TxbCodProvedor);
            this.Controls.Add(this.LbCodigoProvedor);
            this.Controls.Add(this.LbFechaFactura);
            this.Controls.Add(this.LbCodigoFCompra);
            this.Controls.Add(this.pictureBox2);
            this.Name = "facturacompra";
            this.Text = "facturacompra";
            this.Load += new System.EventHandler(this.facturacompra_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LbCodigoFCompra;
        private System.Windows.Forms.Label LbFechaFactura;
        private System.Windows.Forms.Label LbCodigoProvedor;
        private System.Windows.Forms.TextBox TxbCodProvedor;
        private System.Windows.Forms.TextBox TxbCod_FacturaC;
        private System.Windows.Forms.TextBox TxbFechaFactura;
        private System.Windows.Forms.Label LbCodigoProducto;
        private System.Windows.Forms.TextBox TxbCodigoProducto;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label LbCancelar;
        private System.Windows.Forms.Button BtCancelar;
        private System.Windows.Forms.Label LbNuevo;
        private System.Windows.Forms.Label LbBuscar;
        private System.Windows.Forms.Button BtConsultar;
        private System.Windows.Forms.Button BtNuevo;
        private System.Windows.Forms.Button BtModificar;
        private System.Windows.Forms.Label LbSalir;
        private System.Windows.Forms.Label LbActualizar;
        private System.Windows.Forms.Button BtSalir;
        private System.Windows.Forms.Button BtActualizar;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}