﻿namespace DR_Doctor_Lemus
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BtSalir = new System.Windows.Forms.Button();
            this.BtFac_Venta = new System.Windows.Forms.Button();
            this.BtDevolucion = new System.Windows.Forms.Button();
            this.BtProveedor = new System.Windows.Forms.Button();
            this.BtImportacion = new System.Windows.Forms.Button();
            this.BtEmpleado = new System.Windows.Forms.Button();
            this.BtBodeja = new System.Windows.Forms.Button();
            this.BtProductos = new System.Windows.Forms.Button();
            this.BtFac_Compra = new System.Windows.Forms.Button();
            this.BtEntrega = new System.Windows.Forms.Button();
            this.BtCliente = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Derive Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label11.Location = new System.Drawing.Point(647, 500);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 16);
            this.label11.TabIndex = 60;
            this.label11.Text = "Devolución";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Derive Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(647, 375);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 16);
            this.label8.TabIndex = 59;
            this.label8.Text = "Estanteria";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Derive Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.label6.Location = new System.Drawing.Point(659, 249);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 16);
            this.label6.TabIndex = 57;
            this.label6.Text = "Productos";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Derive Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(393, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 16);
            this.label5.TabIndex = 56;
            this.label5.Text = "Factura Venta";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Derive Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label4.Location = new System.Drawing.Point(265, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 16);
            this.label4.TabIndex = 55;
            this.label4.Text = "Entrega";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Derive Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(37, 506);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 16);
            this.label3.TabIndex = 54;
            this.label3.Text = "Importación";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Derive Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label2.Location = new System.Drawing.Point(25, 370);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 16);
            this.label2.TabIndex = 53;
            this.label2.Text = "Factura Compra";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Derive Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(37, 249);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 52;
            this.label1.Text = "Provedor";
            // 
            // BtSalir
            // 
            this.BtSalir.BackColor = System.Drawing.Color.Transparent;
            this.BtSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtSalir.BackgroundImage")));
            this.BtSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtSalir.Location = new System.Drawing.Point(650, 560);
            this.BtSalir.Name = "BtSalir";
            this.BtSalir.Size = new System.Drawing.Size(67, 73);
            this.BtSalir.TabIndex = 47;
            this.BtSalir.UseVisualStyleBackColor = false;
            this.BtSalir.Click += new System.EventHandler(this.BtSalir_Click);
            // 
            // BtFac_Venta
            // 
            this.BtFac_Venta.BackColor = System.Drawing.Color.Transparent;
            this.BtFac_Venta.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtFac_Venta.BackgroundImage")));
            this.BtFac_Venta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtFac_Venta.Location = new System.Drawing.Point(407, 34);
            this.BtFac_Venta.Name = "BtFac_Venta";
            this.BtFac_Venta.Size = new System.Drawing.Size(83, 72);
            this.BtFac_Venta.TabIndex = 46;
            this.BtFac_Venta.UseVisualStyleBackColor = false;
            this.BtFac_Venta.Click += new System.EventHandler(this.BtFac_Venta_Click);
            // 
            // BtDevolucion
            // 
            this.BtDevolucion.BackColor = System.Drawing.Color.Transparent;
            this.BtDevolucion.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtDevolucion.BackgroundImage")));
            this.BtDevolucion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtDevolucion.Location = new System.Drawing.Point(650, 420);
            this.BtDevolucion.Name = "BtDevolucion";
            this.BtDevolucion.Size = new System.Drawing.Size(89, 77);
            this.BtDevolucion.TabIndex = 45;
            this.BtDevolucion.UseVisualStyleBackColor = false;
            this.BtDevolucion.Click += new System.EventHandler(this.BtDevolucion_Click);
            // 
            // BtProveedor
            // 
            this.BtProveedor.BackColor = System.Drawing.Color.Transparent;
            this.BtProveedor.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtProveedor.BackgroundImage")));
            this.BtProveedor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtProveedor.Location = new System.Drawing.Point(40, 155);
            this.BtProveedor.Name = "BtProveedor";
            this.BtProveedor.Size = new System.Drawing.Size(88, 79);
            this.BtProveedor.TabIndex = 44;
            this.BtProveedor.UseVisualStyleBackColor = false;
            this.BtProveedor.Click += new System.EventHandler(this.BtProveedor_Click);
            // 
            // BtImportacion
            // 
            this.BtImportacion.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtImportacion.BackgroundImage")));
            this.BtImportacion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtImportacion.Location = new System.Drawing.Point(40, 413);
            this.BtImportacion.Name = "BtImportacion";
            this.BtImportacion.Size = new System.Drawing.Size(88, 90);
            this.BtImportacion.TabIndex = 43;
            this.BtImportacion.UseVisualStyleBackColor = true;
            this.BtImportacion.Click += new System.EventHandler(this.BtImportacion_Click);
            // 
            // BtEmpleado
            // 
            this.BtEmpleado.BackColor = System.Drawing.Color.Transparent;
            this.BtEmpleado.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtEmpleado.BackgroundImage")));
            this.BtEmpleado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtEmpleado.Location = new System.Drawing.Point(407, 522);
            this.BtEmpleado.Name = "BtEmpleado";
            this.BtEmpleado.Size = new System.Drawing.Size(89, 81);
            this.BtEmpleado.TabIndex = 42;
            this.BtEmpleado.UseVisualStyleBackColor = false;
            this.BtEmpleado.Click += new System.EventHandler(this.BtEmpleado_Click);
            // 
            // BtBodeja
            // 
            this.BtBodeja.BackColor = System.Drawing.Color.Transparent;
            this.BtBodeja.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtBodeja.BackgroundImage")));
            this.BtBodeja.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtBodeja.Location = new System.Drawing.Point(650, 285);
            this.BtBodeja.Name = "BtBodeja";
            this.BtBodeja.Size = new System.Drawing.Size(85, 87);
            this.BtBodeja.TabIndex = 41;
            this.BtBodeja.UseVisualStyleBackColor = false;
            this.BtBodeja.Click += new System.EventHandler(this.BtBodeja_Click);
            // 
            // BtProductos
            // 
            this.BtProductos.BackColor = System.Drawing.Color.Transparent;
            this.BtProductos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtProductos.BackgroundImage")));
            this.BtProductos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtProductos.Location = new System.Drawing.Point(654, 159);
            this.BtProductos.Name = "BtProductos";
            this.BtProductos.Size = new System.Drawing.Size(85, 87);
            this.BtProductos.TabIndex = 39;
            this.BtProductos.UseVisualStyleBackColor = false;
            this.BtProductos.Click += new System.EventHandler(this.BtProductos_Click);
            // 
            // BtFac_Compra
            // 
            this.BtFac_Compra.BackColor = System.Drawing.Color.Transparent;
            this.BtFac_Compra.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtFac_Compra.BackgroundImage")));
            this.BtFac_Compra.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtFac_Compra.Location = new System.Drawing.Point(40, 290);
            this.BtFac_Compra.Name = "BtFac_Compra";
            this.BtFac_Compra.Size = new System.Drawing.Size(88, 77);
            this.BtFac_Compra.TabIndex = 38;
            this.BtFac_Compra.UseVisualStyleBackColor = false;
            this.BtFac_Compra.Click += new System.EventHandler(this.BtFac_Compra_Click);
            // 
            // BtEntrega
            // 
            this.BtEntrega.BackColor = System.Drawing.Color.Transparent;
            this.BtEntrega.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtEntrega.BackgroundImage")));
            this.BtEntrega.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtEntrega.Location = new System.Drawing.Point(256, 36);
            this.BtEntrega.Name = "BtEntrega";
            this.BtEntrega.Size = new System.Drawing.Size(87, 73);
            this.BtEntrega.TabIndex = 37;
            this.BtEntrega.UseVisualStyleBackColor = false;
            this.BtEntrega.Click += new System.EventHandler(this.BtEntrega_Click);
            // 
            // BtCliente
            // 
            this.BtCliente.BackColor = System.Drawing.Color.Transparent;
            this.BtCliente.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtCliente.BackgroundImage")));
            this.BtCliente.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtCliente.Location = new System.Drawing.Point(256, 524);
            this.BtCliente.Name = "BtCliente";
            this.BtCliente.Size = new System.Drawing.Size(90, 77);
            this.BtCliente.TabIndex = 36;
            this.BtCliente.UseVisualStyleBackColor = false;
            this.BtCliente.Click += new System.EventHandler(this.BtCliente_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(219, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(320, 133);
            this.pictureBox1.TabIndex = 48;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(631, 112);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(125, 433);
            this.pictureBox2.TabIndex = 49;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox3.Location = new System.Drawing.Point(219, 500);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(320, 133);
            this.pictureBox3.TabIndex = 50;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox4.Location = new System.Drawing.Point(16, 138);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(144, 413);
            this.pictureBox4.TabIndex = 51;
            this.pictureBox4.TabStop = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Derive Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(658, 636);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 16);
            this.label12.TabIndex = 61;
            this.label12.Text = "Salir";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Derive Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label9.Location = new System.Drawing.Point(404, 626);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 16);
            this.label9.TabIndex = 62;
            this.label9.Text = "Empleados";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Derive Unicode", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label10.Location = new System.Drawing.Point(265, 626);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(64, 16);
            this.label10.TabIndex = 63;
            this.label10.Text = "Cliente";
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(782, 655);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtSalir);
            this.Controls.Add(this.BtFac_Venta);
            this.Controls.Add(this.BtDevolucion);
            this.Controls.Add(this.BtProveedor);
            this.Controls.Add(this.BtImportacion);
            this.Controls.Add(this.BtEmpleado);
            this.Controls.Add(this.BtBodeja);
            this.Controls.Add(this.BtProductos);
            this.Controls.Add(this.BtFac_Compra);
            this.Controls.Add(this.BtEntrega);
            this.Controls.Add(this.BtCliente);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox4);
            this.Name = "Principal";
            this.Text = "Principal";
            this.Load += new System.EventHandler(this.Principal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtSalir;
        private System.Windows.Forms.Button BtFac_Venta;
        private System.Windows.Forms.Button BtDevolucion;
        private System.Windows.Forms.Button BtProveedor;
        private System.Windows.Forms.Button BtImportacion;
        private System.Windows.Forms.Button BtEmpleado;
        private System.Windows.Forms.Button BtBodeja;
        private System.Windows.Forms.Button BtProductos;
        private System.Windows.Forms.Button BtFac_Compra;
        private System.Windows.Forms.Button BtEntrega;
        private System.Windows.Forms.Button BtCliente;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}