﻿namespace DR_Doctor_Lemus
{
    partial class facturaventa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(facturaventa));
            this.TxbCodigoCliente = new System.Windows.Forms.TextBox();
            this.TxbFechaFactura = new System.Windows.Forms.TextBox();
            this.TxbCodigoFVenta = new System.Windows.Forms.TextBox();
            this.LbCodigoCliente = new System.Windows.Forms.Label();
            this.LbFechaFactura = new System.Windows.Forms.Label();
            this.LbCodigoFVenta = new System.Windows.Forms.Label();
            this.TxbCodgioEntrega = new System.Windows.Forms.TextBox();
            this.TxbCodigoEmpleado = new System.Windows.Forms.TextBox();
            this.LbCodgioEntrega = new System.Windows.Forms.Label();
            this.LbCodigoEmpleado = new System.Windows.Forms.Label();
            this.LbCancelar = new System.Windows.Forms.Label();
            this.BtCancelar = new System.Windows.Forms.Button();
            this.LbNuevo = new System.Windows.Forms.Label();
            this.LbBuscar = new System.Windows.Forms.Label();
            this.BtConsultar = new System.Windows.Forms.Button();
            this.BtNuevo = new System.Windows.Forms.Button();
            this.BtModificar = new System.Windows.Forms.Button();
            this.LbSalir = new System.Windows.Forms.Label();
            this.LbActualizar = new System.Windows.Forms.Label();
            this.BtSalir = new System.Windows.Forms.Button();
            this.BtActualizar = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // TxbCodigoCliente
            // 
            this.TxbCodigoCliente.Enabled = false;
            this.TxbCodigoCliente.Location = new System.Drawing.Point(307, 185);
            this.TxbCodigoCliente.Name = "TxbCodigoCliente";
            this.TxbCodigoCliente.Size = new System.Drawing.Size(126, 20);
            this.TxbCodigoCliente.TabIndex = 20;
            this.TxbCodigoCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCodigoCliente_KeyPress);
            // 
            // TxbFechaFactura
            // 
            this.TxbFechaFactura.Enabled = false;
            this.TxbFechaFactura.Location = new System.Drawing.Point(307, 94);
            this.TxbFechaFactura.Name = "TxbFechaFactura";
            this.TxbFechaFactura.Size = new System.Drawing.Size(126, 20);
            this.TxbFechaFactura.TabIndex = 19;
            this.TxbFechaFactura.TextChanged += new System.EventHandler(this.TxbFechaFactura_TextChanged);
            // 
            // TxbCodigoFVenta
            // 
            this.TxbCodigoFVenta.Location = new System.Drawing.Point(322, 30);
            this.TxbCodigoFVenta.Name = "TxbCodigoFVenta";
            this.TxbCodigoFVenta.Size = new System.Drawing.Size(122, 20);
            this.TxbCodigoFVenta.TabIndex = 18;
            this.TxbCodigoFVenta.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCodigoFVenta_KeyPress);
            // 
            // LbCodigoCliente
            // 
            this.LbCodigoCliente.AutoSize = true;
            this.LbCodigoCliente.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigoCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigoCliente.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodigoCliente.Location = new System.Drawing.Point(175, 181);
            this.LbCodigoCliente.Name = "LbCodigoCliente";
            this.LbCodigoCliente.Size = new System.Drawing.Size(126, 20);
            this.LbCodigoCliente.TabIndex = 14;
            this.LbCodigoCliente.Text = "Codigo Cliente";
            // 
            // LbFechaFactura
            // 
            this.LbFechaFactura.AutoSize = true;
            this.LbFechaFactura.BackColor = System.Drawing.Color.Transparent;
            this.LbFechaFactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbFechaFactura.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbFechaFactura.Location = new System.Drawing.Point(175, 95);
            this.LbFechaFactura.Name = "LbFechaFactura";
            this.LbFechaFactura.Size = new System.Drawing.Size(126, 20);
            this.LbFechaFactura.TabIndex = 13;
            this.LbFechaFactura.Text = "Fecha Factura";
            // 
            // LbCodigoFVenta
            // 
            this.LbCodigoFVenta.AutoSize = true;
            this.LbCodigoFVenta.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigoFVenta.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigoFVenta.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodigoFVenta.Location = new System.Drawing.Point(132, 30);
            this.LbCodigoFVenta.Name = "LbCodigoFVenta";
            this.LbCodigoFVenta.Size = new System.Drawing.Size(185, 20);
            this.LbCodigoFVenta.TabIndex = 12;
            this.LbCodigoFVenta.Text = "Codigo Factura Venta";
            // 
            // TxbCodgioEntrega
            // 
            this.TxbCodgioEntrega.Enabled = false;
            this.TxbCodgioEntrega.Location = new System.Drawing.Point(307, 264);
            this.TxbCodgioEntrega.Name = "TxbCodgioEntrega";
            this.TxbCodgioEntrega.Size = new System.Drawing.Size(126, 20);
            this.TxbCodgioEntrega.TabIndex = 54;
            // 
            // TxbCodigoEmpleado
            // 
            this.TxbCodigoEmpleado.Enabled = false;
            this.TxbCodigoEmpleado.Location = new System.Drawing.Point(307, 331);
            this.TxbCodigoEmpleado.Name = "TxbCodigoEmpleado";
            this.TxbCodigoEmpleado.Size = new System.Drawing.Size(126, 20);
            this.TxbCodigoEmpleado.TabIndex = 55;
            this.TxbCodigoEmpleado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCodigoEmpleado_KeyPress);
            // 
            // LbCodgioEntrega
            // 
            this.LbCodgioEntrega.AutoSize = true;
            this.LbCodgioEntrega.BackColor = System.Drawing.Color.Transparent;
            this.LbCodgioEntrega.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodgioEntrega.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodgioEntrega.Location = new System.Drawing.Point(175, 265);
            this.LbCodgioEntrega.Name = "LbCodgioEntrega";
            this.LbCodgioEntrega.Size = new System.Drawing.Size(134, 20);
            this.LbCodgioEntrega.TabIndex = 56;
            this.LbCodgioEntrega.Text = "Codigo Entrega";
            // 
            // LbCodigoEmpleado
            // 
            this.LbCodigoEmpleado.AutoSize = true;
            this.LbCodigoEmpleado.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigoEmpleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigoEmpleado.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodigoEmpleado.Location = new System.Drawing.Point(153, 331);
            this.LbCodigoEmpleado.Name = "LbCodigoEmpleado";
            this.LbCodigoEmpleado.Size = new System.Drawing.Size(150, 20);
            this.LbCodigoEmpleado.TabIndex = 57;
            this.LbCodigoEmpleado.Text = "Codigo Empleado";
            // 
            // LbCancelar
            // 
            this.LbCancelar.AutoSize = true;
            this.LbCancelar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbCancelar.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCancelar.Location = new System.Drawing.Point(37, 354);
            this.LbCancelar.Name = "LbCancelar";
            this.LbCancelar.Size = new System.Drawing.Size(49, 13);
            this.LbCancelar.TabIndex = 127;
            this.LbCancelar.Text = "Cancelar";
            // 
            // BtCancelar
            // 
            this.BtCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtCancelar.BackgroundImage")));
            this.BtCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtCancelar.Location = new System.Drawing.Point(32, 287);
            this.BtCancelar.Name = "BtCancelar";
            this.BtCancelar.Size = new System.Drawing.Size(55, 53);
            this.BtCancelar.TabIndex = 126;
            this.BtCancelar.UseVisualStyleBackColor = false;
            this.BtCancelar.Click += new System.EventHandler(this.BtCancelar_Click);
            // 
            // LbNuevo
            // 
            this.LbNuevo.AutoSize = true;
            this.LbNuevo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbNuevo.Enabled = false;
            this.LbNuevo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbNuevo.Location = new System.Drawing.Point(35, 260);
            this.LbNuevo.Name = "LbNuevo";
            this.LbNuevo.Size = new System.Drawing.Size(39, 13);
            this.LbNuevo.TabIndex = 125;
            this.LbNuevo.Text = "Nuevo";
            this.LbNuevo.Visible = false;
            // 
            // LbBuscar
            // 
            this.LbBuscar.AutoSize = true;
            this.LbBuscar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbBuscar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LbBuscar.Location = new System.Drawing.Point(34, 146);
            this.LbBuscar.Name = "LbBuscar";
            this.LbBuscar.Size = new System.Drawing.Size(40, 13);
            this.LbBuscar.TabIndex = 124;
            this.LbBuscar.Text = "Buscar";
            // 
            // BtConsultar
            // 
            this.BtConsultar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtConsultar.BackgroundImage")));
            this.BtConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtConsultar.Location = new System.Drawing.Point(31, 81);
            this.BtConsultar.Name = "BtConsultar";
            this.BtConsultar.Size = new System.Drawing.Size(54, 50);
            this.BtConsultar.TabIndex = 123;
            this.BtConsultar.UseVisualStyleBackColor = true;
            this.BtConsultar.Click += new System.EventHandler(this.BtConsultar_Click_1);
            // 
            // BtNuevo
            // 
            this.BtNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtNuevo.BackgroundImage")));
            this.BtNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtNuevo.Enabled = false;
            this.BtNuevo.Location = new System.Drawing.Point(32, 198);
            this.BtNuevo.Name = "BtNuevo";
            this.BtNuevo.Size = new System.Drawing.Size(54, 49);
            this.BtNuevo.TabIndex = 122;
            this.BtNuevo.UseVisualStyleBackColor = true;
            this.BtNuevo.Visible = false;
            this.BtNuevo.Click += new System.EventHandler(this.BtNuevo_Click_1);
            // 
            // BtModificar
            // 
            this.BtModificar.BackColor = System.Drawing.Color.Transparent;
            this.BtModificar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtModificar.BackgroundImage")));
            this.BtModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtModificar.Enabled = false;
            this.BtModificar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtModificar.Location = new System.Drawing.Point(506, 185);
            this.BtModificar.Name = "BtModificar";
            this.BtModificar.Size = new System.Drawing.Size(53, 56);
            this.BtModificar.TabIndex = 132;
            this.BtModificar.UseVisualStyleBackColor = false;
            this.BtModificar.Visible = false;
            this.BtModificar.Click += new System.EventHandler(this.BtModificar_Click);
            // 
            // LbSalir
            // 
            this.LbSalir.AutoSize = true;
            this.LbSalir.BackColor = System.Drawing.Color.DimGray;
            this.LbSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.LbSalir.Location = new System.Drawing.Point(519, 372);
            this.LbSalir.Name = "LbSalir";
            this.LbSalir.Size = new System.Drawing.Size(27, 13);
            this.LbSalir.TabIndex = 131;
            this.LbSalir.Text = "Salir";
            // 
            // LbActualizar
            // 
            this.LbActualizar.AutoSize = true;
            this.LbActualizar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbActualizar.Enabled = false;
            this.LbActualizar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbActualizar.Location = new System.Drawing.Point(507, 134);
            this.LbActualizar.Name = "LbActualizar";
            this.LbActualizar.Size = new System.Drawing.Size(53, 13);
            this.LbActualizar.TabIndex = 130;
            this.LbActualizar.Text = "Actualizar";
            this.LbActualizar.Visible = false;
            // 
            // BtSalir
            // 
            this.BtSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtSalir.BackgroundImage")));
            this.BtSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtSalir.Location = new System.Drawing.Point(506, 307);
            this.BtSalir.Name = "BtSalir";
            this.BtSalir.Size = new System.Drawing.Size(53, 48);
            this.BtSalir.TabIndex = 129;
            this.BtSalir.UseVisualStyleBackColor = true;
            this.BtSalir.Click += new System.EventHandler(this.BtSalir_Click_1);
            // 
            // BtActualizar
            // 
            this.BtActualizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtActualizar.BackgroundImage")));
            this.BtActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtActualizar.Enabled = false;
            this.BtActualizar.Location = new System.Drawing.Point(506, 85);
            this.BtActualizar.Name = "BtActualizar";
            this.BtActualizar.Size = new System.Drawing.Size(54, 46);
            this.BtActualizar.TabIndex = 128;
            this.BtActualizar.UseVisualStyleBackColor = true;
            this.BtActualizar.Visible = false;
            this.BtActualizar.Click += new System.EventHandler(this.BtActualizar_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(136, 411);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(308, 137);
            this.dataGridView1.TabIndex = 134;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(121, -7);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(343, 607);
            this.pictureBox2.TabIndex = 133;
            this.pictureBox2.TabStop = false;
            // 
            // facturaventa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(585, 578);
            this.Controls.Add(this.BtModificar);
            this.Controls.Add(this.LbSalir);
            this.Controls.Add(this.LbActualizar);
            this.Controls.Add(this.BtSalir);
            this.Controls.Add(this.BtActualizar);
            this.Controls.Add(this.LbCancelar);
            this.Controls.Add(this.BtCancelar);
            this.Controls.Add(this.LbNuevo);
            this.Controls.Add(this.LbBuscar);
            this.Controls.Add(this.BtConsultar);
            this.Controls.Add(this.BtNuevo);
            this.Controls.Add(this.LbCodigoEmpleado);
            this.Controls.Add(this.LbCodgioEntrega);
            this.Controls.Add(this.TxbCodigoEmpleado);
            this.Controls.Add(this.TxbCodgioEntrega);
            this.Controls.Add(this.TxbCodigoCliente);
            this.Controls.Add(this.TxbFechaFactura);
            this.Controls.Add(this.TxbCodigoFVenta);
            this.Controls.Add(this.LbCodigoCliente);
            this.Controls.Add(this.LbFechaFactura);
            this.Controls.Add(this.LbCodigoFVenta);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.pictureBox2);
            this.Name = "facturaventa";
            this.Text = "facturaventa";
            this.Load += new System.EventHandler(this.facturaventa_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxbCodigoCliente;
        private System.Windows.Forms.TextBox TxbFechaFactura;
        private System.Windows.Forms.TextBox TxbCodigoFVenta;
        private System.Windows.Forms.Label LbCodigoCliente;
        private System.Windows.Forms.Label LbFechaFactura;
        private System.Windows.Forms.Label LbCodigoFVenta;
        private System.Windows.Forms.TextBox TxbCodgioEntrega;
        private System.Windows.Forms.TextBox TxbCodigoEmpleado;
        private System.Windows.Forms.Label LbCodgioEntrega;
        private System.Windows.Forms.Label LbCodigoEmpleado;
        private System.Windows.Forms.Label LbCancelar;
        private System.Windows.Forms.Button BtCancelar;
        private System.Windows.Forms.Label LbNuevo;
        private System.Windows.Forms.Label LbBuscar;
        private System.Windows.Forms.Button BtConsultar;
        private System.Windows.Forms.Button BtNuevo;
        private System.Windows.Forms.Button BtModificar;
        private System.Windows.Forms.Label LbSalir;
        private System.Windows.Forms.Label LbActualizar;
        private System.Windows.Forms.Button BtSalir;
        private System.Windows.Forms.Button BtActualizar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}