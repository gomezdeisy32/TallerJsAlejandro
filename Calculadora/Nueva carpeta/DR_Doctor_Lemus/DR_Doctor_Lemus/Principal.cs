﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace DR_Doctor_Lemus
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();
        }

        System.Data.SqlClient.SqlConnection con;
             
        private void BtSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            
                con = new System.Data.SqlClient.SqlConnection();
                con.ConnectionString = "Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True";

            
        }

        private void BtEntrega_Click(object sender, EventArgs e)
        {
            Entrega frm = new Entrega();
            this.Hide();
            frm.Show();
        }

        private void BtFac_Venta_Click(object sender, EventArgs e)
        {
             facturaventa frm = new facturaventa();
             this.Hide();
             frm.Show();
        
        }

        private void BtEmpleado_Click(object sender, EventArgs e)
        {
            empleado frm = new empleado();
            this.Hide();
            frm.Show();
        }

        private void BtCliente_Click(object sender, EventArgs e)
        {
            cliente frm = new cliente();
            this.Hide();
            frm.Show();
        }

        private void BtImportacion_Click(object sender, EventArgs e)
        {
            importacion frm = new importacion();
            this.Hide();
            frm.Show();
        }

        private void BtFac_Compra_Click(object sender, EventArgs e)
        {
            facturacompra frm = new facturacompra();
            this.Hide();
            frm.Show();
        }

        private void BtProveedor_Click(object sender, EventArgs e)
        {
            Provedor frm = new Provedor();
            this.Hide();
            frm.Show();
        }

        private void BtDevolucion_Click(object sender, EventArgs e)
        {
            Devolucion frm = new Devolucion();
            this.Hide();
            frm.Show();
        }

        private void BtBodeja_Click(object sender, EventArgs e)
        {
            Bodega frm = new Bodega();
            this.Hide();
            frm.Show();
        }

        

        private void BtProductos_Click(object sender, EventArgs e)
        {
            producto frm = new producto();
            this.Hide();
            frm.Show();
        }

        
    }
}
