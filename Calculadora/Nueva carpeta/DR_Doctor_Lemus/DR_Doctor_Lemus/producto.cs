﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DR_Doctor_Lemus;
using System.Data.SqlClient;


namespace DR_Doctor_Lemus
{
    public partial class producto : Form
    {
        public producto()
        {
            InitializeComponent();
        }



        private void BtSalir_Click(object sender, EventArgs e)
        {
            Principal producto = new Principal();
            producto.Show();
            this.Hide();
        }

        private void BtNuevo_Click(object sender, EventArgs e)
        {
            // comprobar que todos los datos ingresaron
            if ((TxbCantidadPastillero.Text == "") || (TxbCodigoProducto.Text == "") || (TxbFechaVencimiento.Text == "")  || (TxbNombreProducto.Text == ""))
            {
                MessageBox.Show("ha dejado un espacio en blanco");
            }
            else
            {
                try
                {
                    //creamos el archivo de conexion y le damos la ruta
                    SqlConnection conex = new SqlConnection("Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                    // abrimos la conexion

                    conex.Open();
                    //creamos el comado de sql
                    SqlCommand cmd = new SqlCommand(" insert into PRODUCTOS(cantidadpastillero,codigoproducto,fechavencimiento,nombreproducto)values('" + TxbCantidadPastillero.Text + ",  '" + TxbCodigoProducto.Text + ", '" + TxbFechaVencimiento.Text + ", '" + TxbNombreProducto.Text +  "')");

                    //ejecutamos la conexion con la sentencia de sql
                    cmd.Connection = conex;
                    cmd.ExecuteNonQuery();
                    try
                    {
                        MessageBox.Show("registro exitoso");
                        //cerramos la conexion
                        conex.Close();
                        //limpiamos el formulario
                        TxbCantidadPastillero.Clear();
                        TxbCodigoProducto.Clear();
                        TxbFechaVencimiento.Clear();
                        TxbNombreProducto.Clear();
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("el reegistro ya existe");
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("no" + ex);

                }


            }
        }

        private void BtConsultar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            try
            {
                SqlCommand cmd = new SqlCommand
                ("select * from producto where CODIGOPRODUCTO ='" + TxbCodigoProducto.Text + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "producto");
                DataRow dro;
                dro = ds.Tables["producto"].Rows[0];
                TxbCantidadPastillero.Text = dro["CANTIDADPASTILLERO"].ToString();
                TxbFechaVencimiento.Text = dro["FECHAVENCIMIENTO"].ToString();
                TxbNombreProducto.Text = dro["NOMBREPRODUCTO"].ToString();
                BtActualizar.Enabled = true;
                BtActualizar.Visible = true;
                BtEliminar.Enabled = true;
                BtEliminar.Visible = true;
                 

            }
            catch
            {
                MessageBox.Show(" producto no encontrado");
            }
            finally
            {
                con.Close();
            }
        }

        private void BtActualizar_Click(object sender, EventArgs e)
        {
            //conexion a base de datos
            SqlConnection conn = new SqlConnection("Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            //abro conexion
            conn.Open();
            //buscamos el nombre y el registro a actualizar para verificar
            string consulta = "select CANTIDADPASTILLERO, NOMBREPRODUCTO  from IMPORTACION where nombreproducto = '" + TxbNombreProducto + "'";
            SqlCommand COMAND = new SqlCommand(consulta, conn);
            // El resultado lo guardaremos en una tabla

            DataTable tabla = new DataTable();
            // Usaremos un DataAdapter para leer los datos
            SqlDataAdapter AdaptadorTabla = new SqlDataAdapter(consulta, conn);
            //DataSet ds = new DataSet();
            // Llenamos la tabla con los datos leídos
            AdaptadorTabla.Fill(tabla);
            //guardo informacion en variables
            string cantidadpastillero = tabla.Rows[0]["CANTIDADPASTILLERO"].ToString();
            string nombreproducto = tabla.Rows[0]["NOMBREPRODUCTO"].ToString();
            // Creamos el cuadro de dialogo y guardamos la respuesta que da el usuario
            DialogResult respuesta = MessageBox.Show("Desea actualizar el registro de " + cantidadpastillero + " " + nombreproducto + ".", "actualizar", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            //Si acepto actualizar se ejecuta
            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {
                string StrComando = "DELETE FROM importacion WHERE nombreproducto =  '" + TxbNombreProducto + "'";
                SqlCommand COMANDO = new SqlCommand(StrComando, conn);
                COMANDO.ExecuteNonQuery();
                MessageBox.Show("El registro seactualizo de manera  EXITOSA");


            }
            else
            {
                MessageBox.Show("el registro no se actualizo");

            }

            TxbCantidadPastillero.Text = "";
            TxbCodigoProducto.Text = "";
            TxbFechaVencimiento.Text = "";
            TxbNombreProducto.Text = "";
        }

        private void BtEliminar_Click(object sender, EventArgs e)
        {
            //conexion a base de datos

            SqlConnection conn = new SqlConnection("Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");

            // abro conexion.

            conn.Open();

            // Buscamos la cantidad del registro a eliminar para verificar
            string consulta = "select CANTIDAD  from PRODUCTO where nombreproducto = '" + TxbNombreProducto.Text + "'";

            SqlCommand COMAND = new SqlCommand(consulta, conn);
            // El resultado lo guardaremos en una tabla

            DataTable tabla = new DataTable();
            // Usaremos un DataAdapter para leer los datos

            SqlDataAdapter AdaptadorTabla = new SqlDataAdapter(consulta, conn);
            //DataSet ds = new DataSet();
            // Llenamos la tabla con los datos leídos
            AdaptadorTabla.Fill(tabla);
            //guardo informacion en variables
            string cantidadpastillero = tabla.Rows[0]["CANTIDADPASTILLERO"].ToString();


            // Creamos el cuadro de dialogo y guardamos la respuesta que da el usuario
            DialogResult respuesta = MessageBox.Show("Desea eliminar el registro de " + cantidadpastillero + " .", "BORRAR", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            //Si acepto eliminar se ejecuta
            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {


                string StrComando = "DELETE FROM producto WHERE nombreproducto =  '" + TxbNombreProducto + "'";

                SqlCommand COMANDO = new SqlCommand(StrComando, conn);
                COMANDO.ExecuteNonQuery();


                MessageBox.Show("El registro se borro de manera  EXITOSA");


            }
            else
            {

                MessageBox.Show("el registro no se borro");
            }



            TxbCantidadPastillero.Text = "";
            TxbCodigoProducto.Text = "";
            TxbFechaVencimiento.Text = "";
            TxbNombreProducto.Text = "";

        }

        

     

        

        private void button1_Click(object sender, EventArgs e)
        {
            TxbCantidadPastillero.Visible = true;
            TxbNombreProducto.Enabled = true;
            TxbCodigoProducto.Visible = true;
            TxbFechaVencimiento.Visible = true;
            TxbNombreProducto.Visible = true;
            BtNuevo.Visible = true;
        }

        

        private void TxbNombreProducto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.sololetras(e);
        }

        private void TxbCodigoProducto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void limpiar()
        {
            TxbCantidadPastillero.Clear();
            TxbCodigoProducto.Clear();
            TxbFechaVencimiento.Clear();
            TxbNombreProducto.Clear();
            BtActualizar.Visible = false;
            BtNuevo.Visible = false;
            BtModificar.Visible = false;


        }
        private void desabilitar()
        {

            TxbCantidadPastillero.Enabled = false;
            TxbFechaVencimiento.Enabled = false;
            TxbNombreProducto.Enabled = false;
            BtActualizar.Enabled = false;
            BtNuevo.Enabled = false;
            BtModificar.Enabled = false;

        }
        private void habilitar()
        {
            TxbCantidadPastillero.Enabled = true;
            TxbFechaVencimiento.Enabled = true;
            TxbNombreProducto.Enabled = true;
            BtActualizar.Enabled = true;
            BtNuevo.Enabled = true;
            BtModificar.Enabled = true;

        }

        private void BtConsultar_Click_1(object sender, EventArgs e)
        {
            if (TxbCodigoProducto.Text == "")
            {

                MessageBox.Show("el campo no puede estar vacio");
                TxbCodigoProducto.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                try
                {
                    SqlCommand cmd = new SqlCommand
                    ("select * from PRODUCTO where CODIGOPRODUCTO ='" + TxbCodigoProducto.Text + "' ", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "PRODUCTO");
                    DataRow dro;
                    dro = ds.Tables["PRODUCTO"].Rows[0];
                    TxbNombreProducto.Text = dro["NOMBREPRODUCTO"].ToString();
                    TxbFechaVencimiento.Text = dro["FECHAVENCIMIENTO"].ToString();
                    TxbCantidadPastillero.Text = dro["CANTIDADPASTILLERO"].ToString();
                    BtActualizar.Enabled = true;
                    BtActualizar.Visible = true;

                }
                catch
                {
                    //MessageBox.Show(" usuario no encontrado")
                    const string mensaje = "producto no encontrado desea crearlo si/no";
                    const string titulo = "consulta";
                    var resultado = MessageBox.Show(mensaje, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (resultado == DialogResult.No)
                    {
                        TxbCodigoProducto.Clear();
                        TxbCodigoProducto.Focus();
                    }
                    else
                    {


                        TxbCodigoProducto.Enabled = false;
                        TxbCantidadPastillero.Clear();
                        TxbFechaVencimiento.Clear();
                        TxbNombreProducto.Clear();
                        TxbNombreProducto.Enabled = true;
                        TxbFechaVencimiento.Enabled = true;
                        TxbCantidadPastillero.Enabled = true;

                        BtNuevo.Visible = true;
                        BtNuevo.Enabled = true;
                        BtConsultar.Enabled = false;

                    }

                }

                finally
                {
                    con.Close();
                }

            }
        }

        private void BtNuevo_Click_1(object sender, EventArgs e)
        {
            if (TxbCodigoProducto.Text == "")
            {
                MessageBox.Show("el campo apellido esta  vacion");
                TxbCodigoProducto.Focus();


            }
            else if (TxbNombreProducto.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbNombreProducto.Focus();
            }
            else if (TxbFechaVencimiento.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbFechaVencimiento.Focus();
            }
            else if (TxbCantidadPastillero.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbCantidadPastillero.Focus();
            }

            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");


                SqlCommand cmd = new SqlCommand("insert into PRODUCTO(CODIGOPRODUCTO, NOMBREPRODUCTO,FECHAVENCIMIENTO ,CANTIDADPASTILLERO)Values('" + Convert.ToInt16(TxbCodigoProducto.Text) + "','" + TxbNombreProducto.Text + "','" + TxbFechaVencimiento.Text + "', '" + Convert.ToInt16(TxbCantidadPastillero.Text) + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se   agrego con exito ");
            }
        }

        private void BtActualizar_Click_1(object sender, EventArgs e)
        {
            habilitar();
            BtActualizar.Enabled = false;
            TxbCodigoProducto.Enabled = false;
            BtModificar.Enabled = true;
            BtModificar.Visible = true;
        }

        private void BtModificar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");

            SqlCommand cmd = new SqlCommand("update PRODUCTO set NOMBREPRODUCTO ='" + TxbNombreProducto.Text + "', FECHAVENCIMIENTO = '" + TxbFechaVencimiento.Text + "', CANTIDADPASTILLERO = '" + TxbCantidadPastillero.Text + "' where CODIGOPRODUCTO = '" + TxbCodigoProducto.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            MessageBox.Show("se modifico  correctamente");
            desabilitar();
            limpiar();
            TxbCodigoProducto.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtCancelar_Click(object sender, EventArgs e)
        {
            desabilitar();
            limpiar();
            TxbCodigoProducto.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtSalir_Click_1(object sender, EventArgs e)
        {
            Principal producto = new Principal();
            producto.Show();
            this.Hide();
        }

        private void BtEliminar_Click_1(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            //SqlConnection con = new SqlConnection("Data Source=|DataDirectory|=Ejercicio_Makia.sdf;Max Database Size=2047");

            SqlCommand cmd = new SqlCommand
            ("Delete from PRODUCTO where CODIGOPRODUCTO ='" + TxbCodigoProducto.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();
            desabilitar();
            MessageBox.Show("Registro Eliminado");
        }

        private void producto_Load(object sender, EventArgs e)
        {
            Conexion objeto = new Conexion();
            objeto.ActualizarGrid(this.dataGridView1, "select * from PRODUCTO");
        }

        private void TxbCantidadPastillero_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        
    }
}
