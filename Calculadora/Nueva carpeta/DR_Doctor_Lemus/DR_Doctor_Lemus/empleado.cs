﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DR_Doctor_Lemus;
using System.Data.SqlClient;



namespace DR_Doctor_Lemus
{
    public partial class empleado : Form
    {
        public empleado()
        {
            InitializeComponent();
        }

        private void BtSalir_Click(object sender, EventArgs e)
        {
            Principal empleado = new Principal();
            empleado.Show();
            this.Hide();
        }

        private void BtNuevo_Click(object sender, EventArgs e)
        {
            // comprobar que todos los datos ingresaron
            if ((TxbCodigoEmpleado.Text == "") || (TxbNombre.Text == "") || (TxbApellido.Text == "") || (TxbDireccion.Text == "") || (TxbNumDocumento.Text == "") || (TxbTelefono.Text == "") || (TxbCelular.Text == ""))
            {
                MessageBox.Show("ha dejado un espacio en blanco");
            }
            else 
            {
                try
                {
                    //creamos el archivo de conexion y le damos la ruta
                    SqlConnection conex = new SqlConnection("Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                    // abrimos la conexion

                    conex.Open();
                    //creamos el comado de sql
                    SqlCommand cmd = new SqlCommand(" insert into EMPLEADOS(codigoempleado,apelido,nombre,direccion,numerodocumento,telefono,celular)values('" + TxbApellido.Text + ", '" + TxbCelular.Text + ", '" + TxbCodigoEmpleado.Text + ", '" + TxbDireccion.Text + ", '" + TxbNombre.Text + ", '" + TxbNumDocumento.Text + ", '" + TxbTelefono.Text + "')");
                    //ejecutamos la conexion con la sentencia de sql
                    cmd.Connection = conex;
                    cmd.ExecuteNonQuery();
                    try
                    {
                        MessageBox.Show("registro exitoso");
                        //cerramos la conexion
                        conex.Close();
                        //limpiamos el formulario
                        TxbTelefono.Clear();
                        TxbNumDocumento.Clear();
                        TxbNombre.Clear();
                        TxbDireccion.Clear();
                        TxbCodigoEmpleado.Clear();
                        TxbCelular.Clear();
                        TxbApellido.Clear();
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("el reegistro ya existe");
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("no" + ex);

                }

 
                }

    
        }

        private void BtConsultar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            try
            {
                SqlCommand cmd = new SqlCommand
                ("select * from empleado where CODIGOEMPLEADO ='" + TxbCodigoEmpleado.Text + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "empleado");
                DataRow dro;
                dro = ds.Tables["empleado"].Rows[0];
                TxbApellido.Text = dro["APELLIDOEMPLEADO"].ToString();
                TxbCelular.Text = dro["CELULAR"].ToString();
              
                TxbDireccion.Text = dro["DIRECCION"].ToString();
                TxbNombre.Text = dro["NOMBREEMPLEADO"].ToString();
                TxbNumDocumento.Text = dro["NUMERODOCUMENTO"].ToString();
                TxbTelefono.Text = dro["TELEFONO"].ToString();
            }
            catch
            {
                MessageBox.Show(" empleado no encontrado");
            }
            finally
            {
                con.Close();
            }

            }

        private void BtActualizar_Click(object sender, EventArgs e)
        {
            habilitar();
            BtActualizar.Enabled = false;
            TxbCodigoEmpleado.Enabled = false;
            BtModificar.Enabled = true;
            BtModificar.Visible = true;
        }



        private void limpiar()
        {
            TxbApellido.Clear();
            TxbCelular.Clear();
            TxbCodigoEmpleado.Clear();
            TxbDireccion.Clear();
            TxbNombre.Clear();
            TxbNumDocumento.Clear();
            TxbTelefono.Clear();
            BtActualizar.Visible = false;
            BtNuevo.Visible = false;
            BtModificar.Visible = false;


        }
        private void desabilitar()
        {

            TxbTelefono.Enabled = false;
            TxbNumDocumento.Enabled = false;
            TxbNombre.Enabled = false;
            TxbDireccion.Enabled = false;
            TxbCelular.Enabled = false;
            TxbApellido.Enabled = false;
            BtActualizar.Enabled = false;
            BtNuevo.Enabled = false;
            BtModificar.Enabled = false;

        }
        private void habilitar()
        {
            TxbApellido.Enabled = true;
            TxbCelular.Enabled = true;
            TxbDireccion.Enabled = true;
            TxbNombre.Enabled = true;
            TxbNumDocumento.Enabled = true;
            TxbTelefono.Enabled = true;
            BtActualizar.Enabled = true;
            BtNuevo.Enabled = true;
            BtModificar.Enabled = true;

        }

        private void BtConsultar_Click_1(object sender, EventArgs e)
        {
            if (TxbCodigoEmpleado.Text == "")
            {

                MessageBox.Show("el campo no puede estar vacio");
                TxbCodigoEmpleado.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                try
                {
                    SqlCommand cmd = new SqlCommand
                    ("select * from EMPLEADO where CODIGOEMPLEADO ='" + TxbCodigoEmpleado.Text + "' ", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "EMPLEADO");
                    DataRow dro;
                    dro = ds.Tables["EMPLEADO"].Rows[0];
                    TxbNombre.Text = dro["NOMBREEMPLEADO"].ToString();
                    TxbApellido.Text = dro["APELLIDOEMPLEADO"].ToString();
                    TxbDireccion.Text = dro["DIRECCION"].ToString();
                    TxbNumDocumento.Text = dro["NUMERODOCUMENTO"].ToString();
                    TxbTelefono.Text = dro["TELEFONO"].ToString();
                    TxbCelular.Text = dro["CELULAR"].ToString();
                    BtActualizar.Enabled = true;
                    BtActualizar.Visible = true;

                    }
                catch
                {
                    //MessageBox.Show(" usuario no encontrado")
                    const string mensaje = "empleado no encontrado desea crearlo si/no";
                    const string titulo = "consulta";
                    var resultado = MessageBox.Show(mensaje, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (resultado == DialogResult.No)
                    {
                        TxbCodigoEmpleado.Clear();
                        TxbCodigoEmpleado.Focus();
                    }
                    else
                    {


                        TxbCodigoEmpleado.Enabled = false;
                        TxbApellido.Clear();
                        TxbCelular.Clear();
                        TxbDireccion.Clear();
                        TxbNombre.Clear();
                        TxbNumDocumento.Clear();
                        TxbTelefono.Clear();
                        TxbApellido.Enabled = true;
                        TxbCelular.Enabled = true;
                        TxbNombre.Enabled = true;
                        TxbNumDocumento.Enabled = true;
                        TxbDireccion.Enabled = true;
                        TxbTelefono.Enabled = true;

                        BtNuevo.Visible = true;
                        BtNuevo.Enabled = true;
                        BtConsultar.Enabled = false;


                    }

                }

                finally
                {
                    con.Close();
                }

            }
       
        }

        private void BtNuevo_Click_1(object sender, EventArgs e)
        {
            if (TxbCodigoEmpleado.Text == "")
            {
                MessageBox.Show("el campo apellido esta  vacion");
                TxbCodigoEmpleado.Focus();


            }
            else if (TxbNombre.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbNombre.Focus();
            }
            else if (TxbApellido.Text == "")
            {
                MessageBox.Show("el campo codigo entrega esta vacio");
                TxbApellido.Focus();
            }
            else if (TxbDireccion.Text == "")
            {
                MessageBox.Show("el campo nombre esta vacio");
                TxbDireccion.Focus();
            }
            else if (TxbNumDocumento.Text == "")
            {
                MessageBox.Show("el campo numero documento esta vacio");
                TxbNumDocumento.Focus();
            }
            else if (TxbTelefono.Text == "")
            {
                MessageBox.Show("el campo numero documento esta vacio");
                TxbTelefono.Focus();
            }
            else if (TxbCelular.Text == "")
            {
                MessageBox.Show("el campo numero documento esta vacio");
                TxbCelular.Focus();
            }

            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");


                SqlCommand cmd = new SqlCommand("insert into EMPLEADO(CODIGOEMPLEADO, NOMBREEMPLEADO,APELLIDOEMPLEADO,DIRECCION,NUMERODOCUMENTO,TELEFONO,CELULAR)Values('" + Convert.ToInt16(TxbCodigoEmpleado.Text) + "','" + TxbNombre.Text + "','" + TxbApellido.Text + "', '" + TxbDireccion.Text + "', '" + Convert.ToInt16(TxbNumDocumento.Text) + "', '" + Convert.ToInt16(TxbTelefono.Text) + "','"+Convert.ToInt16(TxbCelular.Text)+"' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se   agrego con exito ");
            }  
        }

        private void BtActualizar_Click_1(object sender, EventArgs e)
        {
            habilitar();
            BtActualizar.Enabled = false;
            TxbCodigoEmpleado.Enabled = false;
            BtModificar.Enabled = true;
            BtModificar.Visible = true;
        }

        private void BtModificar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");

            SqlCommand cmd = new SqlCommand("update EMPLEADO set NOMBREEMPLEADO ='" + TxbNombre.Text + "', APELLIDOEMPLEADO = '" + TxbApellido.Text + "', DIRECCION = '" + TxbDireccion.Text + "',  NUMERODOCUMENTO = '" + TxbNumDocumento.Text + "', TELEFONO = '" + TxbTelefono.Text + "', CELULAR = '" + TxbCelular.Text + "' where CODIGOEMPLEADO = '" + TxbCodigoEmpleado.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            MessageBox.Show("se modifico  correctamente");
            desabilitar();
            limpiar();
            TxbCodigoEmpleado.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtCancelar_Click(object sender, EventArgs e)
        {
            desabilitar();
            limpiar();
            TxbCodigoEmpleado.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtSalir_Click_1(object sender, EventArgs e)
        {
            Principal empleado = new Principal();
            empleado.Show();
            this.Hide();
        }

        private void empleado_Load(object sender, EventArgs e)
        {
            Conexion objeto = new Conexion();
            objeto.ActualizarGrid(this.dataGridView1, "select * from EMPLEADO");
        }

        private void TxbCodigoEmpleado_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.sololetras(e);
        }

        private void TxbApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.sololetras(e);
        }

        private void TxbNumDocumento_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbCelular_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

       


        

        

       

       

        
    }
}
