﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DR_Doctor_Lemus;
using System.Data.SqlClient;


namespace DR_Doctor_Lemus
{
    public partial class Entrega : Form
    {
        public Entrega()
        {
            InitializeComponent();
        }

        private void BtSalir_Click(object sender, EventArgs e)
        {
            Principal entrega = new Principal();
            entrega.Show();
            this.Hide();
        }

       


            





        

        

        

       

        private void TxbCodCliente_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }
        }

        private void TxbCelularCliente_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }
        }

       

        private void TxbCodigoEmpleado_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }
        }

        private void Entrega_Load(object sender, EventArgs e)
        {
            Conexion objeto = new Conexion();
            objeto.ActualizarGrid(this.dataGridView1, "select * from ENTREGA");
        }
        private void limpiar()
        {
            
            TxbCodEntrega.Clear();
            TxbCodigoEmpleado.Clear();
            BtActualizar.Visible = false;
            BtNuevo.Visible = false;
            BtModificar.Visible = false;


        }
        private void desabilitar()
        {

            TxbCodigoEmpleado.Enabled = false;
            BtActualizar.Enabled = false;
            BtNuevo.Enabled = false;
            BtModificar.Enabled = false;

        }
        private void habilitar()
        {
            TxbCodigoEmpleado.Enabled = true;
            BtActualizar.Enabled = true;
            BtNuevo.Enabled = true;
            BtModificar.Enabled = true;

        }

        private void BtConsultar_Click_1(object sender, EventArgs e)
        {
            if (TxbCodEntrega.Text == "")
            {

                MessageBox.Show("el campo no puede estar vacio");
               TxbCodEntrega.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                try
                {
                    SqlCommand cmd = new SqlCommand
                    ("select * from ENTREGA where CODIGO_ENTREGA ='" + TxbCodEntrega.Text + "' ", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "ENTREGA");
                    DataRow dro;
                    dro = ds.Tables["ENTREGA"].Rows[0];
                    TxbCodigoEmpleado.Text = dro["CODIGOEMPLEADO"].ToString();
                    BtActualizar.Enabled = true;
                    BtActualizar.Visible = true;

                }
                catch
                {
                    //MessageBox.Show(" usuario no encontrado")
                    const string mensaje = "entrega no encontrada desea crearla si/no";
                    const string titulo = "consulta";
                    var resultado = MessageBox.Show(mensaje, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (resultado == DialogResult.No)
                    {
                        TxbCodEntrega.Clear();
                        TxbCodEntrega.Focus();
                    }
                    else
                    {


                        TxbCodEntrega.Enabled = false;
                        TxbCodigoEmpleado.Clear();
                        TxbCodigoEmpleado.Enabled = true;

                        BtNuevo.Visible = true;
                        BtNuevo.Enabled = true;
                        BtConsultar.Enabled = false;


                    }

                }

                finally
                {
                    con.Close();
                }

            }
        }

        private void BtNuevo_Click_1(object sender, EventArgs e)
        {
            if (TxbCodEntrega.Text == "")
            {
                MessageBox.Show("el campo apellido esta  vacion");
                TxbCodEntrega.Focus();


            }
            else if (TxbCodigoEmpleado.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbCodigoEmpleado.Focus();
            }
            
            

            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");


                SqlCommand cmd = new SqlCommand("insert into ENTREGA(CODIGO_ENTREGA, CODIGOEMPLEADO)Values('" + TxbCodEntrega.Text + "','" + Convert.ToInt16(TxbCodigoEmpleado.Text) + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se   agrego con exito ");
            }
        }

        private void BtActualizar_Click_1(object sender, EventArgs e)
        {
            habilitar();
            BtActualizar.Enabled = false;
            TxbCodEntrega.Enabled = false;
            BtModificar.Enabled = true;
            BtModificar.Visible = true;
        }

        private void BtModificar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");

            SqlCommand cmd = new SqlCommand("update ENTREGA set CODIGOEMPLEADO ='" + TxbCodigoEmpleado.Text + "' where CODIGO_ENTREGA = '" + TxbCodEntrega.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            MessageBox.Show("se modifico  correctamente");
            desabilitar();
            limpiar();
            TxbCodEntrega.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtCancelar_Click(object sender, EventArgs e)
        {
            desabilitar();
            limpiar();
            TxbCodEntrega.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtSalir_Click_1(object sender, EventArgs e)
        {
            Principal Entrega = new Principal();
            Entrega.Show();
            this.Hide();
        }

        private void TxbCodigoEmpleado_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }
    }
}
