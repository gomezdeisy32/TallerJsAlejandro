﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DR_Doctor_Lemus;


namespace DR_Doctor_Lemus
{
    public partial class cliente : Form
    {
        public cliente()
        {
            InitializeComponent();
        }


        private void BtSalir_Click(object sender, EventArgs e)
        {
            Principal cliente = new Principal();
            cliente.Show();
            this.Hide();
        }

        private void BtNuevo_Click(object sender, EventArgs e)
        {
            if (TxbCodCliente.Text == "")
            {
                MessageBox.Show("el campo apellido esta  vacion");
                TxbCodCliente.Focus();


            }
            else if (TxbNombreCliente.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbNombreCliente.Focus();
            }
                else if (TxbApellidoCliente.Text== "")
               {
                   MessageBox.Show("el campo codigo entrega esta vacio");
                   TxbApellidoCliente.Focus();
               }
            else if (TxbNumeroDocCliente.Text == "")
            {
                MessageBox.Show("el campo nombre esta vacio");
                TxbNumeroDocCliente.Focus();
            }
            else if (TxbCodFacturaCliente.Text == "")
            {
                MessageBox.Show("el campo numero documento esta vacio");
                TxbCodFacturaCliente.Focus();
            }

            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");


                SqlCommand cmd = new SqlCommand("insert into CLIENTE(CODIGO_CLIENTE, NOMBRECLIENTE,APELLIDOCLIENTE,NUMERODOCUMENTO,CODIGO_FACTURA)Values('" + Convert.ToInt32(TxbCodCliente.Text) + "','" + TxbNombreCliente.Text + "','" + TxbApellidoCliente.Text + "', '" + Convert.ToInt32(TxbNumeroDocCliente.Text) + "', '" + Convert.ToInt32(TxbCodFacturaCliente.Text) + "'  ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se   agrego con exito ");
            }  
        }

        private void BtConsultar_Click(object sender, EventArgs e)
        {
            if (TxbCodCliente.Text == "")
            {

                MessageBox.Show("el campo no puede estar vacio");
                TxbCodCliente.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                try
                {
                    SqlCommand cmd = new SqlCommand
                    ("select * from CLIENTE where CODIGO_CLIENTE ='" + TxbCodCliente.Text + "' ", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "CLIENTE");
                    DataRow dro;
                    dro = ds.Tables["CLIENTE"].Rows[0];
                    TxbApellidoCliente.Text = dro["APELLIDOCLIENTE"].ToString();
                    TxbCodFacturaCliente.Text = dro["CODIGO_FACTURA"].ToString();
                    TxbNombreCliente.Text = dro["NOMBRECLIENTE"].ToString();
                    TxbNumeroDocCliente.Text = dro["NUMERODOCUMENTO"].ToString();
                  
                    BtActualizar.Enabled = true;
                    BtActualizar.Visible = true;



                }
                catch
                {
                    //MessageBox.Show(" usuario no encontrado")
                    const string mensaje = "cliente no encontrado desea crearlo si/no";
                    const string titulo = "consulta";
                    var resultado = MessageBox.Show(mensaje, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (resultado == DialogResult.No)
                    {
                        TxbCodCliente.Clear();
                        TxbCodCliente.Focus();
                    }
                    else
                    {


                        TxbCodCliente.Enabled = false;
                        TxbApellidoCliente.Clear();
                        TxbCodFacturaCliente.Clear();
                        TxbNombreCliente.Clear();
                        TxbNumeroDocCliente.Clear();
                        
                        TxbApellidoCliente.Enabled = true;
                        TxbCodFacturaCliente.Enabled = true;
                        TxbNombreCliente.Enabled = true;
                        TxbNumeroDocCliente.Enabled = true;
                       
                        BtNuevo.Visible = true;
                        BtNuevo.Enabled = true;
                        BtConsultar.Enabled = false;


                    }

                }

                finally
                {
                    con.Close();
                }

            }
       
        }

        private void BtActualizar_Click(object sender, EventArgs e)
        {
            habilitar();
            BtActualizar.Enabled = false;
            TxbCodCliente.Enabled = false;
            Bt_Modificar.Enabled = true;
            Bt_Modificar.Visible = true;
        }

        private void Bt_Modificar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");

            SqlCommand cmd = new SqlCommand("update CLIENTE set NOMBRECLIENTE ='" + TxbNombreCliente.Text + "', APELLIDOCLIENTE = '" + TxbApellidoCliente.Text + "', CODIGO_FACTURA = '" + TxbCodFacturaCliente.Text + "',  NUMERODOCUMENTO = '" + TxbNumeroDocCliente.Text + "' where CODIGO_CLIENTE = '" + TxbCodCliente.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            MessageBox.Show("se modifico  correctamente");
            desabilitar();
            limpiar();
            TxbCodCliente.Enabled = true;
            BtConsultar.Enabled = true;
         
        }
        private void limpiar()
        {
            TxbCodCliente.Clear();
            TxbApellidoCliente.Clear();
            TxbCodFacturaCliente.Clear();
            TxbNombreCliente.Clear();
            TxbNumeroDocCliente.Clear();
            
            BtActualizar.Visible = false;
            BtNuevo.Visible = false;
            Bt_Modificar.Visible = false;
            

        }
        private void desabilitar()
        {

            TxbNumeroDocCliente.Enabled = false;
            TxbNombreCliente.Enabled = false;
            TxbApellidoCliente.Enabled = false;
            TxbCodFacturaCliente.Enabled = false;
            BtActualizar.Enabled = false;
            BtNuevo.Enabled = false;
            Bt_Modificar.Enabled = false;

        }
        private void habilitar()
        {
            TxbApellidoCliente.Enabled = true;
            TxbCodFacturaCliente.Enabled = true;
            TxbNombreCliente.Enabled = true;
            TxbNumeroDocCliente.Enabled = true;
           
            BtActualizar.Enabled = true;
            BtNuevo.Enabled = true;
            
            Bt_Modificar.Enabled = true;

        }

        private void BtCancelar_Click(object sender, EventArgs e)
        {
            desabilitar();
            limpiar();
            TxbCodCliente.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void cliente_Load(object sender, EventArgs e)
        {
            Conexion objeto = new Conexion();
            objeto.ActualizarGrid(this.dataGridView1, "Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
        }

        private void TxbCodCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbNombreCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.sololetras(e);
        }

        private void TxbApellidoCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.sololetras(e);
        }

        private void TxbNumeroDocCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbCodFacturaCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void TxbCodFacturaCliente_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxbNumeroDocCliente_TextChanged(object sender, EventArgs e)
        {

        }

       
        
       

        

        
    }
}
    

