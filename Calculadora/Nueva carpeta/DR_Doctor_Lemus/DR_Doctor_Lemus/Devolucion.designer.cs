﻿namespace DR_Doctor_Lemus
{
    partial class Devolucion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Devolucion));
            this.TxbCodigoFacDev = new System.Windows.Forms.TextBox();
            this.TxbCodEmpleadoDev = new System.Windows.Forms.TextBox();
            this.TxbMotivDevol = new System.Windows.Forms.TextBox();
            this.TxbCodDevoluion = new System.Windows.Forms.TextBox();
            this.LbCodigo_Factura = new System.Windows.Forms.Label();
            this.LbCodigo_Empleado = new System.Windows.Forms.Label();
            this.LbMotivo_Devolucion = new System.Windows.Forms.Label();
            this.LbCodigo_Devolucion = new System.Windows.Forms.Label();
            this.BtNuevo = new System.Windows.Forms.Button();
            this.BtConsultar = new System.Windows.Forms.Button();
            this.BtActualizar = new System.Windows.Forms.Button();
            this.BtSalir = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.LbCantidad = new System.Windows.Forms.Label();
            this.TxbCantidad = new System.Windows.Forms.TextBox();
            this.LbBuscar = new System.Windows.Forms.Label();
            this.LbNuevo = new System.Windows.Forms.Label();
            this.LbActualizar = new System.Windows.Forms.Label();
            this.LbSalir = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.BtModificar = new System.Windows.Forms.Button();
            this.BtCancelar = new System.Windows.Forms.Button();
            this.LbCancelar = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // TxbCodigoFacDev
            // 
            this.TxbCodigoFacDev.Enabled = false;
            this.TxbCodigoFacDev.Location = new System.Drawing.Point(298, 229);
            this.TxbCodigoFacDev.Name = "TxbCodigoFacDev";
            this.TxbCodigoFacDev.Size = new System.Drawing.Size(140, 20);
            this.TxbCodigoFacDev.TabIndex = 34;
            this.TxbCodigoFacDev.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCodigoFacDev_KeyPress);
            // 
            // TxbCodEmpleadoDev
            // 
            this.TxbCodEmpleadoDev.Enabled = false;
            this.TxbCodEmpleadoDev.Location = new System.Drawing.Point(298, 168);
            this.TxbCodEmpleadoDev.Name = "TxbCodEmpleadoDev";
            this.TxbCodEmpleadoDev.Size = new System.Drawing.Size(140, 20);
            this.TxbCodEmpleadoDev.TabIndex = 33;
            this.TxbCodEmpleadoDev.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCodEmpleadoDev_KeyPress);
            // 
            // TxbMotivDevol
            // 
            this.TxbMotivDevol.Enabled = false;
            this.TxbMotivDevol.Location = new System.Drawing.Point(298, 102);
            this.TxbMotivDevol.Name = "TxbMotivDevol";
            this.TxbMotivDevol.Size = new System.Drawing.Size(140, 20);
            this.TxbMotivDevol.TabIndex = 31;
            this.TxbMotivDevol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbMotivDevol_KeyPress);
            // 
            // TxbCodDevoluion
            // 
            this.TxbCodDevoluion.Location = new System.Drawing.Point(298, 49);
            this.TxbCodDevoluion.Name = "TxbCodDevoluion";
            this.TxbCodDevoluion.Size = new System.Drawing.Size(140, 20);
            this.TxbCodDevoluion.TabIndex = 30;
            // 
            // LbCodigo_Factura
            // 
            this.LbCodigo_Factura.AutoSize = true;
            this.LbCodigo_Factura.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigo_Factura.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigo_Factura.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodigo_Factura.Location = new System.Drawing.Point(143, 233);
            this.LbCodigo_Factura.Name = "LbCodigo_Factura";
            this.LbCodigo_Factura.Size = new System.Drawing.Size(132, 20);
            this.LbCodigo_Factura.TabIndex = 28;
            this.LbCodigo_Factura.Text = "Codigo Factura";
            // 
            // LbCodigo_Empleado
            // 
            this.LbCodigo_Empleado.AutoSize = true;
            this.LbCodigo_Empleado.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigo_Empleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigo_Empleado.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodigo_Empleado.Location = new System.Drawing.Point(135, 172);
            this.LbCodigo_Empleado.Name = "LbCodigo_Empleado";
            this.LbCodigo_Empleado.Size = new System.Drawing.Size(150, 20);
            this.LbCodigo_Empleado.TabIndex = 27;
            this.LbCodigo_Empleado.Text = "Codigo Empleado";
            // 
            // LbMotivo_Devolucion
            // 
            this.LbMotivo_Devolucion.AutoSize = true;
            this.LbMotivo_Devolucion.BackColor = System.Drawing.Color.Transparent;
            this.LbMotivo_Devolucion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbMotivo_Devolucion.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbMotivo_Devolucion.Location = new System.Drawing.Point(135, 112);
            this.LbMotivo_Devolucion.Name = "LbMotivo_Devolucion";
            this.LbMotivo_Devolucion.Size = new System.Drawing.Size(154, 20);
            this.LbMotivo_Devolucion.TabIndex = 25;
            this.LbMotivo_Devolucion.Text = "Motivo Devolucion";
            // 
            // LbCodigo_Devolucion
            // 
            this.LbCodigo_Devolucion.AutoSize = true;
            this.LbCodigo_Devolucion.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigo_Devolucion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigo_Devolucion.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodigo_Devolucion.Location = new System.Drawing.Point(124, 49);
            this.LbCodigo_Devolucion.Name = "LbCodigo_Devolucion";
            this.LbCodigo_Devolucion.Size = new System.Drawing.Size(158, 20);
            this.LbCodigo_Devolucion.TabIndex = 24;
            this.LbCodigo_Devolucion.Text = "Codigo Devolucion";
            this.toolTip1.SetToolTip(this.LbCodigo_Devolucion, "recuerde que su codigo debe inicial con una letra");
            // 
            // BtNuevo
            // 
            this.BtNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtNuevo.BackgroundImage")));
            this.BtNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtNuevo.Enabled = false;
            this.BtNuevo.Location = new System.Drawing.Point(6, 200);
            this.BtNuevo.Name = "BtNuevo";
            this.BtNuevo.Size = new System.Drawing.Size(54, 49);
            this.BtNuevo.TabIndex = 38;
            this.BtNuevo.UseVisualStyleBackColor = true;
            this.BtNuevo.Visible = false;
            this.BtNuevo.Click += new System.EventHandler(this.BtNuevo_Click);
            // 
            // BtConsultar
            // 
            this.BtConsultar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtConsultar.BackgroundImage")));
            this.BtConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtConsultar.Location = new System.Drawing.Point(14, 72);
            this.BtConsultar.Name = "BtConsultar";
            this.BtConsultar.Size = new System.Drawing.Size(54, 50);
            this.BtConsultar.TabIndex = 39;
            this.BtConsultar.UseVisualStyleBackColor = true;
            this.BtConsultar.Click += new System.EventHandler(this.BtConsultar_Click);
            // 
            // BtActualizar
            // 
            this.BtActualizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtActualizar.BackgroundImage")));
            this.BtActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtActualizar.Enabled = false;
            this.BtActualizar.Location = new System.Drawing.Point(493, 74);
            this.BtActualizar.Name = "BtActualizar";
            this.BtActualizar.Size = new System.Drawing.Size(54, 46);
            this.BtActualizar.TabIndex = 40;
            this.BtActualizar.UseVisualStyleBackColor = true;
            this.BtActualizar.Visible = false;
            this.BtActualizar.Click += new System.EventHandler(this.BtActualizar_Click);
            // 
            // BtSalir
            // 
            this.BtSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtSalir.BackgroundImage")));
            this.BtSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtSalir.Location = new System.Drawing.Point(497, 307);
            this.BtSalir.Name = "BtSalir";
            this.BtSalir.Size = new System.Drawing.Size(53, 48);
            this.BtSalir.TabIndex = 41;
            this.BtSalir.UseVisualStyleBackColor = true;
            this.BtSalir.Click += new System.EventHandler(this.BtSalir_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(120, -1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(342, 590);
            this.pictureBox2.TabIndex = 43;
            this.pictureBox2.TabStop = false;
            // 
            // LbCantidad
            // 
            this.LbCantidad.AutoSize = true;
            this.LbCantidad.BackColor = System.Drawing.Color.Transparent;
            this.LbCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCantidad.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCantidad.Location = new System.Drawing.Point(154, 295);
            this.LbCantidad.Name = "LbCantidad";
            this.LbCantidad.Size = new System.Drawing.Size(81, 20);
            this.LbCantidad.TabIndex = 44;
            this.LbCantidad.Text = "Cantidad";
            // 
            // TxbCantidad
            // 
            this.TxbCantidad.Enabled = false;
            this.TxbCantidad.Location = new System.Drawing.Point(298, 290);
            this.TxbCantidad.Name = "TxbCantidad";
            this.TxbCantidad.Size = new System.Drawing.Size(140, 20);
            this.TxbCantidad.TabIndex = 45;
            this.TxbCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCantidad_KeyPress);
            // 
            // LbBuscar
            // 
            this.LbBuscar.AutoSize = true;
            this.LbBuscar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbBuscar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LbBuscar.Location = new System.Drawing.Point(20, 125);
            this.LbBuscar.Name = "LbBuscar";
            this.LbBuscar.Size = new System.Drawing.Size(40, 13);
            this.LbBuscar.TabIndex = 46;
            this.LbBuscar.Text = "Buscar";
            // 
            // LbNuevo
            // 
            this.LbNuevo.AutoSize = true;
            this.LbNuevo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbNuevo.Enabled = false;
            this.LbNuevo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbNuevo.Location = new System.Drawing.Point(17, 255);
            this.LbNuevo.Name = "LbNuevo";
            this.LbNuevo.Size = new System.Drawing.Size(39, 13);
            this.LbNuevo.TabIndex = 47;
            this.LbNuevo.Text = "Nuevo";
            this.LbNuevo.Visible = false;
            // 
            // LbActualizar
            // 
            this.LbActualizar.AutoSize = true;
            this.LbActualizar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbActualizar.Enabled = false;
            this.LbActualizar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbActualizar.Location = new System.Drawing.Point(494, 121);
            this.LbActualizar.Name = "LbActualizar";
            this.LbActualizar.Size = new System.Drawing.Size(53, 13);
            this.LbActualizar.TabIndex = 48;
            this.LbActualizar.Text = "Actualizar";
            this.LbActualizar.Visible = false;
            // 
            // LbSalir
            // 
            this.LbSalir.AutoSize = true;
            this.LbSalir.BackColor = System.Drawing.Color.DimGray;
            this.LbSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.LbSalir.Location = new System.Drawing.Point(509, 371);
            this.LbSalir.Name = "LbSalir";
            this.LbSalir.Size = new System.Drawing.Size(27, 13);
            this.LbSalir.TabIndex = 49;
            this.LbSalir.Text = "Salir";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(138, 388);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(312, 148);
            this.dataGridView1.TabIndex = 50;
            // 
            // BtModificar
            // 
            this.BtModificar.BackColor = System.Drawing.Color.Transparent;
            this.BtModificar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtModificar.BackgroundImage")));
            this.BtModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtModificar.Enabled = false;
            this.BtModificar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtModificar.Location = new System.Drawing.Point(493, 172);
            this.BtModificar.Name = "BtModificar";
            this.BtModificar.Size = new System.Drawing.Size(53, 56);
            this.BtModificar.TabIndex = 51;
            this.BtModificar.UseVisualStyleBackColor = false;
            this.BtModificar.Visible = false;
            this.BtModificar.Click += new System.EventHandler(this.BtModificar_Click);
            // 
            // BtCancelar
            // 
            this.BtCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtCancelar.BackgroundImage")));
            this.BtCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtCancelar.Location = new System.Drawing.Point(10, 305);
            this.BtCancelar.Name = "BtCancelar";
            this.BtCancelar.Size = new System.Drawing.Size(55, 53);
            this.BtCancelar.TabIndex = 52;
            this.BtCancelar.UseVisualStyleBackColor = false;
            this.BtCancelar.Click += new System.EventHandler(this.BtCancelar_Click);
            // 
            // LbCancelar
            // 
            this.LbCancelar.AutoSize = true;
            this.LbCancelar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbCancelar.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCancelar.Location = new System.Drawing.Point(11, 361);
            this.LbCancelar.Name = "LbCancelar";
            this.LbCancelar.Size = new System.Drawing.Size(49, 13);
            this.LbCancelar.TabIndex = 53;
            this.LbCancelar.Text = "Cancelar";
            // 
            // Devolucion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(585, 578);
            this.Controls.Add(this.LbCancelar);
            this.Controls.Add(this.BtCancelar);
            this.Controls.Add(this.BtModificar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.LbSalir);
            this.Controls.Add(this.LbActualizar);
            this.Controls.Add(this.LbNuevo);
            this.Controls.Add(this.LbBuscar);
            this.Controls.Add(this.TxbCantidad);
            this.Controls.Add(this.LbCantidad);
            this.Controls.Add(this.BtSalir);
            this.Controls.Add(this.BtActualizar);
            this.Controls.Add(this.BtConsultar);
            this.Controls.Add(this.BtNuevo);
            this.Controls.Add(this.TxbCodigoFacDev);
            this.Controls.Add(this.TxbCodEmpleadoDev);
            this.Controls.Add(this.TxbMotivDevol);
            this.Controls.Add(this.TxbCodDevoluion);
            this.Controls.Add(this.LbCodigo_Factura);
            this.Controls.Add(this.LbCodigo_Empleado);
            this.Controls.Add(this.LbMotivo_Devolucion);
            this.Controls.Add(this.LbCodigo_Devolucion);
            this.Controls.Add(this.pictureBox2);
            this.Name = "Devolucion";
            this.Text = "Devolucion";
            this.Load += new System.EventHandler(this.Devolucion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxbCodigoFacDev;
        private System.Windows.Forms.TextBox TxbCodEmpleadoDev;
        private System.Windows.Forms.TextBox TxbMotivDevol;
        private System.Windows.Forms.TextBox TxbCodDevoluion;
        private System.Windows.Forms.Label LbCodigo_Factura;
        private System.Windows.Forms.Label LbCodigo_Empleado;
        private System.Windows.Forms.Label LbMotivo_Devolucion;
        private System.Windows.Forms.Label LbCodigo_Devolucion;
        private System.Windows.Forms.Button BtNuevo;
        private System.Windows.Forms.Button BtConsultar;
        private System.Windows.Forms.Button BtActualizar;
        private System.Windows.Forms.Button BtSalir;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label LbCantidad;
        private System.Windows.Forms.TextBox TxbCantidad;
        private System.Windows.Forms.Label LbBuscar;
        private System.Windows.Forms.Label LbNuevo;
        private System.Windows.Forms.Label LbActualizar;
        private System.Windows.Forms.Label LbSalir;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button BtModificar;
        private System.Windows.Forms.Button BtCancelar;
        private System.Windows.Forms.Label LbCancelar;
        private System.Windows.Forms.ToolTip toolTip1;

    }
}