﻿namespace DR_Doctor_Lemus
{
    partial class Ingresar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtIngresar = new System.Windows.Forms.Button();
            this.LbUsuario = new System.Windows.Forms.Label();
            this.LbContraseña = new System.Windows.Forms.Label();
            this.TxbContraseña = new System.Windows.Forms.TextBox();
            this.BtSalir = new System.Windows.Forms.Button();
            this.BtLimpiar = new System.Windows.Forms.Button();
            this.TxbUsuario = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // BtIngresar
            // 
            this.BtIngresar.BackColor = System.Drawing.Color.Transparent;
            this.BtIngresar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtIngresar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtIngresar.Location = new System.Drawing.Point(113, 336);
            this.BtIngresar.Name = "BtIngresar";
            this.BtIngresar.Size = new System.Drawing.Size(126, 52);
            this.BtIngresar.TabIndex = 3;
            this.BtIngresar.Text = "INGRESAR";
            this.BtIngresar.UseVisualStyleBackColor = false;
            this.BtIngresar.Click += new System.EventHandler(this.BtIngresar_Click);
            // 
            // LbUsuario
            // 
            this.LbUsuario.AutoSize = true;
            this.LbUsuario.BackColor = System.Drawing.Color.Transparent;
            this.LbUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbUsuario.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbUsuario.Location = new System.Drawing.Point(42, 69);
            this.LbUsuario.Name = "LbUsuario";
            this.LbUsuario.Size = new System.Drawing.Size(123, 33);
            this.LbUsuario.TabIndex = 1;
            this.LbUsuario.Text = "Usuario";
            this.LbUsuario.Click += new System.EventHandler(this.LbUsuario_Click);
            // 
            // LbContraseña
            // 
            this.LbContraseña.AutoSize = true;
            this.LbContraseña.BackColor = System.Drawing.Color.Transparent;
            this.LbContraseña.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbContraseña.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbContraseña.Location = new System.Drawing.Point(17, 207);
            this.LbContraseña.Name = "LbContraseña";
            this.LbContraseña.Size = new System.Drawing.Size(175, 33);
            this.LbContraseña.TabIndex = 2;
            this.LbContraseña.Text = "Contraseña";
            // 
            // TxbContraseña
            // 
            this.TxbContraseña.Location = new System.Drawing.Point(201, 216);
            this.TxbContraseña.Name = "TxbContraseña";
            this.TxbContraseña.PasswordChar = '*';
            this.TxbContraseña.Size = new System.Drawing.Size(121, 20);
            this.TxbContraseña.TabIndex = 2;
            this.TxbContraseña.UseSystemPasswordChar = true;
            this.TxbContraseña.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxbContraseña_KeyDown);
            this.TxbContraseña.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbContraseña_KeyPress);
            // 
            // BtSalir
            // 
            this.BtSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtSalir.Location = new System.Drawing.Point(12, 336);
            this.BtSalir.Name = "BtSalir";
            this.BtSalir.Size = new System.Drawing.Size(86, 52);
            this.BtSalir.TabIndex = 4;
            this.BtSalir.Text = "SALIR";
            this.BtSalir.UseVisualStyleBackColor = true;
            this.BtSalir.Click += new System.EventHandler(this.BtSalir_Click);
            // 
            // BtLimpiar
            // 
            this.BtLimpiar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtLimpiar.Location = new System.Drawing.Point(257, 336);
            this.BtLimpiar.Name = "BtLimpiar";
            this.BtLimpiar.Size = new System.Drawing.Size(86, 52);
            this.BtLimpiar.TabIndex = 5;
            this.BtLimpiar.Text = "LIMPIAR";
            this.BtLimpiar.UseVisualStyleBackColor = true;
            // 
            // TxbUsuario
            // 
            this.TxbUsuario.Location = new System.Drawing.Point(200, 78);
            this.TxbUsuario.Name = "TxbUsuario";
            this.TxbUsuario.Size = new System.Drawing.Size(121, 20);
            this.TxbUsuario.TabIndex = 1;
            this.TxbUsuario.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TxbUsuario_KeyDown);
            this.TxbUsuario.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbUsuario_KeyPress);
            // 
            // Ingresar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(369, 457);
            this.Controls.Add(this.TxbUsuario);
            this.Controls.Add(this.BtLimpiar);
            this.Controls.Add(this.BtSalir);
            this.Controls.Add(this.TxbContraseña);
            this.Controls.Add(this.LbContraseña);
            this.Controls.Add(this.LbUsuario);
            this.Controls.Add(this.BtIngresar);
            this.Name = "Ingresar";
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Ingresar_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtIngresar;
        private System.Windows.Forms.Label LbUsuario;
        private System.Windows.Forms.Label LbContraseña;
        private System.Windows.Forms.TextBox TxbContraseña;
        private System.Windows.Forms.Button BtSalir;
        private System.Windows.Forms.Button BtLimpiar;
        private System.Windows.Forms.TextBox TxbUsuario;
    }
}