﻿namespace DR_Doctor_Lemus
{
    partial class producto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(producto));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.nUEVOToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LbCodigoProducto = new System.Windows.Forms.Label();
            this.LbCantidadPastillero = new System.Windows.Forms.Label();
            this.TxbCodigoProducto = new System.Windows.Forms.TextBox();
            this.TxbCantidadPastillero = new System.Windows.Forms.TextBox();
            this.LbFechaVencimiento = new System.Windows.Forms.Label();
            this.TxbFechaVencimiento = new System.Windows.Forms.TextBox();
            this.LbNombreProducto = new System.Windows.Forms.Label();
            this.TxbNombreProducto = new System.Windows.Forms.TextBox();
            this.LbCancelar = new System.Windows.Forms.Label();
            this.BtCancelar = new System.Windows.Forms.Button();
            this.BtModificar = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.LbSalir = new System.Windows.Forms.Label();
            this.LbActualizar = new System.Windows.Forms.Label();
            this.LbNuevo = new System.Windows.Forms.Label();
            this.LbBuscar = new System.Windows.Forms.Label();
            this.BtSalir = new System.Windows.Forms.Button();
            this.BtActualizar = new System.Windows.Forms.Button();
            this.BtConsultar = new System.Windows.Forms.Button();
            this.BtNuevo = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.BtEliminar = new System.Windows.Forms.Button();
            this.LbEliminar = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nUEVOToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(114, 26);
            // 
            // nUEVOToolStripMenuItem
            // 
            this.nUEVOToolStripMenuItem.Name = "nUEVOToolStripMenuItem";
            this.nUEVOToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.nUEVOToolStripMenuItem.Text = "NUEVO";
            // 
            // LbCodigoProducto
            // 
            this.LbCodigoProducto.AutoSize = true;
            this.LbCodigoProducto.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigoProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigoProducto.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodigoProducto.Location = new System.Drawing.Point(162, 85);
            this.LbCodigoProducto.Name = "LbCodigoProducto";
            this.LbCodigoProducto.Size = new System.Drawing.Size(127, 20);
            this.LbCodigoProducto.TabIndex = 12;
            this.LbCodigoProducto.Text = "Codigo Producto";
            // 
            // LbCantidadPastillero
            // 
            this.LbCantidadPastillero.AutoSize = true;
            this.LbCantidadPastillero.BackColor = System.Drawing.Color.Transparent;
            this.LbCantidadPastillero.Enabled = false;
            this.LbCantidadPastillero.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCantidadPastillero.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.LbCantidadPastillero.Location = new System.Drawing.Point(144, 325);
            this.LbCantidadPastillero.Name = "LbCantidadPastillero";
            this.LbCantidadPastillero.Size = new System.Drawing.Size(137, 20);
            this.LbCantidadPastillero.TabIndex = 16;
            this.LbCantidadPastillero.Text = "CantidadPastillero";
            // 
            // TxbCodigoProducto
            // 
            this.TxbCodigoProducto.Location = new System.Drawing.Point(325, 84);
            this.TxbCodigoProducto.Name = "TxbCodigoProducto";
            this.TxbCodigoProducto.Size = new System.Drawing.Size(122, 20);
            this.TxbCodigoProducto.TabIndex = 18;
            this.TxbCodigoProducto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCodigoProducto_KeyPress);
            // 
            // TxbCantidadPastillero
            // 
            this.TxbCantidadPastillero.Enabled = false;
            this.TxbCantidadPastillero.Location = new System.Drawing.Point(324, 321);
            this.TxbCantidadPastillero.Name = "TxbCantidadPastillero";
            this.TxbCantidadPastillero.Size = new System.Drawing.Size(123, 20);
            this.TxbCantidadPastillero.TabIndex = 22;
            this.TxbCantidadPastillero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCantidadPastillero_KeyPress);
            // 
            // LbFechaVencimiento
            // 
            this.LbFechaVencimiento.AutoSize = true;
            this.LbFechaVencimiento.BackColor = System.Drawing.Color.Transparent;
            this.LbFechaVencimiento.Enabled = false;
            this.LbFechaVencimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbFechaVencimiento.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LbFechaVencimiento.Location = new System.Drawing.Point(144, 242);
            this.LbFechaVencimiento.Name = "LbFechaVencimiento";
            this.LbFechaVencimiento.Size = new System.Drawing.Size(146, 20);
            this.LbFechaVencimiento.TabIndex = 14;
            this.LbFechaVencimiento.Text = "Fecha Vencimiento";
            // 
            // TxbFechaVencimiento
            // 
            this.TxbFechaVencimiento.Enabled = false;
            this.TxbFechaVencimiento.Location = new System.Drawing.Point(325, 241);
            this.TxbFechaVencimiento.Name = "TxbFechaVencimiento";
            this.TxbFechaVencimiento.Size = new System.Drawing.Size(122, 20);
            this.TxbFechaVencimiento.TabIndex = 20;
            // 
            // LbNombreProducto
            // 
            this.LbNombreProducto.AutoSize = true;
            this.LbNombreProducto.BackColor = System.Drawing.Color.Transparent;
            this.LbNombreProducto.Enabled = false;
            this.LbNombreProducto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbNombreProducto.ForeColor = System.Drawing.SystemColors.Control;
            this.LbNombreProducto.Location = new System.Drawing.Point(160, 160);
            this.LbNombreProducto.Name = "LbNombreProducto";
            this.LbNombreProducto.Size = new System.Drawing.Size(129, 20);
            this.LbNombreProducto.TabIndex = 45;
            this.LbNombreProducto.Text = "NombreProducto";
            // 
            // TxbNombreProducto
            // 
            this.TxbNombreProducto.Enabled = false;
            this.TxbNombreProducto.Location = new System.Drawing.Point(325, 160);
            this.TxbNombreProducto.Name = "TxbNombreProducto";
            this.TxbNombreProducto.Size = new System.Drawing.Size(122, 20);
            this.TxbNombreProducto.TabIndex = 46;
            this.TxbNombreProducto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbNombreProducto_KeyPress);
            // 
            // LbCancelar
            // 
            this.LbCancelar.AutoSize = true;
            this.LbCancelar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbCancelar.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCancelar.Location = new System.Drawing.Point(26, 371);
            this.LbCancelar.Name = "LbCancelar";
            this.LbCancelar.Size = new System.Drawing.Size(49, 13);
            this.LbCancelar.TabIndex = 76;
            this.LbCancelar.Text = "Cancelar";
            // 
            // BtCancelar
            // 
            this.BtCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtCancelar.BackgroundImage")));
            this.BtCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtCancelar.Location = new System.Drawing.Point(21, 304);
            this.BtCancelar.Name = "BtCancelar";
            this.BtCancelar.Size = new System.Drawing.Size(55, 53);
            this.BtCancelar.TabIndex = 75;
            this.BtCancelar.UseVisualStyleBackColor = false;
            this.BtCancelar.Click += new System.EventHandler(this.BtCancelar_Click);
            // 
            // BtModificar
            // 
            this.BtModificar.BackColor = System.Drawing.Color.Transparent;
            this.BtModificar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtModificar.BackgroundImage")));
            this.BtModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtModificar.Enabled = false;
            this.BtModificar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtModificar.Location = new System.Drawing.Point(502, 184);
            this.BtModificar.Name = "BtModificar";
            this.BtModificar.Size = new System.Drawing.Size(53, 56);
            this.BtModificar.TabIndex = 74;
            this.BtModificar.UseVisualStyleBackColor = false;
            this.BtModificar.Visible = false;
            this.BtModificar.Click += new System.EventHandler(this.BtModificar_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(139, 426);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(308, 137);
            this.dataGridView1.TabIndex = 73;
            // 
            // LbSalir
            // 
            this.LbSalir.AutoSize = true;
            this.LbSalir.BackColor = System.Drawing.Color.DimGray;
            this.LbSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.LbSalir.Location = new System.Drawing.Point(515, 371);
            this.LbSalir.Name = "LbSalir";
            this.LbSalir.Size = new System.Drawing.Size(27, 13);
            this.LbSalir.TabIndex = 72;
            this.LbSalir.Text = "Salir";
            // 
            // LbActualizar
            // 
            this.LbActualizar.AutoSize = true;
            this.LbActualizar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbActualizar.Enabled = false;
            this.LbActualizar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbActualizar.Location = new System.Drawing.Point(503, 133);
            this.LbActualizar.Name = "LbActualizar";
            this.LbActualizar.Size = new System.Drawing.Size(53, 13);
            this.LbActualizar.TabIndex = 71;
            this.LbActualizar.Text = "Actualizar";
            this.LbActualizar.Visible = false;
            // 
            // LbNuevo
            // 
            this.LbNuevo.AutoSize = true;
            this.LbNuevo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbNuevo.Enabled = false;
            this.LbNuevo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbNuevo.Location = new System.Drawing.Point(25, 264);
            this.LbNuevo.Name = "LbNuevo";
            this.LbNuevo.Size = new System.Drawing.Size(39, 13);
            this.LbNuevo.TabIndex = 70;
            this.LbNuevo.Text = "Nuevo";
            this.LbNuevo.Visible = false;
            // 
            // LbBuscar
            // 
            this.LbBuscar.AutoSize = true;
            this.LbBuscar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbBuscar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LbBuscar.Location = new System.Drawing.Point(24, 150);
            this.LbBuscar.Name = "LbBuscar";
            this.LbBuscar.Size = new System.Drawing.Size(40, 13);
            this.LbBuscar.TabIndex = 69;
            this.LbBuscar.Text = "Buscar";
            // 
            // BtSalir
            // 
            this.BtSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtSalir.BackgroundImage")));
            this.BtSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtSalir.Location = new System.Drawing.Point(502, 306);
            this.BtSalir.Name = "BtSalir";
            this.BtSalir.Size = new System.Drawing.Size(53, 48);
            this.BtSalir.TabIndex = 65;
            this.BtSalir.UseVisualStyleBackColor = true;
            this.BtSalir.Click += new System.EventHandler(this.BtSalir_Click_1);
            // 
            // BtActualizar
            // 
            this.BtActualizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtActualizar.BackgroundImage")));
            this.BtActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtActualizar.Enabled = false;
            this.BtActualizar.Location = new System.Drawing.Point(502, 84);
            this.BtActualizar.Name = "BtActualizar";
            this.BtActualizar.Size = new System.Drawing.Size(54, 46);
            this.BtActualizar.TabIndex = 64;
            this.BtActualizar.UseVisualStyleBackColor = true;
            this.BtActualizar.Visible = false;
            this.BtActualizar.Click += new System.EventHandler(this.BtActualizar_Click_1);
            // 
            // BtConsultar
            // 
            this.BtConsultar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtConsultar.BackgroundImage")));
            this.BtConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtConsultar.Location = new System.Drawing.Point(21, 85);
            this.BtConsultar.Name = "BtConsultar";
            this.BtConsultar.Size = new System.Drawing.Size(54, 50);
            this.BtConsultar.TabIndex = 63;
            this.BtConsultar.UseVisualStyleBackColor = true;
            this.BtConsultar.Click += new System.EventHandler(this.BtConsultar_Click_1);
            // 
            // BtNuevo
            // 
            this.BtNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtNuevo.BackgroundImage")));
            this.BtNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtNuevo.Enabled = false;
            this.BtNuevo.Location = new System.Drawing.Point(15, 212);
            this.BtNuevo.Name = "BtNuevo";
            this.BtNuevo.Size = new System.Drawing.Size(54, 49);
            this.BtNuevo.TabIndex = 62;
            this.BtNuevo.UseVisualStyleBackColor = true;
            this.BtNuevo.Visible = false;
            this.BtNuevo.Click += new System.EventHandler(this.BtNuevo_Click_1);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(121, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(343, 592);
            this.pictureBox2.TabIndex = 66;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // BtEliminar
            // 
            this.BtEliminar.BackColor = System.Drawing.Color.Transparent;
            this.BtEliminar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtEliminar.BackgroundImage")));
            this.BtEliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtEliminar.Location = new System.Drawing.Point(27, 426);
            this.BtEliminar.Name = "BtEliminar";
            this.BtEliminar.Size = new System.Drawing.Size(55, 53);
            this.BtEliminar.TabIndex = 77;
            this.BtEliminar.UseVisualStyleBackColor = false;
            this.BtEliminar.Click += new System.EventHandler(this.BtEliminar_Click_1);
            // 
            // LbEliminar
            // 
            this.LbEliminar.AutoSize = true;
            this.LbEliminar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbEliminar.Location = new System.Drawing.Point(26, 482);
            this.LbEliminar.Name = "LbEliminar";
            this.LbEliminar.Size = new System.Drawing.Size(43, 13);
            this.LbEliminar.TabIndex = 78;
            this.LbEliminar.Text = "Eliminar";
            // 
            // producto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(585, 578);
            this.Controls.Add(this.LbEliminar);
            this.Controls.Add(this.BtEliminar);
            this.Controls.Add(this.LbCancelar);
            this.Controls.Add(this.BtCancelar);
            this.Controls.Add(this.BtModificar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.LbSalir);
            this.Controls.Add(this.LbActualizar);
            this.Controls.Add(this.LbNuevo);
            this.Controls.Add(this.LbBuscar);
            this.Controls.Add(this.BtSalir);
            this.Controls.Add(this.BtActualizar);
            this.Controls.Add(this.BtConsultar);
            this.Controls.Add(this.BtNuevo);
            this.Controls.Add(this.LbCodigoProducto);
            this.Controls.Add(this.TxbNombreProducto);
            this.Controls.Add(this.LbNombreProducto);
            this.Controls.Add(this.TxbFechaVencimiento);
            this.Controls.Add(this.LbFechaVencimiento);
            this.Controls.Add(this.TxbCantidadPastillero);
            this.Controls.Add(this.TxbCodigoProducto);
            this.Controls.Add(this.LbCantidadPastillero);
            this.Controls.Add(this.pictureBox2);
            this.Name = "producto";
            this.Text = "producto";
            this.Load += new System.EventHandler(this.producto_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nUEVOToolStripMenuItem;
        private System.Windows.Forms.Label LbCodigoProducto;
        private System.Windows.Forms.Label LbCantidadPastillero;
        private System.Windows.Forms.TextBox TxbCodigoProducto;
        private System.Windows.Forms.TextBox TxbCantidadPastillero;
        private System.Windows.Forms.Label LbFechaVencimiento;
        private System.Windows.Forms.TextBox TxbFechaVencimiento;
        private System.Windows.Forms.Label LbNombreProducto;
        private System.Windows.Forms.TextBox TxbNombreProducto;
        private System.Windows.Forms.Label LbCancelar;
        private System.Windows.Forms.Button BtCancelar;
        private System.Windows.Forms.Button BtModificar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label LbSalir;
        private System.Windows.Forms.Label LbActualizar;
        private System.Windows.Forms.Label LbNuevo;
        private System.Windows.Forms.Label LbBuscar;
        private System.Windows.Forms.Button BtSalir;
        private System.Windows.Forms.Button BtActualizar;
        private System.Windows.Forms.Button BtConsultar;
        private System.Windows.Forms.Button BtNuevo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button BtEliminar;
        private System.Windows.Forms.Label LbEliminar;
    }
}