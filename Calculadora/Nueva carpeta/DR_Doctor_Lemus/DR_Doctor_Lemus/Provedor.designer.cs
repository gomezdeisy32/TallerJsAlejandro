﻿namespace DR_Doctor_Lemus
{
    partial class Provedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Provedor));
            this.TxbCod_Proveedor = new System.Windows.Forms.TextBox();
            this.Txb_CodigoImportacion = new System.Windows.Forms.TextBox();
            this.Txb_Telefono = new System.Windows.Forms.TextBox();
            this.Txb_Nombre = new System.Windows.Forms.TextBox();
            this.Lb_CodigoImportacion = new System.Windows.Forms.Label();
            this.Lb_Telefono = new System.Windows.Forms.Label();
            this.Lb_Nombre = new System.Windows.Forms.Label();
            this.LbCod_Proveedor = new System.Windows.Forms.Label();
            this.LbCodifoFacturaC = new System.Windows.Forms.Label();
            this.TxbCod_FacturaC = new System.Windows.Forms.TextBox();
            this.BtModificar = new System.Windows.Forms.Button();
            this.LbSalir = new System.Windows.Forms.Label();
            this.LbActualizar = new System.Windows.Forms.Label();
            this.BtSalir = new System.Windows.Forms.Button();
            this.BtActualizar = new System.Windows.Forms.Button();
            this.LbCancelar = new System.Windows.Forms.Label();
            this.BtCancelar = new System.Windows.Forms.Button();
            this.LbNuevo = new System.Windows.Forms.Label();
            this.LbBuscar = new System.Windows.Forms.Label();
            this.BtConsultar = new System.Windows.Forms.Button();
            this.BtNuevo = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // TxbCod_Proveedor
            // 
            this.TxbCod_Proveedor.Location = new System.Drawing.Point(306, 68);
            this.TxbCod_Proveedor.Name = "TxbCod_Proveedor";
            this.TxbCod_Proveedor.Size = new System.Drawing.Size(133, 20);
            this.TxbCod_Proveedor.TabIndex = 56;
            // 
            // Txb_CodigoImportacion
            // 
            this.Txb_CodigoImportacion.Enabled = false;
            this.Txb_CodigoImportacion.Location = new System.Drawing.Point(306, 289);
            this.Txb_CodigoImportacion.Name = "Txb_CodigoImportacion";
            this.Txb_CodigoImportacion.Size = new System.Drawing.Size(133, 20);
            this.Txb_CodigoImportacion.TabIndex = 52;
            // 
            // Txb_Telefono
            // 
            this.Txb_Telefono.Enabled = false;
            this.Txb_Telefono.Location = new System.Drawing.Point(306, 233);
            this.Txb_Telefono.Name = "Txb_Telefono";
            this.Txb_Telefono.Size = new System.Drawing.Size(133, 20);
            this.Txb_Telefono.TabIndex = 51;
            this.Txb_Telefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txb_Telefono_KeyPress);
            // 
            // Txb_Nombre
            // 
            this.Txb_Nombre.Enabled = false;
            this.Txb_Nombre.Location = new System.Drawing.Point(306, 115);
            this.Txb_Nombre.Name = "Txb_Nombre";
            this.Txb_Nombre.Size = new System.Drawing.Size(133, 20);
            this.Txb_Nombre.TabIndex = 49;
            this.Txb_Nombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Txb_Nombre_KeyPress);
            // 
            // Lb_CodigoImportacion
            // 
            this.Lb_CodigoImportacion.AutoSize = true;
            this.Lb_CodigoImportacion.BackColor = System.Drawing.Color.Transparent;
            this.Lb_CodigoImportacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_CodigoImportacion.ForeColor = System.Drawing.SystemColors.Control;
            this.Lb_CodigoImportacion.Location = new System.Drawing.Point(135, 289);
            this.Lb_CodigoImportacion.Name = "Lb_CodigoImportacion";
            this.Lb_CodigoImportacion.Size = new System.Drawing.Size(165, 20);
            this.Lb_CodigoImportacion.TabIndex = 48;
            this.Lb_CodigoImportacion.Text = "Codigo Importacion";
            this.toolTip1.SetToolTip(this.Lb_CodigoImportacion, "recuerde que su codigo debe inicial con una letra");
            // 
            // Lb_Telefono
            // 
            this.Lb_Telefono.AutoSize = true;
            this.Lb_Telefono.BackColor = System.Drawing.Color.Transparent;
            this.Lb_Telefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Telefono.ForeColor = System.Drawing.SystemColors.Control;
            this.Lb_Telefono.Location = new System.Drawing.Point(188, 234);
            this.Lb_Telefono.Name = "Lb_Telefono";
            this.Lb_Telefono.Size = new System.Drawing.Size(79, 20);
            this.Lb_Telefono.TabIndex = 47;
            this.Lb_Telefono.Text = "Telefono";
            // 
            // Lb_Nombre
            // 
            this.Lb_Nombre.AutoSize = true;
            this.Lb_Nombre.BackColor = System.Drawing.Color.Transparent;
            this.Lb_Nombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lb_Nombre.ForeColor = System.Drawing.SystemColors.Control;
            this.Lb_Nombre.Location = new System.Drawing.Point(204, 115);
            this.Lb_Nombre.Name = "Lb_Nombre";
            this.Lb_Nombre.Size = new System.Drawing.Size(71, 20);
            this.Lb_Nombre.TabIndex = 45;
            this.Lb_Nombre.Text = "Nombre";
            // 
            // LbCod_Proveedor
            // 
            this.LbCod_Proveedor.AutoSize = true;
            this.LbCod_Proveedor.BackColor = System.Drawing.Color.Transparent;
            this.LbCod_Proveedor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCod_Proveedor.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCod_Proveedor.Location = new System.Drawing.Point(150, 68);
            this.LbCod_Proveedor.Name = "LbCod_Proveedor";
            this.LbCod_Proveedor.Size = new System.Drawing.Size(151, 20);
            this.LbCod_Proveedor.TabIndex = 44;
            this.LbCod_Proveedor.Text = "Codigo Proveedor";
            this.toolTip1.SetToolTip(this.LbCod_Proveedor, "recuerde que su codigo debe inicial con una letra\r\n");
            // 
            // LbCodifoFacturaC
            // 
            this.LbCodifoFacturaC.AutoSize = true;
            this.LbCodifoFacturaC.BackColor = System.Drawing.Color.Transparent;
            this.LbCodifoFacturaC.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodifoFacturaC.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCodifoFacturaC.Location = new System.Drawing.Point(166, 356);
            this.LbCodifoFacturaC.Name = "LbCodifoFacturaC";
            this.LbCodifoFacturaC.Size = new System.Drawing.Size(138, 20);
            this.LbCodifoFacturaC.TabIndex = 57;
            this.LbCodifoFacturaC.Text = "Factura Compra";
            // 
            // TxbCod_FacturaC
            // 
            this.TxbCod_FacturaC.Enabled = false;
            this.TxbCod_FacturaC.Location = new System.Drawing.Point(306, 352);
            this.TxbCod_FacturaC.Name = "TxbCod_FacturaC";
            this.TxbCod_FacturaC.Size = new System.Drawing.Size(133, 20);
            this.TxbCod_FacturaC.TabIndex = 60;
            this.TxbCod_FacturaC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCod_FacturaC_KeyPress);
            // 
            // BtModificar
            // 
            this.BtModificar.BackColor = System.Drawing.Color.Transparent;
            this.BtModificar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtModificar.BackgroundImage")));
            this.BtModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtModificar.Enabled = false;
            this.BtModificar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtModificar.Location = new System.Drawing.Point(505, 185);
            this.BtModificar.Name = "BtModificar";
            this.BtModificar.Size = new System.Drawing.Size(53, 56);
            this.BtModificar.TabIndex = 117;
            this.BtModificar.UseVisualStyleBackColor = false;
            this.BtModificar.Visible = false;
            this.BtModificar.Click += new System.EventHandler(this.BtModificar_Click);
            // 
            // LbSalir
            // 
            this.LbSalir.AutoSize = true;
            this.LbSalir.BackColor = System.Drawing.Color.DimGray;
            this.LbSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.LbSalir.Location = new System.Drawing.Point(521, 354);
            this.LbSalir.Name = "LbSalir";
            this.LbSalir.Size = new System.Drawing.Size(27, 13);
            this.LbSalir.TabIndex = 116;
            this.LbSalir.Text = "Salir";
            // 
            // LbActualizar
            // 
            this.LbActualizar.AutoSize = true;
            this.LbActualizar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbActualizar.Enabled = false;
            this.LbActualizar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbActualizar.Location = new System.Drawing.Point(506, 134);
            this.LbActualizar.Name = "LbActualizar";
            this.LbActualizar.Size = new System.Drawing.Size(53, 13);
            this.LbActualizar.TabIndex = 115;
            this.LbActualizar.Text = "Actualizar";
            this.LbActualizar.Visible = false;
            // 
            // BtSalir
            // 
            this.BtSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtSalir.BackgroundImage")));
            this.BtSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtSalir.Location = new System.Drawing.Point(505, 292);
            this.BtSalir.Name = "BtSalir";
            this.BtSalir.Size = new System.Drawing.Size(53, 48);
            this.BtSalir.TabIndex = 114;
            this.BtSalir.UseVisualStyleBackColor = true;
            this.BtSalir.Click += new System.EventHandler(this.BtSalir_Click_1);
            // 
            // BtActualizar
            // 
            this.BtActualizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtActualizar.BackgroundImage")));
            this.BtActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtActualizar.Enabled = false;
            this.BtActualizar.Location = new System.Drawing.Point(504, 72);
            this.BtActualizar.Name = "BtActualizar";
            this.BtActualizar.Size = new System.Drawing.Size(54, 46);
            this.BtActualizar.TabIndex = 113;
            this.BtActualizar.UseVisualStyleBackColor = true;
            this.BtActualizar.Visible = false;
            this.BtActualizar.Click += new System.EventHandler(this.BtActualizar_Click_1);
            // 
            // LbCancelar
            // 
            this.LbCancelar.AutoSize = true;
            this.LbCancelar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbCancelar.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCancelar.Location = new System.Drawing.Point(34, 354);
            this.LbCancelar.Name = "LbCancelar";
            this.LbCancelar.Size = new System.Drawing.Size(49, 13);
            this.LbCancelar.TabIndex = 123;
            this.LbCancelar.Text = "Cancelar";
            // 
            // BtCancelar
            // 
            this.BtCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtCancelar.BackgroundImage")));
            this.BtCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtCancelar.Location = new System.Drawing.Point(29, 287);
            this.BtCancelar.Name = "BtCancelar";
            this.BtCancelar.Size = new System.Drawing.Size(55, 53);
            this.BtCancelar.TabIndex = 122;
            this.BtCancelar.UseVisualStyleBackColor = false;
            this.BtCancelar.Click += new System.EventHandler(this.BtCancelar_Click);
            // 
            // LbNuevo
            // 
            this.LbNuevo.AutoSize = true;
            this.LbNuevo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbNuevo.Enabled = false;
            this.LbNuevo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbNuevo.Location = new System.Drawing.Point(33, 247);
            this.LbNuevo.Name = "LbNuevo";
            this.LbNuevo.Size = new System.Drawing.Size(39, 13);
            this.LbNuevo.TabIndex = 121;
            this.LbNuevo.Text = "Nuevo";
            this.LbNuevo.Visible = false;
            // 
            // LbBuscar
            // 
            this.LbBuscar.AutoSize = true;
            this.LbBuscar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbBuscar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LbBuscar.Location = new System.Drawing.Point(32, 133);
            this.LbBuscar.Name = "LbBuscar";
            this.LbBuscar.Size = new System.Drawing.Size(40, 13);
            this.LbBuscar.TabIndex = 120;
            this.LbBuscar.Text = "Buscar";
            // 
            // BtConsultar
            // 
            this.BtConsultar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtConsultar.BackgroundImage")));
            this.BtConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtConsultar.Location = new System.Drawing.Point(29, 68);
            this.BtConsultar.Name = "BtConsultar";
            this.BtConsultar.Size = new System.Drawing.Size(54, 50);
            this.BtConsultar.TabIndex = 119;
            this.BtConsultar.UseVisualStyleBackColor = true;
            this.BtConsultar.Click += new System.EventHandler(this.BtConsultar_Click_1);
            // 
            // BtNuevo
            // 
            this.BtNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtNuevo.BackgroundImage")));
            this.BtNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtNuevo.Enabled = false;
            this.BtNuevo.Location = new System.Drawing.Point(30, 185);
            this.BtNuevo.Name = "BtNuevo";
            this.BtNuevo.Size = new System.Drawing.Size(54, 49);
            this.BtNuevo.TabIndex = 118;
            this.BtNuevo.UseVisualStyleBackColor = true;
            this.BtNuevo.Visible = false;
            this.BtNuevo.Click += new System.EventHandler(this.BtNuevo_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(139, 424);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(308, 137);
            this.dataGridView1.TabIndex = 125;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(121, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(343, 592);
            this.pictureBox2.TabIndex = 124;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // Provedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(585, 578);
            this.Controls.Add(this.LbCancelar);
            this.Controls.Add(this.BtCancelar);
            this.Controls.Add(this.LbNuevo);
            this.Controls.Add(this.LbBuscar);
            this.Controls.Add(this.BtConsultar);
            this.Controls.Add(this.BtNuevo);
            this.Controls.Add(this.BtModificar);
            this.Controls.Add(this.LbSalir);
            this.Controls.Add(this.LbActualizar);
            this.Controls.Add(this.BtSalir);
            this.Controls.Add(this.BtActualizar);
            this.Controls.Add(this.TxbCod_FacturaC);
            this.Controls.Add(this.LbCodifoFacturaC);
            this.Controls.Add(this.TxbCod_Proveedor);
            this.Controls.Add(this.Txb_CodigoImportacion);
            this.Controls.Add(this.Txb_Telefono);
            this.Controls.Add(this.Txb_Nombre);
            this.Controls.Add(this.Lb_CodigoImportacion);
            this.Controls.Add(this.Lb_Telefono);
            this.Controls.Add(this.Lb_Nombre);
            this.Controls.Add(this.LbCod_Proveedor);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.pictureBox2);
            this.Name = "Provedor";
            this.Text = "Provedor";
            this.Load += new System.EventHandler(this.Provedor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxbCod_Proveedor;
        private System.Windows.Forms.TextBox Txb_CodigoImportacion;
        private System.Windows.Forms.TextBox Txb_Telefono;
        private System.Windows.Forms.TextBox Txb_Nombre;
        private System.Windows.Forms.Label Lb_CodigoImportacion;
        private System.Windows.Forms.Label Lb_Telefono;
        private System.Windows.Forms.Label Lb_Nombre;
        private System.Windows.Forms.Label LbCod_Proveedor;
        private System.Windows.Forms.Label LbCodifoFacturaC;
        private System.Windows.Forms.TextBox TxbCod_FacturaC;
        private System.Windows.Forms.Button BtModificar;
        private System.Windows.Forms.Label LbSalir;
        private System.Windows.Forms.Label LbActualizar;
        private System.Windows.Forms.Button BtSalir;
        private System.Windows.Forms.Button BtActualizar;
        private System.Windows.Forms.Label LbCancelar;
        private System.Windows.Forms.Button BtCancelar;
        private System.Windows.Forms.Label LbNuevo;
        private System.Windows.Forms.Label LbBuscar;
        private System.Windows.Forms.Button BtConsultar;
        private System.Windows.Forms.Button BtNuevo;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolTip toolTip1;

    }
}