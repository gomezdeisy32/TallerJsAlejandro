﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DR_Doctor_Lemus;


namespace DR_Doctor_Lemus
{
    public partial class Bodega : Form
    {
        public Bodega()
        {
            InitializeComponent();
        }

        private void BtSalir_Click(object sender, EventArgs e)
        {
            Principal bodega= new Principal();
            bodega.Show();
            this.Hide();

        }

        private void BtNuevo_Click(object sender, EventArgs e)
        {
            if (TxbCantidad.Text == "")
            {
                MessageBox.Show("el campo cantidad esta  vacion");
                 TxbCantidad.Focus();


            }
            else if (TxbCodProducto.Text == "")
            {
                MessageBox.Show("el campo codigo producto esta vacio");
                TxbCodProducto.Focus();
            }
            
            else
            {
                SqlConnection con = new SqlConnection("Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");


                SqlCommand cmd = new SqlCommand("insert into BODEGA(CANTIDAD,CODIGOPRODUCTO)Values('" + Convert.ToInt16(TxbCantidad.Text) + "','" + Convert.ToInt16 (TxbCodProducto.Text) +  "')", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se ha podido agregar con exito ");
            }  
        
    }

        private void BtConsultar_Click(object sender, EventArgs e)
        {
           if( TxbCodestanterias.Text ==""){
               MessageBox.Show("el campo codigo debe tener contenido");
           }else{
            
            SqlConnection con = new SqlConnection("Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            try
            {
                SqlCommand cmd = new SqlCommand
                ("select * from Bodega where CODIGOBODEGA ='" + TxbCodestanterias.Text + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "Bodega");
                DataRow dro;
                dro = ds.Tables["Bodega"].Rows[0];
                TxbCodProducto.Text = dro["CODIGOPRODUCTO"].ToString();
                TxbCantidad.Text = dro["CANTIDAD"].ToString();
                BtActualizar.Enabled = true;
                BtActualizar.Visible = true;
                


            }
            catch
            {
                //MessageBox.Show(" producto no encontrado");
                const string mensaje = "codigo no encontrado desea crearlo si/no";
                const string titulo = "consulta";
                var resultado = MessageBox.Show(mensaje, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resultado == DialogResult.No)
                {
                    TxbCodestanterias.Clear();
                    TxbCodestanterias.Focus();
                }
                else
                {
                    TxbCodestanterias.Enabled = false;
                    TxbCantidad.Clear();
                    TxbCodProducto.Clear();
                    TxbCodProducto.Enabled = true;
                    TxbCantidad.Enabled = true;
                    BtNuevo.Enabled  = true;
                    BtConsultar.Visible = false;
                    
                }
            }
            finally
            {
                con.Close();
            }
         }



        }

        private void BtActualizar_Click(object sender, EventArgs e)
        {
            BtActualizar.Enabled = false;
            TxbCodestanterias.Enabled = false;
            TxbCantidad.Enabled = true;
            TxbCodProducto.Enabled = true;
            BtModificar.Visible = true;
            

        }

        

        

        

        

        

        private void BtModificar_Click(object sender, EventArgs e)
        {
             //con = new SqlConnection("Data Source=JUAN-PC\SQLEXPRESS;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
           
            SqlCommand cmd = new SqlCommand("update BODEGA set CANTIDAD ='" + TxbCantidad.Text + "', CODIGOPRODUCTO = '" + TxbCodProducto.Text + "' where CODIGOBODEGA = '" + TxbCodestanterias.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            MessageBox.Show("se modifico  correctamente");

            TxbCodestanterias.Enabled = false;
            BtConsultar.Enabled = false;
        }

        private void BtActualizar_Click_1(object sender, EventArgs e)
        {
            habilitar();
            BtActualizar.Enabled = false;
            TxbCodestanterias.Enabled = false;
            BtModificar.Enabled = true;
            BtModificar.Visible = true;
        }

        private void Bodega_Load(object sender, EventArgs e)
        {
            Conexion objeto = new Conexion();
            objeto.ActualizarGrid(this.dataGridView1, "select * from ESTANTERIAS");
        }


        private void limpiar()
        {
            TxbCantidad.Clear();
            TxbCodestanterias.Clear();
            TxbCodProducto.Clear();
            BtActualizar.Visible = false;
            BtNuevo.Visible = false;
            BtModificar.Visible = false;


        }
        private void desabilitar()
        {

            TxbCodProducto.Enabled = false;
            TxbCantidad.Enabled = false;
            BtActualizar.Enabled = false;
            BtNuevo.Enabled = false;
            BtModificar.Enabled = false;

        }
        private void habilitar()
        {
            TxbCantidad.Enabled = true;
            TxbCodProducto.Enabled = true;
            BtActualizar.Enabled = true;
            BtNuevo.Enabled = true;
            BtModificar.Enabled = true;

        }

        private void BtConsultar_Click_1(object sender, EventArgs e)
        {
            if (TxbCodestanterias.Text == "")
            {

                MessageBox.Show("el campo no puede estar vacio");
                TxbCodestanterias.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                try
                {
                    SqlCommand cmd = new SqlCommand
                    ("select * from ESTANTERIAS where CODIGOESTANTERIA ='" + TxbCodestanterias.Text + "' ", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "ESTANTERIAS");
                    DataRow dro;
                    dro = ds.Tables["ESTANTERIAS"].Rows[0];
                    TxbCodProducto.Text = dro["CODIGOPRODUCTO"].ToString();
                    TxbCantidad.Text = dro["CANTIDAD"].ToString();
                    BtActualizar.Enabled = true;
                    BtActualizar.Visible = true;

                }
                catch
                {
                    //MessageBox.Show(" usuario no encontrado")
                    const string mensaje = "estateria no encontrada desea crearla si/no";
                    const string titulo = "consulta";
                    var resultado = MessageBox.Show(mensaje, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (resultado == DialogResult.No)
                    {
                        TxbCodestanterias.Clear();
                        TxbCodestanterias.Focus();
                    }
                    else
                    {


                        TxbCodestanterias.Enabled = false;
                        TxbCantidad.Clear();
                        TxbCodProducto.Clear();
                        TxbCodProducto.Enabled = true;
                        TxbCantidad.Enabled = true;

                        BtNuevo.Visible = true;
                        BtNuevo.Enabled = true;
                        BtConsultar.Enabled = false;


                    }

                }

                finally
                {
                    con.Close();
                }

            }
        }

        private void BtNuevo_Click_1(object sender, EventArgs e)
        {
            if (TxbCodestanterias.Text == "")
            {
                MessageBox.Show("el campo apellido esta  vacion");
                TxbCodestanterias.Focus();


            }
            else if (TxbCodProducto.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbCodProducto.Focus();
            }
            else if (TxbCantidad.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbCantidad.Focus();
            }


            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");


                SqlCommand cmd = new SqlCommand("insert into ESTANTERIAS(CODIGO_ENTREGA, CODIGOESTANTERIA,CODIGOPRODUCTO,CANTIDAD)Values('" + TxbCodestanterias.Text + "','" + Convert.ToInt16(TxbCodProducto.Text) + "','" + Convert.ToInt16(TxbCantidad.Text) + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se   agrego con exito ");
            }
        }

        private void BtModificar_Click_1(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");

            SqlCommand cmd = new SqlCommand("update ESTANTERIAS set CANTIDAD ='" + TxbCantidad.Text + "', CODIGOPRODUCTO = '" + TxbCodProducto.Text + "'  where CODIGOESTANTERIA = '" + TxbCodestanterias.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            MessageBox.Show("se modifico  correctamente");
            desabilitar();
            limpiar();
            TxbCodestanterias.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtCancelar_Click(object sender, EventArgs e)
        {
            desabilitar();
            limpiar();
            TxbCodestanterias.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtSalir_Click_1(object sender, EventArgs e)
        {

            Principal Estanteria = new Principal();
            Estanteria.Show();
            this.Hide();

        }

        private void TxbCodestanterias_TextChanged(object sender, EventArgs e)
        {

        }

        
        

        

       
        }
}
