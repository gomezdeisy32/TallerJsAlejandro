﻿namespace DR_Doctor_Lemus
{
    partial class Bodega
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Bodega));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.LbCod_Estanteria = new System.Windows.Forms.Label();
            this.LbCod_Producto = new System.Windows.Forms.Label();
            this.LbCantidad = new System.Windows.Forms.Label();
            this.TxbCodestanterias = new System.Windows.Forms.TextBox();
            this.TxbCodProducto = new System.Windows.Forms.TextBox();
            this.TxbCantidad = new System.Windows.Forms.TextBox();
            this.BtNuevo = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.LbBuscar = new System.Windows.Forms.Label();
            this.LbNuevo = new System.Windows.Forms.Label();
            this.BtCancelar = new System.Windows.Forms.Button();
            this.LbCancelar = new System.Windows.Forms.Label();
            this.BtActualizar = new System.Windows.Forms.Button();
            this.BtSalir = new System.Windows.Forms.Button();
            this.LbActualizar = new System.Windows.Forms.Label();
            this.LbSalir = new System.Windows.Forms.Label();
            this.BtModificar = new System.Windows.Forms.Button();
            this.BtConsultar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(121, -7);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(343, 607);
            this.pictureBox2.TabIndex = 113;
            this.pictureBox2.TabStop = false;
            // 
            // LbCod_Estanteria
            // 
            this.LbCod_Estanteria.AutoSize = true;
            this.LbCod_Estanteria.BackColor = System.Drawing.Color.Transparent;
            this.LbCod_Estanteria.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCod_Estanteria.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCod_Estanteria.Location = new System.Drawing.Point(145, 110);
            this.LbCod_Estanteria.Name = "LbCod_Estanteria";
            this.LbCod_Estanteria.Size = new System.Drawing.Size(153, 20);
            this.LbCod_Estanteria.TabIndex = 28;
            this.LbCod_Estanteria.Text = "Codigo Estanteria";
            // 
            // LbCod_Producto
            // 
            this.LbCod_Producto.AutoSize = true;
            this.LbCod_Producto.BackColor = System.Drawing.Color.Transparent;
            this.LbCod_Producto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCod_Producto.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCod_Producto.Location = new System.Drawing.Point(145, 192);
            this.LbCod_Producto.Name = "LbCod_Producto";
            this.LbCod_Producto.Size = new System.Drawing.Size(142, 20);
            this.LbCod_Producto.TabIndex = 29;
            this.LbCod_Producto.Text = "Codigo Producto";
            // 
            // LbCantidad
            // 
            this.LbCantidad.AutoSize = true;
            this.LbCantidad.BackColor = System.Drawing.Color.Transparent;
            this.LbCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCantidad.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCantidad.Location = new System.Drawing.Point(190, 269);
            this.LbCantidad.Name = "LbCantidad";
            this.LbCantidad.Size = new System.Drawing.Size(81, 20);
            this.LbCantidad.TabIndex = 31;
            this.LbCantidad.Text = "Cantidad";
            // 
            // TxbCodestanterias
            // 
            this.TxbCodestanterias.BackColor = System.Drawing.Color.White;
            this.TxbCodestanterias.Location = new System.Drawing.Point(309, 110);
            this.TxbCodestanterias.Name = "TxbCodestanterias";
            this.TxbCodestanterias.Size = new System.Drawing.Size(136, 20);
            this.TxbCodestanterias.TabIndex = 32;
            this.TxbCodestanterias.TextChanged += new System.EventHandler(this.TxbCodestanterias_TextChanged);
            // 
            // TxbCodProducto
            // 
            this.TxbCodProducto.Enabled = false;
            this.TxbCodProducto.Location = new System.Drawing.Point(309, 192);
            this.TxbCodProducto.Name = "TxbCodProducto";
            this.TxbCodProducto.Size = new System.Drawing.Size(136, 20);
            this.TxbCodProducto.TabIndex = 33;
            // 
            // TxbCantidad
            // 
            this.TxbCantidad.Enabled = false;
            this.TxbCantidad.Location = new System.Drawing.Point(305, 269);
            this.TxbCantidad.Name = "TxbCantidad";
            this.TxbCantidad.Size = new System.Drawing.Size(136, 20);
            this.TxbCantidad.TabIndex = 35;
            // 
            // BtNuevo
            // 
            this.BtNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtNuevo.BackgroundImage")));
            this.BtNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtNuevo.Enabled = false;
            this.BtNuevo.Location = new System.Drawing.Point(24, 210);
            this.BtNuevo.Name = "BtNuevo";
            this.BtNuevo.Size = new System.Drawing.Size(54, 49);
            this.BtNuevo.TabIndex = 102;
            this.BtNuevo.UseVisualStyleBackColor = true;
            this.BtNuevo.Visible = false;
            this.BtNuevo.Click += new System.EventHandler(this.BtNuevo_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(137, 397);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(308, 137);
            this.dataGridView1.TabIndex = 114;
            // 
            // LbBuscar
            // 
            this.LbBuscar.AutoSize = true;
            this.LbBuscar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbBuscar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LbBuscar.Location = new System.Drawing.Point(26, 158);
            this.LbBuscar.Name = "LbBuscar";
            this.LbBuscar.Size = new System.Drawing.Size(40, 13);
            this.LbBuscar.TabIndex = 104;
            this.LbBuscar.Text = "Buscar";
            // 
            // LbNuevo
            // 
            this.LbNuevo.AutoSize = true;
            this.LbNuevo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbNuevo.Enabled = false;
            this.LbNuevo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbNuevo.Location = new System.Drawing.Point(27, 272);
            this.LbNuevo.Name = "LbNuevo";
            this.LbNuevo.Size = new System.Drawing.Size(39, 13);
            this.LbNuevo.TabIndex = 105;
            this.LbNuevo.Text = "Nuevo";
            this.LbNuevo.Visible = false;
            // 
            // BtCancelar
            // 
            this.BtCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtCancelar.BackgroundImage")));
            this.BtCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtCancelar.Location = new System.Drawing.Point(24, 299);
            this.BtCancelar.Name = "BtCancelar";
            this.BtCancelar.Size = new System.Drawing.Size(55, 53);
            this.BtCancelar.TabIndex = 106;
            this.BtCancelar.UseVisualStyleBackColor = false;
            this.BtCancelar.Click += new System.EventHandler(this.BtCancelar_Click);
            // 
            // LbCancelar
            // 
            this.LbCancelar.AutoSize = true;
            this.LbCancelar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbCancelar.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCancelar.Location = new System.Drawing.Point(29, 366);
            this.LbCancelar.Name = "LbCancelar";
            this.LbCancelar.Size = new System.Drawing.Size(49, 13);
            this.LbCancelar.TabIndex = 107;
            this.LbCancelar.Text = "Cancelar";
            // 
            // BtActualizar
            // 
            this.BtActualizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtActualizar.BackgroundImage")));
            this.BtActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtActualizar.Enabled = false;
            this.BtActualizar.Location = new System.Drawing.Point(494, 110);
            this.BtActualizar.Name = "BtActualizar";
            this.BtActualizar.Size = new System.Drawing.Size(54, 46);
            this.BtActualizar.TabIndex = 108;
            this.BtActualizar.UseVisualStyleBackColor = true;
            this.BtActualizar.Visible = false;
            this.BtActualizar.Click += new System.EventHandler(this.BtActualizar_Click_1);
            // 
            // BtSalir
            // 
            this.BtSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtSalir.BackgroundImage")));
            this.BtSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtSalir.Location = new System.Drawing.Point(494, 332);
            this.BtSalir.Name = "BtSalir";
            this.BtSalir.Size = new System.Drawing.Size(53, 48);
            this.BtSalir.TabIndex = 109;
            this.BtSalir.UseVisualStyleBackColor = true;
            this.BtSalir.Click += new System.EventHandler(this.BtSalir_Click_1);
            // 
            // LbActualizar
            // 
            this.LbActualizar.AutoSize = true;
            this.LbActualizar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbActualizar.Enabled = false;
            this.LbActualizar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbActualizar.Location = new System.Drawing.Point(495, 159);
            this.LbActualizar.Name = "LbActualizar";
            this.LbActualizar.Size = new System.Drawing.Size(53, 13);
            this.LbActualizar.TabIndex = 110;
            this.LbActualizar.Text = "Actualizar";
            this.LbActualizar.Visible = false;
            // 
            // LbSalir
            // 
            this.LbSalir.AutoSize = true;
            this.LbSalir.BackColor = System.Drawing.Color.DimGray;
            this.LbSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.LbSalir.Location = new System.Drawing.Point(507, 397);
            this.LbSalir.Name = "LbSalir";
            this.LbSalir.Size = new System.Drawing.Size(27, 13);
            this.LbSalir.TabIndex = 111;
            this.LbSalir.Text = "Salir";
            // 
            // BtModificar
            // 
            this.BtModificar.BackColor = System.Drawing.Color.Transparent;
            this.BtModificar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtModificar.BackgroundImage")));
            this.BtModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtModificar.Enabled = false;
            this.BtModificar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtModificar.Location = new System.Drawing.Point(494, 210);
            this.BtModificar.Name = "BtModificar";
            this.BtModificar.Size = new System.Drawing.Size(53, 56);
            this.BtModificar.TabIndex = 112;
            this.BtModificar.UseVisualStyleBackColor = false;
            this.BtModificar.Visible = false;
            this.BtModificar.Click += new System.EventHandler(this.BtModificar_Click_1);
            // 
            // BtConsultar
            // 
            this.BtConsultar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtConsultar.BackgroundImage")));
            this.BtConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtConsultar.Location = new System.Drawing.Point(23, 93);
            this.BtConsultar.Name = "BtConsultar";
            this.BtConsultar.Size = new System.Drawing.Size(54, 50);
            this.BtConsultar.TabIndex = 103;
            this.BtConsultar.UseVisualStyleBackColor = true;
            this.BtConsultar.Click += new System.EventHandler(this.BtConsultar_Click_1);
            // 
            // Bodega
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(585, 578);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.BtModificar);
            this.Controls.Add(this.LbSalir);
            this.Controls.Add(this.LbActualizar);
            this.Controls.Add(this.BtSalir);
            this.Controls.Add(this.BtActualizar);
            this.Controls.Add(this.LbCancelar);
            this.Controls.Add(this.BtCancelar);
            this.Controls.Add(this.LbNuevo);
            this.Controls.Add(this.LbBuscar);
            this.Controls.Add(this.BtConsultar);
            this.Controls.Add(this.BtNuevo);
            this.Controls.Add(this.TxbCantidad);
            this.Controls.Add(this.TxbCodProducto);
            this.Controls.Add(this.TxbCodestanterias);
            this.Controls.Add(this.LbCantidad);
            this.Controls.Add(this.LbCod_Producto);
            this.Controls.Add(this.LbCod_Estanteria);
            this.Controls.Add(this.pictureBox2);
            this.Name = "Bodega";
            this.Text = "Bodega";
            this.Load += new System.EventHandler(this.Bodega_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label LbCod_Estanteria;
        private System.Windows.Forms.Label LbCod_Producto;
        private System.Windows.Forms.Label LbCantidad;
        private System.Windows.Forms.TextBox TxbCodestanterias;
        private System.Windows.Forms.TextBox TxbCodProducto;
        private System.Windows.Forms.TextBox TxbCantidad;
        private System.Windows.Forms.Button BtNuevo;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label LbBuscar;
        private System.Windows.Forms.Label LbNuevo;
        private System.Windows.Forms.Button BtCancelar;
        private System.Windows.Forms.Label LbCancelar;
        private System.Windows.Forms.Button BtActualizar;
        private System.Windows.Forms.Button BtSalir;
        private System.Windows.Forms.Label LbActualizar;
        private System.Windows.Forms.Label LbSalir;
        private System.Windows.Forms.Button BtModificar;
        private System.Windows.Forms.Button BtConsultar;

    }
}