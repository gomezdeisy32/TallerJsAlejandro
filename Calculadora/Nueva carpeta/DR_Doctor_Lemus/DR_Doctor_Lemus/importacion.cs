﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DR_Doctor_Lemus;
using System.Data.SqlClient;

namespace DR_Doctor_Lemus
{
    public partial class importacion : Form
    {
        public importacion()
        {
            InitializeComponent();
        }
        System.Data.SqlClient.SqlConnection con;

        private void BtSalir_Click(object sender, EventArgs e)
        {
            Principal importacion = new Principal();
            importacion.Show();
            this.Hide();
        }










        private void limpiar()
        {
            TxbCantidad.Clear();
            TxbFechaExpedicion.Clear();
            TxbFechaImportacion.Clear();
            TxbCodigo_Importacion.Clear();
            BtActualizar.Visible = false;
            BtNuevo.Visible = false;
            BtModificar.Visible = false;


        }
        private void desabilitar()
        {

            TxbCantidad.Enabled = false;
            TxbFechaExpedicion.Enabled = false;
            TxbFechaImportacion.Enabled = false;
            BtActualizar.Enabled = false;
            BtNuevo.Enabled = false;
            BtModificar.Enabled = false;

        }
        private void habilitar()
        {
            TxbCantidad.Enabled = true;
            TxbFechaExpedicion.Enabled = true;
            TxbFechaImportacion.Enabled = true;
            BtActualizar.Enabled = true;
            BtNuevo.Enabled = true;
            BtModificar.Enabled = true;

        }







        private void BtModificar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");

            SqlCommand cmd = new SqlCommand("update IMPORTACION set CANTIDAD ='" + TxbCantidad.Text + "', FECHA_IMPORTACION = '" + TxbFechaImportacion.Text + "', FECHA_EXPEDICION = '" + TxbFechaExpedicion.Text + "' where CODIGO_IMPORTACION = '" + TxbCodigo_Importacion.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            MessageBox.Show("se modifico  correctamente");
            desabilitar();
            limpiar();
            TxbCodigo_Importacion.Enabled = true;
            BtConsultar.Enabled = true;

        }

        private void BtConsultar_Click_1(object sender, EventArgs e)
        {
            if (TxbCodigo_Importacion.Text == "")
            {

                MessageBox.Show("el campo no puede estar vacio");
                TxbCodigo_Importacion.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                try
                {
                    SqlCommand cmd = new SqlCommand
                    ("select * from IMPORTACION where CODIGO_IMPORTACION ='" + TxbCodigo_Importacion.Text + "' ", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "IMPORTACION");
                    DataRow dro;
                    dro = ds.Tables["IMPORTACION"].Rows[0];
                    TxbCantidad.Text = dro["CANTIDAD"].ToString();
                    TxbFechaExpedicion.Text = dro["FECHA_EXPEDICION"].ToString();
                    TxbFechaImportacion.Text = dro["FECHA_IMPORTACION"].ToString();
                    BtActualizar.Enabled = true;
                    BtActualizar.Visible = true;

                }
                catch
                {
                    //MessageBox.Show(" usuario no encontrado")
                    const string mensaje = "importacion no encontrada desea crearla si/no";
                    const string titulo = "consulta";
                    var resultado = MessageBox.Show(mensaje, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (resultado == DialogResult.No)
                    {
                        TxbCodigo_Importacion.Clear();
                        TxbCodigo_Importacion.Focus();
                    }
                    else
                    {


                        TxbCodigo_Importacion.Enabled = false;
                        TxbCantidad.Clear();
                        TxbFechaExpedicion.Clear();
                        TxbFechaImportacion.Clear();
                        TxbFechaImportacion.Enabled = true;
                        TxbFechaExpedicion.Enabled = true;
                        LbCantidad.Enabled = true;

                        BtNuevo.Visible = true;
                        BtNuevo.Enabled = true;
                        BtConsultar.Enabled = false;

                    }

                }

                finally
                {
                    con.Close();
                }

            }

        }

        private void BtNuevo_Click_1(object sender, EventArgs e)
        {
            if (TxbCodigo_Importacion.Text == "")
            {
                MessageBox.Show("el campo apellido esta  vacion");
                TxbCodigo_Importacion.Focus();


            }
            else if (LbCantidad.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                LbCantidad.Focus();
            }
            else if (TxbFechaImportacion.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbFechaImportacion.Focus();
            }
            else if (TxbFechaExpedicion.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbFechaExpedicion.Focus();
            }

            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");


                SqlCommand cmd = new SqlCommand("insert into IMPORTACION(CODIGO_IMPORTACION, CANTIDAD ,FECHA_EXPEDICION,FECHA_IMPORTACION)Values('" + TxbCodigo_Importacion.Text + "','" + Convert.ToInt16(TxbCantidad.Text) + "','" + TxbFechaExpedicion.Text + "', '" + TxbFechaImportacion.Text + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se   agrego con exito ");
            }

        }

        private void BtCancelar_Click(object sender, EventArgs e)
        {
            desabilitar();
            limpiar();
            TxbCodigo_Importacion.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtActualizar_Click(object sender, EventArgs e)
        {
            habilitar();
            BtActualizar.Enabled = false;
            TxbCodigo_Importacion.Enabled = false;
            BtModificar.Enabled = true;
            BtModificar.Visible = true;

        }

        private void BtSalir_Click_1(object sender, EventArgs e)
        {
            Principal importacion = new Principal();
            importacion.Show();
            this.Hide();

        }

        private void importacion_Load(object sender, EventArgs e)
        {
            Conexion objeto = new Conexion();
            objeto.ActualizarGrid(this.dataGridView1, "select * from IMPORTACION");
        }

      
        private void TxbCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbCantidad_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxbFechaExpedicion_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxbFechaImportacion_TextChanged(object sender, EventArgs e)
        {

        }

        

    } 
    }


