﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

using DR_Doctor_Lemus;

namespace DR_Doctor_Lemus
{
    public partial class facturaventa : Form
    {
        public facturaventa()
        {
            InitializeComponent();
        }

       

        private void BtSalir_Click(object sender, EventArgs e)
        {
            Principal facturaventa = new Principal();
            facturaventa.Show();
            this.Hide();

        }

        private void BtNuevo_Click(object sender, EventArgs e)
        {
            // comprobar que todos los datos ingresaron
            if ( (TxbCodigoCliente.Text == "") || (TxbCodigoFVenta.Text == "") || (TxbFechaFactura.Text == ""))
            {
                MessageBox.Show("ha dejado un espacio en blanco");
            }
            else
            {
                try
                {
                    //creamos el archivo de conexion y le damos la ruta
                    SqlConnection conex = new SqlConnection("Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                    // abrimos la conexion

                    conex.Open();
                    //creamos el comado de sql
                    SqlCommand cmd = new SqlCommand(" insert into FACTURAVENTA(codigocliente,codigofacturaventa,fechafactura)values('" + TxbCodigoCliente.Text + ",  '" + TxbCodigoFVenta.Text + ", '" + TxbFechaFactura.Text +  "')");
                    //ejecutamos la conexion con la sentencia de sql
                    cmd.Connection = conex;
                    cmd.ExecuteNonQuery();
                    try
                    {
                        MessageBox.Show("registro exitoso");
                        //cerramos la conexion
                        conex.Close();
                        //limpiamos el formulario
                        TxbCodigoCliente.Clear();
                        TxbCodigoFVenta.Clear();
                        TxbFechaFactura.Clear();
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("el reegistro ya existe");
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("no" + ex);

                }


            }
        }

        private void BtConsultar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            try
            {
                SqlCommand cmd = new SqlCommand
                ("select * from factura_venta where CODIGOFACTURA ='" + TxbCodigoFVenta.Text + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "facturaventa ");
                DataRow dro;
                dro = ds.Tables["facturaventa "].Rows[0];
                TxbCodigoCliente.Text = dro["CODIGO_CLIENTE"].ToString();
                TxbFechaFactura.Text = dro["FECHAFACTURA"].ToString();

            }
            catch
            {
                MessageBox.Show(" factura no encontrada");
            }
            finally
            {
                con.Close();
            }

        }

        private void BtActualizar_Click(object sender, EventArgs e)
        {
            //conexion a base de datos
            SqlConnection conn = new SqlConnection("Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            //abro conexion
            conn.Open();
            //buscamos el nombre y el registro a actualizar para verificar
            string consulta = "select  CODIGOCLIENTE  from FACTURAVENTA where fechafactura = '" + TxbFechaFactura + "'";
            SqlCommand COMAND = new SqlCommand(consulta, conn);
            // El resultado lo guardaremos en una tabla

            DataTable tabla = new DataTable();
            // Usaremos un DataAdapter para leer los datos
            SqlDataAdapter AdaptadorTabla = new SqlDataAdapter(consulta, conn);
            //DataSet ds = new DataSet();
            // Llenamos la tabla con los datos leídos
            AdaptadorTabla.Fill(tabla);
            //guardo informacion en variables
            string codigocliente = tabla.Rows[0]["CODIGOCLIENTE"].ToString();
            string fechafactura = tabla.Rows[0]["FECHAFACTURA"].ToString();
            // Creamos el cuadro de dialogo y guardamos la respuesta que da el usuario
            DialogResult respuesta = MessageBox.Show("Desea actualizar el registro de " + fechafactura + " " + codigocliente + ".", "actualizar", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            //Si acepto actualizar se ejecuta
            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {
                string StrComando = "DELETE FROM facturaventa WHERE fechafactura =  '" + TxbFechaFactura + "'";
                SqlCommand COMANDO = new SqlCommand(StrComando, conn);
                COMANDO.ExecuteNonQuery();
                MessageBox.Show("El registro seactualizo de manera  EXITOSA");


            }
            else
            {
                MessageBox.Show("el registro no se actualizo");

            }
    
            TxbCodigoCliente.Text = "";
            TxbCodigoFVenta.Text = "";
            TxbFechaFactura.Text = "";
        }

        private void TxbCodigoFVenta_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }
        }

        private void TxbFechaFactura_TextChanged(object sender, EventArgs e)
        {

        }

        private void TxbCodigoCliente_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }
        }



        private void limpiar()
        {
            TxbCodgioEntrega.Clear();
            TxbCodigoCliente.Clear();
            TxbCodigoEmpleado.Clear();
            TxbCodigoFVenta.Clear();
            TxbFechaFactura.Clear();
            BtActualizar.Visible = false;
            BtNuevo.Visible = false;
            BtModificar.Visible = false;


        }
        private void desabilitar()
        {

            TxbCodgioEntrega.Enabled = false;
            TxbCodigoCliente.Enabled = false;
            TxbCodigoEmpleado.Enabled = false;
            TxbFechaFactura.Enabled = false;
            BtActualizar.Enabled = false;
            BtNuevo.Enabled = false;
            BtModificar.Enabled = false;

        }
        private void habilitar()
        {
            TxbCodgioEntrega.Enabled = true;
            TxbCodigoCliente.Enabled = true;
            TxbCodigoEmpleado.Enabled = true;
            TxbFechaFactura.Enabled = true;
            BtActualizar.Enabled = true;
            BtNuevo.Enabled = true;
            BtModificar.Enabled = true;

        }

        private void BtConsultar_Click_1(object sender, EventArgs e)
        {
            if (TxbCodigoFVenta.Text == "")
            {

                MessageBox.Show("el campo no puede estar vacio");
                TxbCodigoFVenta.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                try
                {
                    SqlCommand cmd = new SqlCommand
                    ("select * from FACTURA_VENTA where CODIGOFACTURA ='" + TxbCodigoFVenta.Text + "' ", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "FACTURA_VENTA");
                    DataRow dro;
                    dro = ds.Tables["FACTURA_VENTA"].Rows[0];
                    TxbFechaFactura.Text = dro["FECHAFACTURA"].ToString();
                    TxbCodigoCliente.Text = dro["CODIGO_CLIENTE"].ToString();
                    TxbCodgioEntrega.Text = dro["CODIGO_ENTREGA"].ToString();
                    TxbCodigoEmpleado.Text = dro["CODIGOEMPLEADO"].ToString();
                    BtActualizar.Enabled = true;
                    BtActualizar.Visible = true;

                }
                catch
                {
                    //MessageBox.Show(" usuario no encontrado")
                    const string mensaje = "factura no encontrada desea crearla si/no";
                    const string titulo = "consulta";
                    var resultado = MessageBox.Show(mensaje, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (resultado == DialogResult.No)
                    {
                        TxbCodigoFVenta.Clear();
                        TxbCodigoFVenta.Focus();
                    }
                    else
                    {


                        TxbCodigoFVenta.Enabled = false;
                        TxbFechaFactura.Clear();
                        TxbCodigoEmpleado.Clear();
                        TxbCodigoCliente.Clear();
                        TxbCodgioEntrega.Clear();
                        TxbFechaFactura.Enabled = true;
                        TxbCodigoEmpleado.Enabled = true;
                        TxbCodigoCliente.Enabled = true;
                        TxbCodgioEntrega.Enabled = true;

                        BtNuevo.Visible = true;
                        BtNuevo.Enabled = true;
                        BtConsultar.Enabled = false;

                    }

                }

                finally
                {
                    con.Close();
                }

            }
        }

        private void BtNuevo_Click_1(object sender, EventArgs e)
        {
            if (TxbCodigoFVenta.Text == "")
            {
                MessageBox.Show("el campo apellido esta  vacion");
                TxbCodigoFVenta.Focus();


            }
            else if (TxbFechaFactura.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbFechaFactura.Focus();
            }
            else if (TxbCodigoCliente.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbCodigoCliente.Focus();
            }
            else if (TxbCodgioEntrega.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbCodgioEntrega.Focus();
            }
            else if (TxbCodigoEmpleado.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbCodigoEmpleado.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");


                SqlCommand cmd = new SqlCommand("insert into FACTURA_VENTA(CODIGOFACTURA, FECHAFACTURA,CODIGO_CLIENTE,CODIGO_ENTREGA,CODIGOEMPLEADO)Values('" + Convert.ToInt16(TxbCodigoFVenta.Text) + "','" + TxbFechaFactura.Text + "','" + Convert.ToInt16(TxbCodigoCliente.Text) + "', '" + TxbCodgioEntrega.Text + "', '" + Convert.ToInt16(TxbCodigoEmpleado.Text) + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se   agrego con exito ");
            }
        }

        private void BtActualizar_Click_1(object sender, EventArgs e)
        {
            habilitar();
            BtActualizar.Enabled = false;
            TxbCodigoFVenta.Enabled = false;
            BtModificar.Enabled = true;
            BtModificar.Visible = true;
        }

        private void BtModificar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");

            SqlCommand cmd = new SqlCommand("update FACTURA_VENTA set FECHAFACTURA ='" +  TxbFechaFactura.Text + "', CODIGO_CLIENTE = '" + Convert.ToInt16(TxbCodigoCliente.Text) + "', CODIGO_ENTREGA = '" + TxbCodgioEntrega.Text + "', CODIGOEMPLEADO = '" + Convert.ToInt16(TxbCodigoEmpleado.Text) + "'  where CODIGOFACTURA = '" + Convert.ToInt16(TxbCodigoFVenta.Text) + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            MessageBox.Show("se modifico  correctamente");
            desabilitar();
            limpiar();
            TxbCodigoFVenta.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtCancelar_Click(object sender, EventArgs e)
        {
            desabilitar();
            limpiar();
            TxbCodigoFVenta.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtSalir_Click_1(object sender, EventArgs e)
        {
            Principal Facturaventa = new Principal();
            Facturaventa.Show();
            this.Hide();
        }

        private void facturaventa_Load(object sender, EventArgs e)
        {
            Conexion objeto = new Conexion();
            objeto.ActualizarGrid(this.dataGridView1, "select * from FACTURA_VENTA");
        }

        private void TxbCodigoFVenta_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbCodigoCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbCodigoEmpleado_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }
    }
}
