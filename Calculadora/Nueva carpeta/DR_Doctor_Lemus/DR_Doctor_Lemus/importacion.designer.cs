﻿namespace DR_Doctor_Lemus
{
    partial class importacion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(importacion));
            this.LbCancelar = new System.Windows.Forms.Label();
            this.BtCancelar = new System.Windows.Forms.Button();
            this.BtModificar = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.LbSalir = new System.Windows.Forms.Label();
            this.LbActualizar = new System.Windows.Forms.Label();
            this.LbNuevo = new System.Windows.Forms.Label();
            this.LbBuscar = new System.Windows.Forms.Label();
            this.BtSalir = new System.Windows.Forms.Button();
            this.BtActualizar = new System.Windows.Forms.Button();
            this.BtConsultar = new System.Windows.Forms.Button();
            this.BtNuevo = new System.Windows.Forms.Button();
            this.LbCodigoImportacion = new System.Windows.Forms.Label();
            this.TxbCantidad = new System.Windows.Forms.TextBox();
            this.LbCantidad = new System.Windows.Forms.Label();
            this.TxbFechaImportacion = new System.Windows.Forms.TextBox();
            this.LbFechaImportacion = new System.Windows.Forms.Label();
            this.TxbFechaExpedicion = new System.Windows.Forms.TextBox();
            this.TxbCodigo_Importacion = new System.Windows.Forms.TextBox();
            this.LbFechaExpedicion = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // LbCancelar
            // 
            this.LbCancelar.AutoSize = true;
            this.LbCancelar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbCancelar.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCancelar.Location = new System.Drawing.Point(30, 362);
            this.LbCancelar.Name = "LbCancelar";
            this.LbCancelar.Size = new System.Drawing.Size(49, 13);
            this.LbCancelar.TabIndex = 97;
            this.LbCancelar.Text = "Cancelar";
            // 
            // BtCancelar
            // 
            this.BtCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtCancelar.BackgroundImage")));
            this.BtCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtCancelar.Location = new System.Drawing.Point(25, 295);
            this.BtCancelar.Name = "BtCancelar";
            this.BtCancelar.Size = new System.Drawing.Size(55, 53);
            this.BtCancelar.TabIndex = 96;
            this.BtCancelar.UseVisualStyleBackColor = false;
            this.BtCancelar.Click += new System.EventHandler(this.BtCancelar_Click);
            // 
            // BtModificar
            // 
            this.BtModificar.BackColor = System.Drawing.Color.Transparent;
            this.BtModificar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtModificar.BackgroundImage")));
            this.BtModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtModificar.Enabled = false;
            this.BtModificar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtModificar.Location = new System.Drawing.Point(506, 175);
            this.BtModificar.Name = "BtModificar";
            this.BtModificar.Size = new System.Drawing.Size(53, 56);
            this.BtModificar.TabIndex = 95;
            this.BtModificar.UseVisualStyleBackColor = false;
            this.BtModificar.Visible = false;
            this.BtModificar.Click += new System.EventHandler(this.BtModificar_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(143, 417);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(308, 137);
            this.dataGridView1.TabIndex = 94;
            // 
            // LbSalir
            // 
            this.LbSalir.AutoSize = true;
            this.LbSalir.BackColor = System.Drawing.Color.DimGray;
            this.LbSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.LbSalir.Location = new System.Drawing.Point(519, 362);
            this.LbSalir.Name = "LbSalir";
            this.LbSalir.Size = new System.Drawing.Size(27, 13);
            this.LbSalir.TabIndex = 93;
            this.LbSalir.Text = "Salir";
            // 
            // LbActualizar
            // 
            this.LbActualizar.AutoSize = true;
            this.LbActualizar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbActualizar.Enabled = false;
            this.LbActualizar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbActualizar.Location = new System.Drawing.Point(507, 124);
            this.LbActualizar.Name = "LbActualizar";
            this.LbActualizar.Size = new System.Drawing.Size(53, 13);
            this.LbActualizar.TabIndex = 92;
            this.LbActualizar.Text = "Actualizar";
            this.LbActualizar.Visible = false;
            // 
            // LbNuevo
            // 
            this.LbNuevo.AutoSize = true;
            this.LbNuevo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbNuevo.Enabled = false;
            this.LbNuevo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbNuevo.Location = new System.Drawing.Point(29, 255);
            this.LbNuevo.Name = "LbNuevo";
            this.LbNuevo.Size = new System.Drawing.Size(39, 13);
            this.LbNuevo.TabIndex = 91;
            this.LbNuevo.Text = "Nuevo";
            this.LbNuevo.Visible = false;
            // 
            // LbBuscar
            // 
            this.LbBuscar.AutoSize = true;
            this.LbBuscar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbBuscar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LbBuscar.Location = new System.Drawing.Point(28, 141);
            this.LbBuscar.Name = "LbBuscar";
            this.LbBuscar.Size = new System.Drawing.Size(40, 13);
            this.LbBuscar.TabIndex = 90;
            this.LbBuscar.Text = "Buscar";
            // 
            // BtSalir
            // 
            this.BtSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtSalir.BackgroundImage")));
            this.BtSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtSalir.Location = new System.Drawing.Point(506, 297);
            this.BtSalir.Name = "BtSalir";
            this.BtSalir.Size = new System.Drawing.Size(53, 48);
            this.BtSalir.TabIndex = 88;
            this.BtSalir.UseVisualStyleBackColor = true;
            this.BtSalir.Click += new System.EventHandler(this.BtSalir_Click_1);
            // 
            // BtActualizar
            // 
            this.BtActualizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtActualizar.BackgroundImage")));
            this.BtActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtActualizar.Enabled = false;
            this.BtActualizar.Location = new System.Drawing.Point(506, 75);
            this.BtActualizar.Name = "BtActualizar";
            this.BtActualizar.Size = new System.Drawing.Size(54, 46);
            this.BtActualizar.TabIndex = 87;
            this.BtActualizar.UseVisualStyleBackColor = true;
            this.BtActualizar.Visible = false;
            this.BtActualizar.Click += new System.EventHandler(this.BtActualizar_Click);
            // 
            // BtConsultar
            // 
            this.BtConsultar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtConsultar.BackgroundImage")));
            this.BtConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtConsultar.Location = new System.Drawing.Point(25, 76);
            this.BtConsultar.Name = "BtConsultar";
            this.BtConsultar.Size = new System.Drawing.Size(54, 50);
            this.BtConsultar.TabIndex = 86;
            this.BtConsultar.UseVisualStyleBackColor = true;
            this.BtConsultar.Click += new System.EventHandler(this.BtConsultar_Click_1);
            // 
            // BtNuevo
            // 
            this.BtNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtNuevo.BackgroundImage")));
            this.BtNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtNuevo.Enabled = false;
            this.BtNuevo.Location = new System.Drawing.Point(26, 193);
            this.BtNuevo.Name = "BtNuevo";
            this.BtNuevo.Size = new System.Drawing.Size(54, 49);
            this.BtNuevo.TabIndex = 85;
            this.BtNuevo.UseVisualStyleBackColor = true;
            this.BtNuevo.Visible = false;
            this.BtNuevo.Click += new System.EventHandler(this.BtNuevo_Click_1);
            // 
            // LbCodigoImportacion
            // 
            this.LbCodigoImportacion.AutoSize = true;
            this.LbCodigoImportacion.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigoImportacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigoImportacion.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbCodigoImportacion.Location = new System.Drawing.Point(166, 76);
            this.LbCodigoImportacion.Name = "LbCodigoImportacion";
            this.LbCodigoImportacion.Size = new System.Drawing.Size(147, 20);
            this.LbCodigoImportacion.TabIndex = 77;
            this.LbCodigoImportacion.Text = "Codigo Importacion";
            this.toolTip1.SetToolTip(this.LbCodigoImportacion, "recuerde que su codigo debe inicial con una letra");
            // 
            // TxbCantidad
            // 
            this.TxbCantidad.Enabled = false;
            this.TxbCantidad.Location = new System.Drawing.Point(329, 151);
            this.TxbCantidad.Name = "TxbCantidad";
            this.TxbCantidad.Size = new System.Drawing.Size(122, 20);
            this.TxbCantidad.TabIndex = 84;
            this.TxbCantidad.TextChanged += new System.EventHandler(this.TxbCantidad_TextChanged);
            this.TxbCantidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCantidad_KeyPress);
            // 
            // LbCantidad
            // 
            this.LbCantidad.AutoSize = true;
            this.LbCantidad.BackColor = System.Drawing.Color.Transparent;
            this.LbCantidad.Enabled = false;
            this.LbCantidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCantidad.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCantidad.Location = new System.Drawing.Point(164, 151);
            this.LbCantidad.Name = "LbCantidad";
            this.LbCantidad.Size = new System.Drawing.Size(73, 20);
            this.LbCantidad.TabIndex = 83;
            this.LbCantidad.Text = "Cantidad";
            // 
            // TxbFechaImportacion
            // 
            this.TxbFechaImportacion.Enabled = false;
            this.TxbFechaImportacion.Location = new System.Drawing.Point(329, 232);
            this.TxbFechaImportacion.Name = "TxbFechaImportacion";
            this.TxbFechaImportacion.Size = new System.Drawing.Size(122, 20);
            this.TxbFechaImportacion.TabIndex = 81;
            this.TxbFechaImportacion.TextChanged += new System.EventHandler(this.TxbFechaImportacion_TextChanged);
            // 
            // LbFechaImportacion
            // 
            this.LbFechaImportacion.AutoSize = true;
            this.LbFechaImportacion.BackColor = System.Drawing.Color.Transparent;
            this.LbFechaImportacion.Enabled = false;
            this.LbFechaImportacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbFechaImportacion.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.LbFechaImportacion.Location = new System.Drawing.Point(148, 233);
            this.LbFechaImportacion.Name = "LbFechaImportacion";
            this.LbFechaImportacion.Size = new System.Drawing.Size(142, 20);
            this.LbFechaImportacion.TabIndex = 78;
            this.LbFechaImportacion.Text = "Fecha Importacion";
            // 
            // TxbFechaExpedicion
            // 
            this.TxbFechaExpedicion.Enabled = false;
            this.TxbFechaExpedicion.Location = new System.Drawing.Point(328, 312);
            this.TxbFechaExpedicion.Name = "TxbFechaExpedicion";
            this.TxbFechaExpedicion.Size = new System.Drawing.Size(123, 20);
            this.TxbFechaExpedicion.TabIndex = 82;
            this.TxbFechaExpedicion.TextChanged += new System.EventHandler(this.TxbFechaExpedicion_TextChanged);
            // 
            // TxbCodigo_Importacion
            // 
            this.TxbCodigo_Importacion.Location = new System.Drawing.Point(329, 75);
            this.TxbCodigo_Importacion.Name = "TxbCodigo_Importacion";
            this.TxbCodigo_Importacion.Size = new System.Drawing.Size(122, 20);
            this.TxbCodigo_Importacion.TabIndex = 80;
            // 
            // LbFechaExpedicion
            // 
            this.LbFechaExpedicion.AutoSize = true;
            this.LbFechaExpedicion.BackColor = System.Drawing.Color.Transparent;
            this.LbFechaExpedicion.Enabled = false;
            this.LbFechaExpedicion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbFechaExpedicion.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.LbFechaExpedicion.Location = new System.Drawing.Point(148, 316);
            this.LbFechaExpedicion.Name = "LbFechaExpedicion";
            this.LbFechaExpedicion.Size = new System.Drawing.Size(135, 20);
            this.LbFechaExpedicion.TabIndex = 79;
            this.LbFechaExpedicion.Text = "Fecha Expedicion";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(125, -7);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(343, 592);
            this.pictureBox2.TabIndex = 89;
            this.pictureBox2.TabStop = false;
            // 
            // importacion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(585, 578);
            this.Controls.Add(this.LbCancelar);
            this.Controls.Add(this.BtCancelar);
            this.Controls.Add(this.BtModificar);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.LbSalir);
            this.Controls.Add(this.LbActualizar);
            this.Controls.Add(this.LbNuevo);
            this.Controls.Add(this.LbBuscar);
            this.Controls.Add(this.BtSalir);
            this.Controls.Add(this.BtActualizar);
            this.Controls.Add(this.BtConsultar);
            this.Controls.Add(this.BtNuevo);
            this.Controls.Add(this.LbCodigoImportacion);
            this.Controls.Add(this.TxbCantidad);
            this.Controls.Add(this.LbCantidad);
            this.Controls.Add(this.TxbFechaImportacion);
            this.Controls.Add(this.LbFechaImportacion);
            this.Controls.Add(this.TxbFechaExpedicion);
            this.Controls.Add(this.TxbCodigo_Importacion);
            this.Controls.Add(this.LbFechaExpedicion);
            this.Controls.Add(this.pictureBox2);
            this.Name = "importacion";
            this.Text = "importacion";
            this.Load += new System.EventHandler(this.importacion_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LbCancelar;
        private System.Windows.Forms.Button BtCancelar;
        private System.Windows.Forms.Button BtModificar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label LbSalir;
        private System.Windows.Forms.Label LbActualizar;
        private System.Windows.Forms.Label LbNuevo;
        private System.Windows.Forms.Label LbBuscar;
        private System.Windows.Forms.Button BtSalir;
        private System.Windows.Forms.Button BtActualizar;
        private System.Windows.Forms.Button BtConsultar;
        private System.Windows.Forms.Button BtNuevo;
        private System.Windows.Forms.Label LbCodigoImportacion;
        private System.Windows.Forms.TextBox TxbCantidad;
        private System.Windows.Forms.Label LbCantidad;
        private System.Windows.Forms.TextBox TxbFechaImportacion;
        private System.Windows.Forms.Label LbFechaImportacion;
        private System.Windows.Forms.TextBox TxbFechaExpedicion;
        private System.Windows.Forms.TextBox TxbCodigo_Importacion;
        private System.Windows.Forms.Label LbFechaExpedicion;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolTip toolTip1;

    }
}