﻿namespace DR_Doctor_Lemus
{
    partial class empleado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(empleado));
            this.TxbTelefono = new System.Windows.Forms.TextBox();
            this.TxbNumDocumento = new System.Windows.Forms.TextBox();
            this.TxbDireccion = new System.Windows.Forms.TextBox();
            this.TxbApellido = new System.Windows.Forms.TextBox();
            this.TxbNombre = new System.Windows.Forms.TextBox();
            this.TxbCodigoEmpleado = new System.Windows.Forms.TextBox();
            this.LbTelefono = new System.Windows.Forms.Label();
            this.LbNumDocumento = new System.Windows.Forms.Label();
            this.LbDireccion = new System.Windows.Forms.Label();
            this.LbApellido = new System.Windows.Forms.Label();
            this.LbNombre = new System.Windows.Forms.Label();
            this.LbCodigoEmpleado = new System.Windows.Forms.Label();
            this.LbCelular = new System.Windows.Forms.Label();
            this.TxbCelular = new System.Windows.Forms.TextBox();
            this.LbCancelar = new System.Windows.Forms.Label();
            this.BtCancelar = new System.Windows.Forms.Button();
            this.LbNuevo = new System.Windows.Forms.Label();
            this.LbBuscar = new System.Windows.Forms.Label();
            this.BtConsultar = new System.Windows.Forms.Button();
            this.BtNuevo = new System.Windows.Forms.Button();
            this.BtModificar = new System.Windows.Forms.Button();
            this.LbSalir = new System.Windows.Forms.Label();
            this.LbActualizar = new System.Windows.Forms.Label();
            this.BtSalir = new System.Windows.Forms.Button();
            this.BtActualizar = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // TxbTelefono
            // 
            this.TxbTelefono.Location = new System.Drawing.Point(301, 337);
            this.TxbTelefono.Name = "TxbTelefono";
            this.TxbTelefono.Size = new System.Drawing.Size(131, 20);
            this.TxbTelefono.TabIndex = 23;
            this.TxbTelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbTelefono_KeyPress);
            // 
            // TxbNumDocumento
            // 
            this.TxbNumDocumento.Location = new System.Drawing.Point(301, 279);
            this.TxbNumDocumento.Name = "TxbNumDocumento";
            this.TxbNumDocumento.Size = new System.Drawing.Size(131, 20);
            this.TxbNumDocumento.TabIndex = 22;
            this.TxbNumDocumento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbNumDocumento_KeyPress);
            // 
            // TxbDireccion
            // 
            this.TxbDireccion.Location = new System.Drawing.Point(301, 210);
            this.TxbDireccion.Name = "TxbDireccion";
            this.TxbDireccion.Size = new System.Drawing.Size(131, 20);
            this.TxbDireccion.TabIndex = 21;
            // 
            // TxbApellido
            // 
            this.TxbApellido.Location = new System.Drawing.Point(301, 138);
            this.TxbApellido.Name = "TxbApellido";
            this.TxbApellido.Size = new System.Drawing.Size(131, 20);
            this.TxbApellido.TabIndex = 20;
            this.TxbApellido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbApellido_KeyPress);
            // 
            // TxbNombre
            // 
            this.TxbNombre.Location = new System.Drawing.Point(301, 76);
            this.TxbNombre.Name = "TxbNombre";
            this.TxbNombre.Size = new System.Drawing.Size(131, 20);
            this.TxbNombre.TabIndex = 19;
            this.TxbNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbNombre_KeyPress);
            // 
            // TxbCodigoEmpleado
            // 
            this.TxbCodigoEmpleado.Location = new System.Drawing.Point(301, 18);
            this.TxbCodigoEmpleado.Name = "TxbCodigoEmpleado";
            this.TxbCodigoEmpleado.Size = new System.Drawing.Size(131, 20);
            this.TxbCodigoEmpleado.TabIndex = 18;
            this.TxbCodigoEmpleado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCodigoEmpleado_KeyPress);
            // 
            // LbTelefono
            // 
            this.LbTelefono.AutoSize = true;
            this.LbTelefono.BackColor = System.Drawing.Color.Transparent;
            this.LbTelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbTelefono.ForeColor = System.Drawing.SystemColors.Control;
            this.LbTelefono.Location = new System.Drawing.Point(192, 329);
            this.LbTelefono.Name = "LbTelefono";
            this.LbTelefono.Size = new System.Drawing.Size(79, 20);
            this.LbTelefono.TabIndex = 17;
            this.LbTelefono.Text = "Telefono";
            // 
            // LbNumDocumento
            // 
            this.LbNumDocumento.AutoSize = true;
            this.LbNumDocumento.BackColor = System.Drawing.Color.Transparent;
            this.LbNumDocumento.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbNumDocumento.ForeColor = System.Drawing.SystemColors.Control;
            this.LbNumDocumento.Location = new System.Drawing.Point(122, 277);
            this.LbNumDocumento.Name = "LbNumDocumento";
            this.LbNumDocumento.Size = new System.Drawing.Size(168, 20);
            this.LbNumDocumento.TabIndex = 16;
            this.LbNumDocumento.Text = "Numero Documento";
            // 
            // LbDireccion
            // 
            this.LbDireccion.AutoSize = true;
            this.LbDireccion.BackColor = System.Drawing.Color.Transparent;
            this.LbDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbDireccion.ForeColor = System.Drawing.SystemColors.Control;
            this.LbDireccion.Location = new System.Drawing.Point(192, 202);
            this.LbDireccion.Name = "LbDireccion";
            this.LbDireccion.Size = new System.Drawing.Size(84, 20);
            this.LbDireccion.TabIndex = 15;
            this.LbDireccion.Text = "Direccion";
            // 
            // LbApellido
            // 
            this.LbApellido.AutoSize = true;
            this.LbApellido.BackColor = System.Drawing.Color.Transparent;
            this.LbApellido.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbApellido.ForeColor = System.Drawing.SystemColors.Control;
            this.LbApellido.Location = new System.Drawing.Point(192, 135);
            this.LbApellido.Name = "LbApellido";
            this.LbApellido.Size = new System.Drawing.Size(73, 20);
            this.LbApellido.TabIndex = 14;
            this.LbApellido.Text = "Apellido";
            // 
            // LbNombre
            // 
            this.LbNombre.AutoSize = true;
            this.LbNombre.BackColor = System.Drawing.Color.Transparent;
            this.LbNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbNombre.ForeColor = System.Drawing.SystemColors.Control;
            this.LbNombre.Location = new System.Drawing.Point(192, 73);
            this.LbNombre.Name = "LbNombre";
            this.LbNombre.Size = new System.Drawing.Size(71, 20);
            this.LbNombre.TabIndex = 13;
            this.LbNombre.Text = "Nombre";
            // 
            // LbCodigoEmpleado
            // 
            this.LbCodigoEmpleado.AutoSize = true;
            this.LbCodigoEmpleado.BackColor = System.Drawing.Color.Transparent;
            this.LbCodigoEmpleado.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCodigoEmpleado.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCodigoEmpleado.Location = new System.Drawing.Point(141, 18);
            this.LbCodigoEmpleado.Name = "LbCodigoEmpleado";
            this.LbCodigoEmpleado.Size = new System.Drawing.Size(150, 20);
            this.LbCodigoEmpleado.TabIndex = 12;
            this.LbCodigoEmpleado.Text = "Codigo Empleado";
            // 
            // LbCelular
            // 
            this.LbCelular.AutoSize = true;
            this.LbCelular.BackColor = System.Drawing.Color.Transparent;
            this.LbCelular.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LbCelular.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCelular.Location = new System.Drawing.Point(209, 392);
            this.LbCelular.Name = "LbCelular";
            this.LbCelular.Size = new System.Drawing.Size(65, 20);
            this.LbCelular.TabIndex = 29;
            this.LbCelular.Text = "Celular";
            // 
            // TxbCelular
            // 
            this.TxbCelular.Location = new System.Drawing.Point(301, 395);
            this.TxbCelular.Name = "TxbCelular";
            this.TxbCelular.Size = new System.Drawing.Size(131, 20);
            this.TxbCelular.TabIndex = 30;
            this.TxbCelular.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbCelular_KeyPress);
            // 
            // LbCancelar
            // 
            this.LbCancelar.AutoSize = true;
            this.LbCancelar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbCancelar.ForeColor = System.Drawing.SystemColors.Control;
            this.LbCancelar.Location = new System.Drawing.Point(40, 374);
            this.LbCancelar.Name = "LbCancelar";
            this.LbCancelar.Size = new System.Drawing.Size(49, 13);
            this.LbCancelar.TabIndex = 82;
            this.LbCancelar.Text = "Cancelar";
            // 
            // BtCancelar
            // 
            this.BtCancelar.BackColor = System.Drawing.Color.Transparent;
            this.BtCancelar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtCancelar.BackgroundImage")));
            this.BtCancelar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtCancelar.Location = new System.Drawing.Point(35, 307);
            this.BtCancelar.Name = "BtCancelar";
            this.BtCancelar.Size = new System.Drawing.Size(55, 53);
            this.BtCancelar.TabIndex = 81;
            this.BtCancelar.UseVisualStyleBackColor = false;
            this.BtCancelar.Click += new System.EventHandler(this.BtCancelar_Click);
            // 
            // LbNuevo
            // 
            this.LbNuevo.AutoSize = true;
            this.LbNuevo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbNuevo.Enabled = false;
            this.LbNuevo.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbNuevo.Location = new System.Drawing.Point(39, 267);
            this.LbNuevo.Name = "LbNuevo";
            this.LbNuevo.Size = new System.Drawing.Size(39, 13);
            this.LbNuevo.TabIndex = 80;
            this.LbNuevo.Text = "Nuevo";
            this.LbNuevo.Visible = false;
            // 
            // LbBuscar
            // 
            this.LbBuscar.AutoSize = true;
            this.LbBuscar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbBuscar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.LbBuscar.Location = new System.Drawing.Point(38, 153);
            this.LbBuscar.Name = "LbBuscar";
            this.LbBuscar.Size = new System.Drawing.Size(40, 13);
            this.LbBuscar.TabIndex = 79;
            this.LbBuscar.Text = "Buscar";
            // 
            // BtConsultar
            // 
            this.BtConsultar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtConsultar.BackgroundImage")));
            this.BtConsultar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtConsultar.Location = new System.Drawing.Point(35, 88);
            this.BtConsultar.Name = "BtConsultar";
            this.BtConsultar.Size = new System.Drawing.Size(54, 50);
            this.BtConsultar.TabIndex = 78;
            this.BtConsultar.UseVisualStyleBackColor = true;
            this.BtConsultar.Click += new System.EventHandler(this.BtConsultar_Click_1);
            // 
            // BtNuevo
            // 
            this.BtNuevo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtNuevo.BackgroundImage")));
            this.BtNuevo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtNuevo.Enabled = false;
            this.BtNuevo.Location = new System.Drawing.Point(36, 205);
            this.BtNuevo.Name = "BtNuevo";
            this.BtNuevo.Size = new System.Drawing.Size(54, 49);
            this.BtNuevo.TabIndex = 77;
            this.BtNuevo.UseVisualStyleBackColor = true;
            this.BtNuevo.Visible = false;
            this.BtNuevo.Click += new System.EventHandler(this.BtNuevo_Click_1);
            // 
            // BtModificar
            // 
            this.BtModificar.BackColor = System.Drawing.Color.Transparent;
            this.BtModificar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtModificar.BackgroundImage")));
            this.BtModificar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtModificar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtModificar.Location = new System.Drawing.Point(503, 192);
            this.BtModificar.Name = "BtModificar";
            this.BtModificar.Size = new System.Drawing.Size(53, 56);
            this.BtModificar.TabIndex = 87;
            this.BtModificar.UseVisualStyleBackColor = false;
            this.BtModificar.Click += new System.EventHandler(this.BtModificar_Click);
            // 
            // LbSalir
            // 
            this.LbSalir.AutoSize = true;
            this.LbSalir.BackColor = System.Drawing.Color.DimGray;
            this.LbSalir.ForeColor = System.Drawing.SystemColors.Control;
            this.LbSalir.Location = new System.Drawing.Point(516, 379);
            this.LbSalir.Name = "LbSalir";
            this.LbSalir.Size = new System.Drawing.Size(27, 13);
            this.LbSalir.TabIndex = 86;
            this.LbSalir.Text = "Salir";
            // 
            // LbActualizar
            // 
            this.LbActualizar.AutoSize = true;
            this.LbActualizar.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.LbActualizar.Enabled = false;
            this.LbActualizar.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.LbActualizar.Location = new System.Drawing.Point(504, 141);
            this.LbActualizar.Name = "LbActualizar";
            this.LbActualizar.Size = new System.Drawing.Size(53, 13);
            this.LbActualizar.TabIndex = 85;
            this.LbActualizar.Text = "Actualizar";
            this.LbActualizar.Visible = false;
            // 
            // BtSalir
            // 
            this.BtSalir.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtSalir.BackgroundImage")));
            this.BtSalir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtSalir.Location = new System.Drawing.Point(503, 314);
            this.BtSalir.Name = "BtSalir";
            this.BtSalir.Size = new System.Drawing.Size(53, 48);
            this.BtSalir.TabIndex = 84;
            this.BtSalir.UseVisualStyleBackColor = true;
            this.BtSalir.Click += new System.EventHandler(this.BtSalir_Click_1);
            // 
            // BtActualizar
            // 
            this.BtActualizar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtActualizar.BackgroundImage")));
            this.BtActualizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtActualizar.Enabled = false;
            this.BtActualizar.Location = new System.Drawing.Point(503, 92);
            this.BtActualizar.Name = "BtActualizar";
            this.BtActualizar.Size = new System.Drawing.Size(54, 46);
            this.BtActualizar.TabIndex = 83;
            this.BtActualizar.UseVisualStyleBackColor = true;
            this.BtActualizar.Visible = false;
            this.BtActualizar.Click += new System.EventHandler(this.BtActualizar_Click_1);
            // 
            // dataGridView1
            // 
            this.dataGridView1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(126, 436);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(330, 144);
            this.dataGridView1.TabIndex = 88;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox2.Location = new System.Drawing.Point(121, -7);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(343, 607);
            this.pictureBox2.TabIndex = 89;
            this.pictureBox2.TabStop = false;
            // 
            // empleado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(585, 578);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.BtModificar);
            this.Controls.Add(this.LbSalir);
            this.Controls.Add(this.LbActualizar);
            this.Controls.Add(this.BtSalir);
            this.Controls.Add(this.BtActualizar);
            this.Controls.Add(this.LbCancelar);
            this.Controls.Add(this.BtCancelar);
            this.Controls.Add(this.LbNuevo);
            this.Controls.Add(this.LbBuscar);
            this.Controls.Add(this.BtConsultar);
            this.Controls.Add(this.BtNuevo);
            this.Controls.Add(this.TxbCelular);
            this.Controls.Add(this.LbCelular);
            this.Controls.Add(this.TxbTelefono);
            this.Controls.Add(this.TxbNumDocumento);
            this.Controls.Add(this.TxbDireccion);
            this.Controls.Add(this.TxbApellido);
            this.Controls.Add(this.TxbNombre);
            this.Controls.Add(this.TxbCodigoEmpleado);
            this.Controls.Add(this.LbTelefono);
            this.Controls.Add(this.LbNumDocumento);
            this.Controls.Add(this.LbDireccion);
            this.Controls.Add(this.LbApellido);
            this.Controls.Add(this.LbNombre);
            this.Controls.Add(this.LbCodigoEmpleado);
            this.Controls.Add(this.pictureBox2);
            this.Name = "empleado";
            this.Text = "empleado";
            this.Load += new System.EventHandler(this.empleado_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxbTelefono;
        private System.Windows.Forms.TextBox TxbNumDocumento;
        private System.Windows.Forms.TextBox TxbDireccion;
        private System.Windows.Forms.TextBox TxbApellido;
        private System.Windows.Forms.TextBox TxbNombre;
        private System.Windows.Forms.TextBox TxbCodigoEmpleado;
        private System.Windows.Forms.Label LbTelefono;
        private System.Windows.Forms.Label LbNumDocumento;
        private System.Windows.Forms.Label LbDireccion;
        private System.Windows.Forms.Label LbApellido;
        private System.Windows.Forms.Label LbNombre;
        private System.Windows.Forms.Label LbCodigoEmpleado;
        private System.Windows.Forms.Label LbCelular;
        private System.Windows.Forms.TextBox TxbCelular;
        private System.Windows.Forms.Label LbCancelar;
        private System.Windows.Forms.Button BtCancelar;
        private System.Windows.Forms.Label LbNuevo;
        private System.Windows.Forms.Label LbBuscar;
        private System.Windows.Forms.Button BtConsultar;
        private System.Windows.Forms.Button BtNuevo;
        private System.Windows.Forms.Button BtModificar;
        private System.Windows.Forms.Label LbSalir;
        private System.Windows.Forms.Label LbActualizar;
        private System.Windows.Forms.Button BtSalir;
        private System.Windows.Forms.Button BtActualizar;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}