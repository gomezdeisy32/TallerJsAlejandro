﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DR_Doctor_Lemus;
using System.Data.SqlClient;

namespace DR_Doctor_Lemus
{
    public partial class facturacompra : Form
    {
        public facturacompra()
        {
            InitializeComponent();
        }

        private void BtSalir_Click(object sender, EventArgs e)
        {
            Principal facturacompra = new Principal();
            facturacompra.Show();
            this.Hide();
        }

        private void BtNuevo_Click(object sender, EventArgs e)
        {
            // comprobar que todos los datos ingresaron
            if ((TxbCod_FacturaC.Text == "") ||  (TxbCodigoProducto.Text == "") || (TxbCodProvedor.Text == "") || (TxbFechaFactura.Text == ""))
            {
                MessageBox.Show("ha dejado un espacio en blanco");
            }
            else
            {
                try
                {
                    //creamos el archivo de conexion y le damos la ruta
                    SqlConnection conex = new SqlConnection("Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                    // abrimos la conexion

                    conex.Open();
                    //creamos el comado de sql
                    SqlCommand cmd = new SqlCommand(" insert into FACTURACOMPRA(codigofacturac,  codigoproducto, codproveedor, fachacartura)values('" + TxbCod_FacturaC.Text + ",  '" + TxbCodigoProducto.Text + ", '" + TxbCodProvedor.Text + ", '" + TxbFechaFactura.Text + "')");
                    //ejecutamos la conexion con la sentencia de sql
                    cmd.Connection = conex;
                    cmd.ExecuteNonQuery();
                    try
                    {
                        MessageBox.Show("registro exitoso");
                        //cerramos la conexion
                        conex.Close();
                        //limpiamos el formulario

                        TxbCod_FacturaC.Clear();
                        TxbCodigoProducto.Clear();
                        TxbCodProvedor.Clear();
                        TxbFechaFactura.Clear();
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("el reegistro ya existe");
                    }
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("no" + ex);

                }
            }







        }

        private void BtConsultar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection("Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            try
            {
                SqlCommand cmd = new SqlCommand
                ("select * from factura_compra where CODIGOFACTURA_C ='" + TxbCod_FacturaC.Text + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                DataSet ds = new DataSet();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(ds, "facturacompra");
                DataRow dro;
                dro = ds.Tables["facturacompra"].Rows[0];
                TxbCodigoProducto.Text = dro["CODIGO_PRODUCTO"].ToString();
                TxbCodProvedor.Text = dro["CODIGO_PROVEEDOR"].ToString();
                TxbFechaFactura.Text = dro["FECHAFACTURA_C"].ToString();

            }
            catch
            {
                MessageBox.Show("factura no encontrada");
            }
            finally
            {
                con.Close();
            }


        }

        private void BtActualizar_Click(object sender, EventArgs e)
        {
            //conexion a base de datos
            SqlConnection conn = new SqlConnection("Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            //abro conexion
            conn.Open();
            //buscamos el nombre y el registro a actualizar para verificar
            string consulta = "select CODPROVEDOR, CODIGOPRODUCTO  from FACTURACOMPRA where fechafactura = '" + TxbFechaFactura + "'";
            SqlCommand COMAND = new SqlCommand(consulta, conn);
            // El resultado lo guardaremos en una tabla

            DataTable tabla = new DataTable();
            // Usaremos un DataAdapter para leer los datos
            SqlDataAdapter AdaptadorTabla = new SqlDataAdapter(consulta, conn);
            //DataSet ds = new DataSet();
            // Llenamos la tabla con los datos leídos
            AdaptadorTabla.Fill(tabla);
            //guardo informacion en variables
            string codprovedor = tabla.Rows[0]["CODPROVEDOR"].ToString();
            string codigoproducto = tabla.Rows[0]["CODIGOPRODUCTO"].ToString();
            // Creamos el cuadro de dialogo y guardamos la respuesta que da el usuario
            DialogResult respuesta = MessageBox.Show("Desea actualizar el registro de " + codprovedor + " " + codigoproducto + ".", "actualizar", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
            //Si acepto actualizar se ejecuta
            if (respuesta == System.Windows.Forms.DialogResult.Yes)
            {
                string StrComando = "DELETE FROM FACTURACOMPRA WHERE fecfafactura =  '" + TxbFechaFactura + "'";
                SqlCommand COMANDO = new SqlCommand(StrComando, conn);
                COMANDO.ExecuteNonQuery();
                MessageBox.Show("El registro seactualizo de manera  EXITOSA");


            }
            else
            {
                MessageBox.Show("el registro no se actualizo");

            }
            TxbCodigoProducto.Text = "";
            TxbCod_FacturaC.Text = "";
            TxbCodProvedor.Text = "";
            TxbFechaFactura.Text = "";

        }

        private void TxbCod_FacturaC_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }

        }

        private void TxbFechaFactura_TextChanged(object sender, EventArgs e)
        {

        }

        

   

        private void TxbCodProvedor_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }

        }

        

        private void TxbCodigoProducto_TextChanged_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
                MessageBox.Show("solo numeros");
            }

        }

        private void limpiar()
        {
            TxbCod_FacturaC.Clear();
            TxbCodigoProducto.Clear();
            TxbCodProvedor.Clear();
            TxbFechaFactura.Clear();
            BtActualizar.Visible = false;
            BtNuevo.Visible = false;
            BtModificar.Visible = false;


        }
        private void desabilitar()
        {

            TxbFechaFactura.Enabled = false;
            TxbCodProvedor.Enabled = false;
            TxbCodigoProducto.Enabled = false;
            BtActualizar.Enabled = false;
            BtNuevo.Enabled = false;
            BtModificar.Enabled = false;

        }
        private void habilitar()
        {
            TxbCodigoProducto.Enabled = true;
            TxbCodProvedor.Enabled = true;
            TxbFechaFactura.Enabled = true;
            BtActualizar.Enabled = true;
            BtNuevo.Enabled = true;
            BtModificar.Enabled = true;

        }

        private void BtConsultar_Click_1(object sender, EventArgs e)
        {
            if (TxbCod_FacturaC.Text == "")
            {

                MessageBox.Show("el campo no puede estar vacio");
                TxbCod_FacturaC.Focus();
            }
            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
                try
                {
                    SqlCommand cmd = new SqlCommand
                    ("select * from FACTURA_COMPRA where CODIGOFACTURA_C ='" + TxbCod_FacturaC.Text + "' ", con);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds, "FACTURA_COMPRA");
                    DataRow dro;
                    dro = ds.Tables["FACTURA_COMPRA"].Rows[0];
                    TxbFechaFactura.Text = dro["FECHAFACTURA_C"].ToString();
                    TxbCodigoProducto.Text = dro["CODIGO_PRODUCTO"].ToString();
                    TxbCodProvedor.Text = dro["CODIGO_PROVEEDOR"].ToString();
                    BtActualizar.Enabled = true;
                    BtActualizar.Visible = true;

                }
                catch
                {
                    //MessageBox.Show(" usuario no encontrado")
                    const string mensaje = "factura no encontrada desea crearla si/no";
                    const string titulo = "consulta";
                    var resultado = MessageBox.Show(mensaje, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (resultado == DialogResult.No)
                    {
                        TxbCod_FacturaC.Clear();
                        TxbCod_FacturaC.Focus();
                    }
                    else
                    {


                        TxbCod_FacturaC.Enabled = false;
                        TxbCodigoProducto.Clear();
                        TxbCodProvedor.Clear();
                        TxbFechaFactura.Clear();
                        TxbFechaFactura.Enabled = true;
                        TxbCodProvedor.Enabled = true;
                        TxbCodigoProducto.Enabled = true;

                        BtNuevo.Visible = true;
                        BtNuevo.Enabled = true;
                        BtConsultar.Enabled = false;

                        }

                }

                finally
                {
                    con.Close();
                }

            }
       
        }

        private void BtNuevo_Click_1(object sender, EventArgs e)
        {
            if (TxbCod_FacturaC.Text == "")
            {
                MessageBox.Show("el campo apellido esta  vacion");
                TxbCod_FacturaC.Focus();


            }
            else if (TxbFechaFactura.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbFechaFactura.Focus();
            }
            else if (TxbCodigoProducto.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbCodigoProducto.Focus();
            }
            else if (TxbCodProvedor.Text == "")
            {
                MessageBox.Show("el campo codigo factura esta vacio");
                TxbCodProvedor.Focus();
            }

            else
            {
                SqlConnection con = new SqlConnection(@"Data Source=FR002S011;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");


                SqlCommand cmd = new SqlCommand("insert into FACTURA_COMPRA(CODIGOFACTURA_C, FECHAFACTURA_C,CODIGO_PRODUCTO,CODIGO_PROVEEDOR)Values('" + Convert.ToInt16(TxbCod_FacturaC.Text) + "','" + TxbFechaFactura.Text + "','" + Convert.ToInt16(TxbCodigoProducto.Text) + "', '" + TxbCodProvedor.Text + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se   agrego con exito ");
            }
        }

        private void BtActualizar_Click_1(object sender, EventArgs e)
        {
            habilitar();
            BtActualizar.Enabled = false;
            TxbCod_FacturaC.Enabled = false;
            BtModificar.Enabled = true;
            BtModificar.Visible = true;
        }

        private void BtModificar_Click(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");

            SqlCommand cmd = new SqlCommand("update FACTURA_COMPRA set FECHAFACTURA_C ='" + TxbFechaFactura.Text + "', CODIGO_PRODUCTO = '" + TxbCodigoProducto.Text + "', CODIGO_PROVEEDOR = '" + TxbCodProvedor.Text + "'  where CODIGOFACTURA_C = '" + TxbCod_FacturaC.Text + "'", con);
            con.Open();
            cmd.ExecuteNonQuery();

            MessageBox.Show("se modifico  correctamente");
            desabilitar();
            limpiar();
            TxbCod_FacturaC.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void BtCancelar_Click(object sender, EventArgs e)
        {
            desabilitar();
            limpiar();
            TxbCod_FacturaC.Enabled = true;
            BtConsultar.Enabled = true;
        }

        private void facturacompra_Load(object sender, EventArgs e)
        {
            Conexion objeto = new Conexion();
            objeto.ActualizarGrid(this.dataGridView1, "select * from FACTURA_COMPRA");
        }

        private void BtSalir_Click_1(object sender, EventArgs e)
        {
            Principal Facturacompra = new Principal();
            Facturacompra.Show();
            this.Hide();
        }

        private void TxbCod_FacturaC_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }

        private void TxbCodigoProducto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Validar.solonumeros(e);
        }
    }

}