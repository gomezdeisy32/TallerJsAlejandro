﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
namespace DR_Doctor_Lemus
{
    class Conexion
    {
        SqlConnection Miconexion;
        public void conectar()
        {
            //SqlConnection Miconexion = new SqlConnection();
            Miconexion = new SqlConnection(@"Data Source=FR002S004;Initial Catalog=DR_LEMOS_PRODUCTOS;Integrated Security=True");
            Miconexion.Open();
        }
        public void Desconectar()
        {
            Miconexion.Close();
        }
        public void Ejecutarsql(String Query)
        {
            SqlCommand Micomando = new SqlCommand(Query, this.Miconexion);
            int FilasAfectadas = Micomando.ExecuteNonQuery();
            if (FilasAfectadas > 0)
                MessageBox.Show("OPERACION REALIZADA ECITOSAMENTE", "LA BASE DATOS HA SIDO MODIFICADA", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                MessageBox.Show("NO SE PUEDE REALIZAR LA OPERACION", "ERROR DEL SISTEMA", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        public void ActualizarGrid(DataGridView dg, String Query)
        {
            this.conectar();
            System.Data.DataSet MiDataSet = new System.Data.DataSet();
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(Query, Miconexion);
            MyDataAdapter.Fill(MiDataSet, "CLIENTE");
            dg.DataSource = MiDataSet;
            dg.DataMember = "CLIENTE";
            this.Desconectar();

        }
        public void ActualizarGrid1(DataGridView dg, String Query)
        {
            this.conectar();
            System.Data.DataSet MiDataSet = new System.Data.DataSet();
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(Query, Miconexion);
            MyDataAdapter.Fill(MiDataSet, "PROVEEDOR");
            dg.DataSource = MiDataSet;
            dg.DataMember = "PROVEEDOR";
            this.Desconectar();

        }
        public void ActualizarGrid2(DataGridView dg, String Query)
        {
            this.conectar();
            System.Data.DataSet MiDataSet = new System.Data.DataSet();
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(Query, Miconexion);
            MyDataAdapter.Fill(MiDataSet, "DEVOLUCION");
            dg.DataSource = MiDataSet;
            dg.DataMember = "DEVOLUCION";
            this.Desconectar();

        }
        public void ActualizarGrid3(DataGridView dg, String Query)
        {
            this.conectar();
            System.Data.DataSet MiDataSet = new System.Data.DataSet();
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(Query, Miconexion);
            MyDataAdapter.Fill(MiDataSet, "EMPLEADO");
            dg.DataSource = MiDataSet;
            dg.DataMember = "EMPLEADO";
            this.Desconectar();

        }
        public void ActualizarGrid4(DataGridView dg, String Query)
        {
            this.conectar();
            System.Data.DataSet MiDataSet = new System.Data.DataSet();
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(Query, Miconexion);
            MyDataAdapter.Fill(MiDataSet, "ENTREGA");
            dg.DataSource = MiDataSet;
            dg.DataMember = "ENTREGA";
            this.Desconectar();

        }
        public void ActualizarGrid5(DataGridView dg, String Query)
        {
            this.conectar();
            System.Data.DataSet MiDataSet = new System.Data.DataSet();
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(Query, Miconexion);
            MyDataAdapter.Fill(MiDataSet, "ESTANTERIAS");
            dg.DataSource = MiDataSet;
            dg.DataMember = "ESTANTERIAS";
            this.Desconectar();

        }
        public void ActualizarGrid6(DataGridView dg, String Query)
        {
            this.conectar();
            System.Data.DataSet MiDataSet = new System.Data.DataSet();
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(Query, Miconexion);
            MyDataAdapter.Fill(MiDataSet, "FACTURA_COMPRA");
            dg.DataSource = MiDataSet;
            dg.DataMember = "FACTURA_COMPRA";
            this.Desconectar();

        }
        public void ActualizarGrid7(DataGridView dg, String Query)
        {
            this.conectar();
            System.Data.DataSet MiDataSet = new System.Data.DataSet();
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(Query, Miconexion);
            MyDataAdapter.Fill(MiDataSet, "FACTURA_VENTA");
            dg.DataSource = MiDataSet;
            dg.DataMember = "FACTURA_VENTA";
            this.Desconectar();

        }
        public void ActualizarGrid8(DataGridView dg, String Query)
        {
            this.conectar();
            System.Data.DataSet MiDataSet = new System.Data.DataSet();
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(Query, Miconexion);
            MyDataAdapter.Fill(MiDataSet, "IMPORTACION");
            dg.DataSource = MiDataSet;
            dg.DataMember = "IMPORTACION";
            this.Desconectar();

        }
        public void ActualizarGrid9(DataGridView dg, String Query)
        {
            this.conectar();
            System.Data.DataSet MiDataSet = new System.Data.DataSet();
            SqlDataAdapter MyDataAdapter = new SqlDataAdapter(Query, Miconexion);
            MyDataAdapter.Fill(MiDataSet, "PRODUCTO");
            dg.DataSource = MiDataSet;
            dg.DataMember = "PRODUCTO";
            this.Desconectar();

        }
        

    }
}
